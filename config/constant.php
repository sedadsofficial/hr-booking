<?php

return [
    'APP_NAME' => 'SEDA Booking System',

    'status' => [
        'PENDING_APPROVAL' => 'Pending Approval',
        'PENDING_CATERER_QUOTATION' => 'Pending Caterer Quotation',
        'PENDING_CATERER_CONFIRMATION' => 'Pending Caterer Confirmation',
        'APPROVED' => 'Approved',
        'REJECTED' => 'Rejected',
        'CANCELLED' => 'Cancelled'
    ]

];
