@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Setting</a></li>
    <li class="breadcrumb-item"><a href="#">Vehicle</a></li>
    <li class="breadcrumb-item active" aria-current="page">Parameter Management</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Parameter Management</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="row no-gutters">
                            <div class="nav flex-column nav-pills pr-4 border-right" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-vehicles-tab" data-toggle="pill" href="#v-pills-vehicles" role="tab" aria-controls="v-pills-vehicles" aria-selected="false">Vehicles</a>
                            </div>
                            <div class="tab-content ml-4" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-vehicles" role="tabpanel" aria-labelledby="v-pills-vehicle-tab">
                                    <div class="flex-row-reverse ml-2 mb-2">
                                        <button id="btn-add-vehicles" class="btn btn-success mb-2 pull-right"><i class="fa fa-plus-circle"></i> Add</button>
                                    </div>
                                    <table id="dtVehicles" class="datatable table display w-100" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Vehicle Name</th>
                                            <th>Capacity</th>
                                            <th>Plate No</th>
                                            <th>Insurance Date</th>
                                            <th>Roadtax Date</th>
                                            <th>Is Active?</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Vehicle Name</th>
                                            <th>Capacity</th>
                                            <th>Plate No</th>
                                            <th>Insurance Date</th>
                                            <th>Roadtax Date</th>
                                            <th>Is Active?</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- Edit Vehicle Modal -->
                        <div id="vehiclesModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info">Vehicle Name</label>
                                                        <input type="text" id="input-name" name="input-name" class="form-control input-name" placeholder="Enter vehicle name"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info">Capacity</label>
                                                        <input type="text" id="input-capacity" name="input-capacity" class="form-control input-capacity" placeholder="Enter capacity"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info">Plate Number</label>
                                                        <input type="text" id="input-plate-num" name="input-plate-num" class="form-control input-plate-num" placeholder="Enter plate number"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Insurance Date</label>
                                                        <div class="input-group date" id="datetimepicker-insurance" data-target-input="nearest">
                                                            <input type="text" id="datetimepicker-insurance-input" class="form-control datetimepicker-input" data-target="#datetimepicker-insurance" readonly/>
                                                            <div class="input-group-append" data-target="#datetimepicker-insurance" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Upload Insurance Copy</label>
                                                        <div id="insurance-file-div"><a href='#' class='badge badge-primary mb-3 insurance-file-link' target='_blank'>View Uploaded File</a></div>
                                                        <input type="file" id="input-insurance-file" name="input-insurance-file" class="form-control-file input-insurance-file"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Roadtax Expiry Date</label>
                                                        <div class="input-group date" id="datetimepicker-roadtax" data-target-input="nearest">
                                                            <input type="text" id="datetimepicker-roadtax-input" class="form-control datetimepicker-input" data-target="#datetimepicker-roadtax" readonly/>
                                                            <div class="input-group-append" data-target="#datetimepicker-roadtax" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Upload Roadtax Copy</label>
                                                        <div id="roadtax-file-div"><a href='#' class='badge badge-primary mb-3 roadtax-file-link' target='_blank'>View Uploaded File</a></div>
                                                        <input type="file" id="input-roadtax-file" name="input-roadtax-file" class="form-control-file input-roadtax-file"/>
                                                    </div>
                                                </div>
                                                <input id="vehiclesId" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });

            let vehicles = $('#dtVehicles').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: true,
                lengthChange:false,
                ajax: {
                    url: '{{ route('admin.setting.api.get_vehicles_parameter') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                order: [1,'asc'],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {data: 'name'},
                    {data: 'capacity'},
                    {data: 'plate_number'},
                    {
                        data: 'insurance_date',
                        render: function (data) {
                            return moment(data).format('DD/MM/YYYY');
                        }
                    },
                    {
                        data: 'roadtax_expiry_date',
                        render: function (data) {
                            return moment(data).format('DD/MM/YYYY');
                        }
                    },
                    {
                        data: 'is_active',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return row.is_active == 1 ? 'Yes' : 'No';
                        }
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<button class="btn btn-primary btn-sm service"><i class="fa fa-wrench"></i> Service Logbook</button> <input type=\'button\' class=\'btn btn-warning btn-sm edit\' value=\'Edit\'/> <input type=\'button\' class=\'btn '+ (row.is_active == 1 ? 'btn-danger' : 'btn-success') + ' btn-sm delete\' value=' + (row.is_active == 1 ? 'Deactivate' : 'Activate') + '>';
                        }
                    }
                ],
            });

            /**
             * Vehicles START
             */
            $('#dtVehicles tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtVehicles').DataTable().row( row ).data()["id"];
                let name = $('#dtVehicles').DataTable().row( row ).data()["name"];
                let capacity = $('#dtVehicles').DataTable().row( row ).data()["capacity"];
                let plate_number = $('#dtVehicles').DataTable().row( row ).data()["plate_number"];
                let insuranceDate = $('#dtVehicles').DataTable().row( row ).data()["insurance_date"];
                let roadtaxDate = $('#dtVehicles').DataTable().row( row ).data()["roadtax_expiry_date"];
                let insuranceFile = $('#dtVehicles').DataTable().row( row ).data()["insurance_file_name"];
                let roadtaxFile = $('#dtVehicles').DataTable().row( row ).data()["roadtax_file_name"];

                $('#vehiclesModal .modal-title').html('Edit');
                $('#vehiclesModal .registration-form').trigger('reset');
                $('#vehiclesModal .input-name').val(name);
                $('#vehiclesModal .input-capacity').val(capacity);
                $('#vehiclesModal .input-plate-num').val(plate_number);
                $('#datetimepicker-insurance').datetimepicker('date', moment(insuranceDate).format('DD/MM/YYYY'));
                $('#datetimepicker-roadtax').datetimepicker('date', moment(roadtaxDate).format('DD/MM/YYYY'));
                $('#vehiclesId').val(id);

                if (insuranceFile) {
                    $('#insurance-file-div').show();
                    let url = '{{ asset('storage/:id') }}';
                    url = url.replace(':id', insuranceFile);
                    $('a.insurance-file-link').attr('href',url);
                } else {
                    $('#insurance-file-div').hide();
                }

                if (roadtaxFile) {
                    $('#roadtax-file-div').show();
                    let url = '{{ asset('storage/:id') }}';
                    url = url.replace(':id', roadtaxFile);
                    $('a.roadtax-file-link').attr('href',url);
                } else {
                    $('#roadtax-file-div').hide();
                }

                $('#vehiclesModal').modal();
            });

            $('#dtVehicles tbody').on('click', '.service', function () {
                let row = $(this).closest('tr');
                let id = $('#dtVehicles').DataTable().row( row ).data()["id"];
                let url = '{{ url('admin/setting/vehicle/service_logbook/:id') }}';
                url = url.replace(':id', id);
                window.location.href = url;
            });

            $('#btn-add-vehicles').on('click', function () {
                $('#vehiclesModal .modal-title').html('Add');
                $('#vehiclesModal .registration-form').trigger('reset');
                $('#insurance-file-div').hide();
                $('#roadtax-file-div').hide();
                $('#datetimepicker-insurance').datetimepicker('date', moment().format('DD/MM/YYYY'));
                $('#datetimepicker-roadtax').datetimepicker('date', moment().format('DD/MM/YYYY'));
                $('#vehiclesModal').modal();
            });

            $('#vehiclesModal .save').on('click', function() {
                if ($('#frmSubmission').valid()) {
                    $.blockUI({baseZ: 2000});
                    let formData = new FormData();

                    if ($('#vehiclesModal .modal-title').html() == 'Edit') {
                        formData.append('vehicle_id', $('#vehiclesId').val());
                    }

                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('name', $('#vehiclesModal .input-name').val());
                    formData.append('capacity', $('#vehiclesModal .input-capacity').val());
                    formData.append('plate_num', $('#vehiclesModal .input-plate-num').val());
                    formData.append('insurance_date', moment($("#datetimepicker-insurance").datetimepicker('date').format()).format('YYYY-MM-DD 00:00:00') );
                    formData.append('roadtax_date', moment($("#datetimepicker-roadtax").datetimepicker('date').format()).format('YYYY-MM-DD 00:00:00') );
                    formData.append('insurance_file', $('input[type=file].input-insurance-file')[0].files[0]);
                    formData.append('roadtax_file', $('input[type=file].input-roadtax-file')[0].files[0]);

                    $.ajax({
                        method: 'post',
                        url: '{{ route('admin.setting.api.save_vehicles_parameter') }}',
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        data: formData,
                    })
                        .done(function( msg ){
                            $.unblockUI();
                            Swal.fire({
                                icon: 'success',
                                title: 'Data has been saved',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function () {
                                $('#vehiclesModal').modal('hide');
                                vehicles.ajax.reload();
                            })
                        })
                        .fail(function( msg ) {
                            $.unblockUI();
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        });
                }
            });

            $('#dtVehicles tbody').on('click', '.delete', function () {
                var row = $(this).closest('tr');
                var id = $('#dtVehicles').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "This will show/hide vehicle from application form",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, proceed!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('admin.setting.api.delete_vehicles_parameter') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                vehicle_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Saved!',
                                    'Data has been updated.',
                                    'success'
                                );
                                vehicles.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#frmSubmission').validate({
                rules: {
                    'input-name': {
                        required: true
                    },
                    'input-capacity': {
                        required: true
                    },
                    'input-plate-num': {
                        required: true
                    }
                }
            });
        });
    </script>
@endsection
