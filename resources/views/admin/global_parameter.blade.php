@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Setting</a></li>
    <li class="breadcrumb-item"><a href="#">Global</a></li>
    <li class="breadcrumb-item active" aria-current="page">Parameter Management</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Parameter Management</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="row no-gutters">
                            <div class="nav flex-column nav-pills pr-4 border-right" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-divisions-tab" data-toggle="pill" href="#v-pills-divisions" role="tab" aria-controls="v-pills-divisions" aria-selected="false">Divisions</a>
                            </div>
                            <div class="tab-content ml-4" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-divisions" role="tabpanel" aria-labelledby="v-pills-divisions-tab">
                                    <div class="flex-row-reverse ml-2 mb-2">
                                        <button id="btn-add-divisions" class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> Add</button>
                                    </div>
                                    <table id="dtDivisions" class="datatable table display w-100" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- Edit Divisions Modal -->
                        <div id="divisionsModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info">Name</label>
                                                        <input type="text" name="input-name" class="form-control input-name" placeholder="Enter division name"/>
                                                    </div>
                                                </div>
                                                <input class="data-id" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let divisions = $('#dtDivisions').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: false,
                lengthChange:false,
                ajax: {
                    url: '{{ route('room.admin.api.get_divisions_parameter') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                order: [1,'asc'],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                        orderable: false
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type=\'button\' class=\'btn btn-warning btn-sm edit\' value=\'Edit\'/> <input type=\'button\' class=\'btn btn-danger btn-sm delete\' value=\'Delete\'/>';
                        }
                    }
                ],
            });

            /**
             * Divisions START
             */
            $('#dtDivisions tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtDivisions').DataTable().row( row ).data()["id"];
                let name = $('#dtDivisions').DataTable().row( row ).data()["name"];

                $('#divisionsModal .modal-title').html('Edit');
                $('#divisionsModal .input-name').val(name);
                $('#divisionsModal .data-id').val(id);

                $('#divisionsModal').modal();
            });

            $('#btn-add-divisions').on('click', function () {
                $('#divisionsModal .modal-title').html('Add');
                $('#divisionsModal .registration-form').trigger('reset');
                $('#divisionsModal').modal();
            });

            $('#divisionsModal .save').on('click', function() {
                let data = {
                    "_token": "{{ csrf_token() }}",
                    form: $('#divisionsModal .registration-form').serializeArray()
                };

                if ($('#divisionsModal .modal-title').html() == 'Edit') {
                    data = { ...data, data_id: $('#divisionsModal .data-id').val() };
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('room.admin.api.save_divisions_parameter') }}",
                    data: data
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Data has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        divisions.ajax.reload();
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });

            $('#dtDivisions tbody').on('click', '.delete', function () {
                var row = $(this).closest('tr');
                var id = $('#dtDivisions').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.admin.api.delete_divisions_parameter') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                data_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Data has been deleted',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                divisions.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });
        });
    </script>
@endsection
