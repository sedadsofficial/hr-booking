@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Setting</a></li>
    <li class="breadcrumb-item"><a href="#">Vehicle</a></li>
    <li class="breadcrumb-item active" aria-current="page">Driver Management</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Driver Management</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2" style="overflow-x: auto;">
                        <div class="flex-row-reverse ml-2 mb-5">
                            <button id="btn-add-driver" class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> Add Driver</button>
                        </div>
                        <table id="dtDriver" class="datatable table display" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Division</th>
                                <th>Official Office Driver?</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Division</th>
                                <th>Official Office Driver?</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>

                        <!-- Modal: userSelectionModal -->
                        <div class="modal fade .modal-lg" id="userSelectionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Assign as Driver</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table id="dtUser" class="datatable table display" cellspacing="0" style="table-layout: auto">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Division</th>
                                                    <th>Assign as</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Division</th>
                                                    <th>Assign as</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Add Driver Modal -->
                        <div id="driversModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Name</label>
                                                        <input type="text" name="input-name" class="form-control-plaintext input-name" placeholder="Enter name" readonly/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Division</label>
                                                        <input type="text" name="input-division" class="form-control-plaintext input-division" placeholder="Enter division" readonly/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">License Expiry Date</label>
                                                        <div class="input-group date" id="datetimepicker-expiry" data-target-input="nearest">
                                                            <input type="text" id="datetimepicker-input" class="form-control datetimepicker-input" data-target="#datetimepicker-expiry" readonly/>
                                                            <div class="input-group-append" data-target="#datetimepicker-expiry" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Upload License Copy</label>
                                                        <input type="file" id="input-file" name="input-file" class="form-control-file"/>
                                                    </div>
                                                </div>
                                                <input id="userId" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Edit Driver Modal -->
                        <div id="driversEditModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmEditSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Name</label>
                                                        <input type="text" name="input-name" class="form-control-plaintext input-name" placeholder="Enter name" readonly/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Division</label>
                                                        <input type="text" name="input-division" class="form-control-plaintext input-division" placeholder="Enter division" readonly/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">License Expiry Date</label>
                                                        <div class="input-group date" id="datetimepicker-edit-expiry" data-target-input="nearest">
                                                            <input type="text" id="datetimepicker-edit-input" class="form-control datetimepicker-input" data-target="#datetimepicker-edit-expiry" readonly/>
                                                            <div class="input-group-append" data-target="#datetimepicker-edit-expiry" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Upload License Copy</label>
                                                        <div><a href='#' class='badge badge-primary mb-3 link-to-file' target='_blank'>View Uploaded File</a></div>
                                                        <input type="file" id="input-edit-file" name="input-edit-file" class="form-control-file input-edit-file"/>
                                                    </div>
                                                </div>
                                                <input id="driverId" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });

            let table = $('#dtDriver').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                ajax: {
                    url: '{{ route('admin.setting.api.get_all_drivers') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                columns: [
                    {
                        data: 'drivers.id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'division',
                    },
                    {
                        data: 'is_official_office_driver',
                        render: function (data, type, row, meta) {
                            return row.is_official_office_driver == 1 ? '<span class="badge badge-success">Yes</span>' : ' No';
                        }
                    },
                    {
                        data: 'is_active',
                        render: function (data, type, row, meta) {
                            return row.is_active == 1 ? '<span class="badge badge-success">Active</span>' : ' Inactive';
                        }
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let button = "";

                            if (row.is_active == 1) {
                                button += "<input type='button' class='btn btn-danger btn-sm activate' value='Deactivate'/>";
                            } else {
                                button += "<input type='button' class='btn btn-primary btn-sm activate' value='Activate'/>";
                            }

                            if (row.is_official_office_driver == 0) {
                                button += "&nbsp<input type='button' class='btn btn-warning btn-sm edit' value='Edit'/>";
                            }

                            return button;
                        }
                    }
                ],
            });

            $('#btn-add-driver').on('click', function () {
                $('#userSelectionModal').modal();
            });

            $('#userSelectionModal').on('shown.bs.modal', function (e) {
                let url = '{{ route('admin.setting.api.get_all_users') }}';
                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateTo').val();
                $("#dtUser").DataTable().destroy();
                $('#dtUser').DataTable({
                    scrollX: true,
                    processing : true,
                    serverSide : true,
                    pagingType : "full_numbers",
                    paging : true,
                    searching : true,
                    lengthMenu : [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                    ajax : {
                        url: url,
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            selectedDateFrom : moment(dateFrom).format('YYYY-MM-DD HH:mm:00'),
                            selectedDateTo : moment(dateTo).format('YYYY-MM-DD HH:mm:00')
                        },
                        failure: function ( result ) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        }
                    },
                    columns : [
                        {
                            data: 'id',
                            width : '10%',
                            searchable : false,
                            orderable : false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: 'name'},
                        {data: 'division'},
                        {
                            data: 'id',
                            width:'20%',
                            searchable: false,
                            orderable: false,
                            render : function(data,type,row,meta) {
                                if (!row.driver_id) {
                                    return "<input type='button' class='btn btn-primary btn-sm mb-2 assignOfficeDriver' value='Office Driver'/>&nbsp <input type='button' class='btn btn-info btn-sm assignReplacementDriver' value='Replacement Driver'/>";
                                } else {
                                    return "-";
                                }
                            }
                        }
                    ],
                    error: function(e) {
                        console.log(e.responseText);
                    }
                });
            });

            $('body').on('hidden.bs.modal', function () {
                if($('.modal.show').length > 0) {
                    $('body').addClass('modal-open');
                }
            });

            $('#dtDriver tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtDriver').DataTable().row( row ).data()["replacement_driver_id"];
                let name = $('#dtDriver').DataTable().row( row ).data()["name"];
                let division = $('#dtDriver').DataTable().row( row ).data()["division"];
                let expiryDate = $('#dtDriver').DataTable().row( row ).data()["license_expiry_date"];
                let fileName = $('#dtDriver').DataTable().row( row ).data()["license_file_name"];
                let url = '{{ asset('storage/:id') }}';
                url = url.replace(':id', fileName);

                $('#driversEditModal .registration-form').trigger('reset');

                $('#datetimepicker-edit-expiry').datetimepicker('date', moment(expiryDate).format('DD/MM/YYYY'));

                $('#driversEditModal .modal-title').html('Edit Replacement Driver');
                $('#driversEditModal .input-name').val(name);
                $('#driversEditModal .input-division').val(division);
                $('a.link-to-file').attr('href',url);

                $('#driverId').val(id);

                $('#driversEditModal').modal('show');
            });

            $('#dtDriver tbody').on('click', '.activate', function () {
                let row = $(this).closest('tr');
                let id = $('#dtDriver').DataTable().row( row ).data()["id"];

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This will affect the listing of drivers in application form",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, proceed!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('admin.setting.api.activate_or_deactivate_driver') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                driver_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Saved!',
                                    'Status changed successfully!',
                                    'success'
                                );
                                table.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                });
            });

            $('#dtUser tbody').on('click', '.assignOfficeDriver', function () {
                let row = $(this).closest('tr');
                let id = $('#dtUser').DataTable().row( row ).data()["id"];

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You are assigning a Driver role to this user!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, proceed!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('admin.setting.api.add_office_driver') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                user_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Saved!',
                                    'Driver added successfully!',
                                    'success'
                                );
                                table.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                });
            });

            $('#dtUser tbody').on('click', '.assignReplacementDriver', function () {
                let row = $(this).closest('tr');
                let id = $('#dtUser').DataTable().row( row ).data()["id"];
                let name = $('#dtUser').DataTable().row( row ).data()["name"];
                let division = $('#dtUser').DataTable().row( row ).data()["division"];

                $('#driversModal .registration-form').trigger('reset');

                $('#datetimepicker-expiry').datetimepicker('date', moment().format('DD/MM/YYYY'));

                $('#driversModal .modal-title').html('Assign as Replacement Driver');
                $('#driversModal .input-name').val(name);
                $('#driversModal .input-division').val(division);
                $('#userId').val(id);

                $('#userSelectionModal').modal('hide');
                $('#driversModal').modal('show');
            });

            $("#driversModal .save").on('click', function () {
                if ($('#frmSubmission').valid()) {
                    $.blockUI({baseZ: 2000});
                    let formData = new FormData();
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('user_id', $('#userId').val());
                    formData.append('expiry_date', moment($("#datetimepicker-expiry").datetimepicker('date').format()).format('YYYY-MM-DD 00:00:00') );
                    formData.append('file', $('input[type=file]')[0].files[0]);

                    $.ajax({
                        method: 'post',
                        url: '{{ route('admin.setting.api.add_replacement_driver') }}',
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        data: formData,
                    })
                        .done(function( msg ){
                            $.unblockUI();
                            Swal.fire({
                                icon: 'success',
                                title: 'Driver saved successfully!',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function () {
                                $('#driversModal').modal('hide');
                                table.ajax.reload();
                            })
                        })
                        .fail(function( msg ) {
                            $.unblockUI();
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        });
                }
            });

            $("#driversEditModal .save").on('click', function () {
                $.blockUI({baseZ: 2000});
                let formData = new FormData();
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('driver_id', $('#driverId').val());
                formData.append('expiry_date', moment($("#datetimepicker-edit-expiry").datetimepicker('date').format()).format('YYYY-MM-DD 00:00:00') );
                formData.append('file', $('input[type=file].input-edit-file')[0].files[0]);

                $.ajax({
                    method: 'post',
                    url: '{{ route('admin.setting.api.edit_replacement_driver') }}',
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    data: formData,
                })
                    .done(function( msg ){
                        $.unblockUI();
                        Swal.fire({
                            icon: 'success',
                            title: 'Driver saved successfully!',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function () {
                            $('#driversEditModal').modal('hide');
                            table.ajax.reload();
                        })
                    })
                    .fail(function( msg ) {
                        $.unblockUI();
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });

            $('#frmSubmission').validate({
                rules: {
                    'datetimepicker-input': {
                        required: true
                    },
                    'input-file': {
                        required: true
                    },
                }
            });
        });
    </script>
@endsection
