@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Setting</a></li>
    <li class="breadcrumb-item"><a href="#">Vehicle</a></li>
    <li class="breadcrumb-item"><a href="#">Parameter Management</a></li>
    <li class="breadcrumb-item active" aria-current="page">Service Logbook</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Service Logbook</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="tab-content ml-4" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-vehicles" role="tabpanel" aria-labelledby="v-pills-vehicle-tab">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-xl-6 col-lg-6">
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-borderless">
                                                    <tr>
                                                        <td class="font-weight-bold">Vehicle</td>
                                                        <td>{{ $vehicle->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-weight-bold">Plate Number</td>
                                                        <td>{{ $vehicle->plate_number }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-weight-bold">Insurance Date</td>
                                                        <td>{{ Carbon\Carbon::parse($vehicle->insurance_date)->format('d/m/Y')}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-weight-bold">Roadtax Expiry Date</td>
                                                        <td>{{ Carbon\Carbon::parse($vehicle->insurance_date)->format('d/m/Y')}}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="flex-row-reverse ml-2 mb-2">
                                    <button id="btn-add-service-log" class="btn btn-success mb-2 pull-right"><i class="fa fa-plus-circle"></i> Add Record</button>
                                </div>
                                <table id="dtServiceLog" class="datatable table display" cellspacing="0" style="table-layout: auto">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Panel Workshop Name</th>
                                            <th>Service Description</th>
                                            <th>Service Date</th>
                                            <th>Next Service Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Panel Workshop Name</th>
                                            <th>Service Description</th>
                                            <th>Service Date</th>
                                            <th>Next Service Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- Add Service Log Modal -->
                        <div class="modal fade .modal-lg" id="serviceLogModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Panel Workshop Name</label>
                                                        <input type="text" id="input-workshop-name" name="input-workshop-name" class="form-control input-workshop-name" placeholder="Enter panel workshop name"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Service Description</label>
                                                        <textarea type="text" id="input-service-desc" name="input-service-desc" class="form-control input-service-desc" placeholder="Enter service description"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Service Date</label>
                                                        <div class="input-group date" id="datetimepicker-service" data-target-input="nearest">
                                                            <input type="text" id="datetimepicker-service-input" class="form-control datetimepicker-input" data-target="#datetimepicker-service" readonly/>
                                                            <div class="input-group-append" data-target="#datetimepicker-service" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Next Service Date</label>
                                                        <div class="input-group date" id="datetimepicker-next-service" data-target-input="nearest">
                                                            <input type="text" id="datetimepicker-next-service-input" class="form-control datetimepicker-input" data-target="#datetimepicker-next-service" readonly/>
                                                            <div class="input-group-append" data-target="#datetimepicker-next-service" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input id="serviceLogId" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });

            let service = $('#dtServiceLog').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: true,
                lengthChange:false,
                ajax: {
                    url: '{{ route('admin.setting.api.get_service_log') }}',
                    type: "POST",
                    data: {
                        _token: '{{ csrf_token() }}',
                        vehicle_id: '{{ $vehicle->id }}'
                    }
                },
                order: [1,'asc'],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {data: 'panel_workshop_name'},
                    {data: 'service_desc'},
                    {
                        data: 'service_date',
                        render: function (data) {
                            return moment(data).format('DD/MM/YYYY');
                        }
                    },
                    {
                        data: 'next_service_date',
                        render: function (data) {
                            return moment(data).format('DD/MM/YYYY');
                        }
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type=\'button\' class=\'btn btn-warning btn-sm edit\' value=\'Edit\'/> <input type=\'button\' class=\'btn btn-danger btn-sm delete\' value=\'Delete\'/>';
                        }
                    }
                ]
            });

            $('#btn-add-service-log').on('click', function () {
                $('#serviceLogModal .modal-title').html('Add');
                $('#serviceLogModal .registration-form').trigger('reset');
                $('#insurance-file-div').hide();
                $('#roadtax-file-div').hide();
                $('#datetimepicker-service').datetimepicker('date', moment().format('DD/MM/YYYY'));
                $('#datetimepicker-next-service').datetimepicker('date', moment().format('DD/MM/YYYY'));
                $('#vehiclesId').val({{ $vehicle->id }});
                $('#serviceLogModal').modal();
            });

            $('#serviceLogModal .save').on('click', function() {
                if ($('#frmSubmission').valid()) {
                    $.blockUI({baseZ: 2000});

                    let data = {
                        "_token": "{{ csrf_token() }}",
                        vehicleId: '{{ $vehicle->id }}',
                        dateService: $("#datetimepicker-service").datetimepicker('date').format(),
                        dateNextService: $("#datetimepicker-next-service").datetimepicker('date').format(),
                        form: $('#frmSubmission').serialize()
                    };

                    if ($('#serviceLogModal .modal-title').html() == 'Edit') {
                        data = { ...data, serviceLogId: $('#serviceLogId').val() };
                    }

                    $.ajax({
                        method: "POST",
                        url: "{{ route('admin.setting.api.save_service_log') }}",
                        data: data
                    })
                        .done(function( msg ) {
                            $.unblockUI();
                            $('.modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Service log has been saved',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            service.ajax.reload();
                        })
                        .fail(function( msg ) {
                            $.unblockUI();
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            });
                        });
                }
            });

            $('#dtServiceLog tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtServiceLog').DataTable().row( row ).data()["id"];
                let panel_workshop_name = $('#dtServiceLog').DataTable().row( row ).data()["panel_workshop_name"];
                let service_desc = $('#dtServiceLog').DataTable().row( row ).data()["service_desc"];
                let service_date = $('#dtServiceLog').DataTable().row( row ).data()["service_date"];
                let next_service_date = $('#dtServiceLog').DataTable().row( row ).data()["next_service_date"];

                $('#serviceLogModal .modal-title').html('Edit');
                $('#serviceLogModal .registration-form').trigger('reset');
                $('#serviceLogModal .input-workshop-name').val(panel_workshop_name);
                $('#serviceLogModal .input-service-desc').val(service_desc);
                $('#datetimepicker-service').datetimepicker('date', moment(service_date).format('DD/MM/YYYY'));
                $('#datetimepicker-next-service').datetimepicker('date', moment(next_service_date).format('DD/MM/YYYY'));
                $('#serviceLogId').val(id);

                $('#serviceLogModal').modal();
            });

            $('#dtServiceLog tbody').on('click', '.delete', function () {
                var row = $(this).closest('tr');
                var id = $('#dtServiceLog').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('admin.setting.api.delete_service_log') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                serviceLogId: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Deleted!',
                                    'Data has been deleted.',
                                    'success'
                                );
                                service.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#frmSubmission').validate({
                rules: {
                    'input-workshop-name': {
                        required: true
                    },
                    'input-service-desc': {
                        required: true
                    }
                }
            });
        });
    </script>
@endsection
