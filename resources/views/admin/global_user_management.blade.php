@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Setting</a></li>
    <li class="breadcrumb-item"><a href="#">Global</a></li>
    <li class="breadcrumb-item active" aria-current="page">User Management</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">User Management</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2" style="overflow-x: auto;">
                        <table id="dtUser" class="datatable table display" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Division</th>
                                <th>System Admin?</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Division</th>
                                <th>System Admin?</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit User Modal -->
    <div id="userModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modalTitle" class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                </div>
                <div id="modalBody" class="modal-body">
                    <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                        <fieldset style="display: block;">
                            <div class="form-bottom">
                                <div class="form-group">
                                    <label class="label-info">Name</label>
                                    <input type="text" name="input-name" class="form-control input-name" placeholder="Enter user's name"/>
                                </div>
                                <div class="form-group">
                                    <label class="label-info">Email</label>
                                    <input type="text" name="input-email" class="form-control input-email" placeholder="Enter email"/>
                                </div>
                                <div class="form-group">
                                    <label class="label-info">Division</label>
                                    {!! Form::select('division', $division, old('division'), ['class' => 'form-control division']) !!}
                                </div>
                            </div>
                            <input id="userId" type="hidden"/>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let table = $('#dtUser').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                ajax: {
                    url: '{{ route('admin.setting.api.get_all_users') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                columns: [
                    {
                        data: 'users.id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                    },
                    {
                        data: 'email',
                    },
                    {
                        data: 'division',
                    },
                    {
                        data: 'is_admin',
                        render: function (data, type, row, meta) {
                            return row.is_admin == 1 ? '<span class="badge badge-success">Yes</span>' : ' No';
                        }
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let button = "";

                            button = "<input type='button' class='btn btn-success btn-sm edit' value='Edit'/>";
                            if (row.is_admin == 1) {
                                button += "<input type='button' class='btn btn-danger btn-sm activate' value='Revoke Admin Access'/>";
                            } else {
                                button += "<input type='button' class='btn btn-primary btn-sm activate' value='Grant Admin Access'/>";
                            }
                            return button;
                        }
                    }
                ],
            });

            $('#dtUser tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtUser').DataTable().row( row ).data()["id"];
                let name = $('#dtUser').DataTable().row( row ).data()["name"];
                let email = $('#dtUser').DataTable().row( row ).data()["email"];
                let division_id = $('#dtUser').DataTable().row( row ).data()["division_id"];

                $('#userModal .modal-title').html('Edit');
                $('#userModal .input-name').val(name);
                $('#userModal .input-email').val(email);
                $('#userModal .division').val(division_id);
                $('#userId').val(id);

                $('#userModal').modal();
            });

            $('#userModal .save').on('click', function() {
                let data = {
                    "_token": "{{ csrf_token() }}",
                    form: $('#userModal .registration-form').serializeArray()
                };

                if ($('#userModal .modal-title').html() == 'Edit') {
                    data = { ...data, user_id: $('#userId').val() };
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('admin.setting.api.save_user_info') }}",
                    data: data
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Data has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        table.ajax.reload();
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });

            $('#dtUser tbody').on('click', '.activate', function () {
                let row = $(this).closest('tr');
                let id = $('#dtUser').DataTable().row( row ).data()["id"];

                Swal.fire({
                    title: 'Are you sure?',
                    text: "This will provide / restrict user to certain access in the system",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, proceed!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('admin.setting.api.grant_or_revoke_admin') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                user_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Saved!',
                                    'Status changed successfully!',
                                    'success'
                                );
                                table.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                });
            });
        });
    </script>
@endsection
