@extends('layouts.app_alt')

@section('content')
    <style>
        .bg-planner-image {
            background: url("{{ asset('template/img/bg-planner-unsplash.jpg') }}");
            background-position: center;
            background-size: cover;
        }
    </style>
    <div class="col-lg-6 d-none d-lg-block bg-planner-image"></div>
    <div class="col-lg-6">
        <div class="p-5">
            <div class="text-center">
                <div class="row">
                    <div class="col-lg-4 text-right">
                        <span style="font-size: 40px; color: #4e73df;">
                            <i class="fas fa-calendar-check"></i>
                        </span>
                    </div>
                    <div class="col-lg-8 text-left">
                        <div class="h4 text-primary mb-4">{{ config('constant.APP_NAME') }}<sup>1.0<sup></div>
                    </div>
                </div>
            </div>
            <div class="container d-flex justify-content-center align-items-center">
                <div class="row">
                    <div>
                        <div class="panel panel-default">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Login</div>
                            <div class="panel-body">
                                <div class="card border-left-primary shadow">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <form class="form-horizontal w-auto" role="form" method="POST" action="{{ url('/login') }}">
                                                    {{ csrf_field() }}

                                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                        <label for="email" class="control-label">E-Mail Address</label>

                                                        <div class="">
                                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                                            @if ($errors->has('email'))
                                                                <span class="error">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                        <label for="password" class="control-label">Password</label>

                                                        <div class="">
                                                            <input id="password" type="password" class="form-control" name="password">

                                                            @if ($errors->has('password'))
                                                                <span class="error">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="remember"> Remember Me
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="">
                                                            <button type="submit" class="btn btn-primary">
                                                                <i class="fa fa-btn fa-sign-in"></i> Login
                                                            </button>

                                                            <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
