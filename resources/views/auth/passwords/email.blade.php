@extends('layouts.app_alt')

<!-- Main Content -->
@section('content')
    <div class="col-lg-12">
        <div class="p-5">
            <div class="text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-auto text-right">
                        <span style="font-size: 40px; color: #4e73df;">
                            <i class="fas fa-calendar-check"></i>
                        </span>
                    </div>
                    <div class="col-lg-3 text-left">
                        <div class="h4 text-primary mb-4">{{ config('constant.APP_NAME') }}<sup>1.0<sup></div>
                    </div>
                </div>
            </div>
            <div class="container d-flex justify-content-center align-items-center">
                <div class="col-lg-12">
                    <div class="w-100">
                        <div class="panel panel-default">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Reset Password</div>
                            <div class="panel-body">
                                <div class="card border-left-primary shadow">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                @if (session('status'))
                                                    <div class="alert alert-success">
                                                        {{ session('status') }}
                                                    </div>
                                                @endif
                                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                                                    {{ csrf_field() }}

                                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                        <div class="col-md">
                                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                                            @if ($errors->has('email'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-4">
                                                            <button type="submit" class="btn btn-primary">
                                                                <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
