@extends('layouts.app_alt')

@section('content')
    <div class="col-lg-12">
        <div class="p-5">
            <div class="text-center">
                <div class="row justify-content-center">
                    <div class="col-lg-auto text-right">
                        <span style="font-size: 40px; color: #4e73df;">
                            <i class="fas fa-calendar-check"></i>
                        </span>
                    </div>
                    <div class="col-lg-3 text-left">
                        <div class="h4 text-primary mb-4">{{ config('constant.APP_NAME') }}<sup>1.0<sup></div>
                    </div>
                </div>
            </div>
            <div class="container d-flex justify-content-center align-items-center">
                <div class="col-lg-12">
                    <div class="w-100">
                        <div class="panel panel-default">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Register</div>
                            <div class="panel-body">
                                <div class="card border-left-primary shadow">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <form class="form-horizontal w-auto" role="form" method="POST" action="{{ url('/register') }}">
                                                    {{ csrf_field() }}

                                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label for="name" class="control-label">Name</label>

                                                    <div class="">
                                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                                        @if ($errors->has('name'))
                                                            <span class="error">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label for="email" class="control-label">E-Mail Address</label>

                                                    <div class="">
                                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                                        @if ($errors->has('email'))
                                                            <span class="error">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('division') ? ' has-error' : '' }}">
                                                    <label for="division" class="control-label">Division</label>

                                                    <div class="">
                                                        {!! Form::select('division', $division, old('division'), ['class' => 'form-control']) !!}
{{--                                                        <input id="division" type="email" class="form-control" name="division" value="{{ old('division') }}">--}}

                                                        @if ($errors->has('division'))
                                                            <span class="error">
                                                            <strong>{{ $errors->first('division') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                    <label for="password" class="control-label">Password</label>

                                                    <div class="">
                                                        <input id="password" type="password" class="form-control" name="password">

                                                        @if ($errors->has('password'))
                                                            <span class="error">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                                    <label for="password-confirm" class="control-label">Confirm Password</label>

                                                    <div class="">
                                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                                        @if ($errors->has('password_confirmation'))
                                                            <span class="error">
                                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="">
                                                        <button type="submit" class="btn btn-primary">
                                                            <i class="fa fa-btn fa-user"></i> Register
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
