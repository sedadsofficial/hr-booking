@extends('layouts.app')

@section('content')
    <table id="myTable" class="datatable table" cellspacing="0" style="width:100%">
        <thead>
        <tr>
            <th>#</th>
            <th>Meeting Description</th>
            <th>Meeting Room</th>
            <th>Meeting Date</th>
            <th>Applicant Name</th>
            <th>Applied On</th>
            <th>Status</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Meeting Description</th>
            <th>Meeting Room</th>
            <th>Meeting Date</th>
            <th>Applicant Name</th>
            <th>Applied On</th>
            <th>Status</th>
        </tr>
        </tfoot>
        <tbody>
        <tr>
            <th>1</th>
            <th>Meeting Description</th>
            <th>Lestari</th>
            <th>2019-10-10</th>
            <th>Ella</th>
            <th>2019-10-10</th>
            <th>Pending</th>
        </tr>
        <tr>
            <th>2</th>
            <th>Meeting Description</th>
            <th>Lestari</th>
            <th>2019-10-10</th>
            <th>Ella</th>
            <th>2019-10-10</th>
            <th>Pending</th>
        </tr>
        </tbody>
    </table>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>
@endsection
