@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Vehicle</a></li>
    <li class="breadcrumb-item active" aria-current="page">Jobs</li>
@endsection

@section('content')
    <div style="overflow-x: auto; width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">My Jobs</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2" style="overflow-x: auto;">
                        <table id="dtMyBooking" class="datatable table display" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Booking No</th>
                                <th>Purpose Detail</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Vehicle</th>
                                <th>Booking Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Booking No</th>
                                <th>Purpose Detail</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Vehicle</th>
                                <th>Booking Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let table = $('#dtMyBooking').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                ajax: {
                    url: '{{ route('vehicle.api.get_my_jobs') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                order: [
                    [3,'desc']
                ],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data) {
                            return 'VB' + data;
                        }
                    },
                    {
                        data: 'purpose_detail',
                        orderable: false,
                    },
                    {
                        data: 'car_booking_date_from',
                        render: function (data) {
                            return moment(data).format('DD/MM/YYYY h:mm A');
                        }
                    },
                    {
                        data: 'car_booking_date_to',
                        render: function (data) {
                            return moment(data).format('DD/MM/YYYY h:mm A');
                        }
                    },
                    {
                        data: 'name',
                        orderable: false,
                    },
                    {data: 'booking_status'},
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let button = "<input type='button' class='btn btn-primary btn-sm view' value='View'/>";

                            return button;
                        }
                    }
                ],
            });

            $('#dtMyBooking tbody').on('click', '.view', function () {
                let row = $(this).closest('tr');
                let id = $('#dtMyBooking').DataTable().row( row ).data()["id"];
                let url = '{{ url('vehicle/jobs/view/:id') }}';
                url = url.replace(':id', id);
                window.location.href = url;
            });
        });
    </script>
@endsection
