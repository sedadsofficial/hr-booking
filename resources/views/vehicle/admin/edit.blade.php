@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Car Booking</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Booking Details</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div id="datePickDiv" class="collapse show">
                            <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                <fieldset style="display: block;">
                                    <div class="form-bottom">
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">From</label>
                                            <div class="input-group date" id="datetimepicker_from" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_from" readonly/>
                                                <div class="input-group-append" data-target="#datetimepicker_from" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">To</label>
                                            <div class="input-group date" id="datetimepicker_to" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_to" readonly/>
                                                <div class="input-group-append" data-target="#datetimepicker_to" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Destination</label>
                                            <input type="text" name="input-destination" class="form-control" placeholder="Enter Destination" value="{{ $booking->destination }}"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Purpose of Booking</label>
                                            <div class="form-group form-check">
                                                <input type="checkbox" class="form-check-input form-control-plaintext" id="check-meeting-training-seminar" name="check-purpose[]" value="Meeting/Training/Seminar" disabled {{ in_array('Meeting/Training/Seminar', json_decode($booking->purpose_booking)) ? 'checked' : '' }}>
                                                <label class="form-check-label" for="check-meeting-training-seminar">Meeting/Training/Seminar</label>
                                            </div>
                                            <div class="form-group form-check">
                                                <input type="checkbox" class="form-check-input form-control-plaintext" id="check-site-visit" name="check-purpose[]" value="Site Visit" disabled {{ in_array('Site Visit', json_decode($booking->purpose_booking)) ? 'checked' : '' }}>
                                                <label class="form-check-label" for="check-site-visit">Site Visit</label>
                                            </div>
                                            <div class="form-group form-check">
                                                <input type="checkbox" class="form-check-input form-control-plaintext" id="check-others" name="check-purpose[]" value="Others" disabled {{ in_array('Others', json_decode($booking->purpose_booking)) ? 'checked' : '' }}>
                                                <label class="form-check-label" for="check-others">Others</label>
                                                <input type="text" id="input-others" name="input-others" class="form-control-plaintext" value="{{ $booking->purpose_booking_others }}" readonly/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Details of Purpose</label>
                                            <textarea type="text" id="input-detail-of-purpose" name="input-detail-of-purpose" class="form-control-plaintext" placeholder="Enter details of purpose">{{ $booking->purpose_detail }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Transport Type</label>
                                            <label class="form-control radio-inline"><input type="radio" name="input-transport-type" id="input-transport-with-driver" value="1" disabled {{$booking->transport_type == 1 ? 'checked' : ''}}>Transportation With Office Driver</label>
                                            <label class="form-control radio-inline"><input type="radio" name="input-transport-type" id="input-transport-only" value="0" disabled {{$booking->transport_type == 0 ? 'checked' : ''}}>Transportation Only</label>
                                            @if($booking->transport_type == 1)
                                                <div class="form-group">
                                                    <label class="label-info">Select a Driver</label>
                                                    {!! Form::select('input-driver', $driver, $booking->driver_id, ['class' => 'form-control-plaintext']) !!}
                                                </div>
                                            @else
                                                <div class="form-group">
                                                    <label class="label-info">Select a Driver</label>
                                                    {!! Form::select('input-driver', $replacementDriver, $booking->driver_id, ['class' => 'form-control-plaintext']) !!}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Passenger(s) info</label>
                                            <div class="form-group border rounded p-3 passenger-div">
                                                <button type="button" id="btn-add-passenger" class="btn btn-success mb-3"><i class="fa fa-plus-circle"></i> Add Passenger</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Remarks</label>
                                            <textarea type="text" id="input-remark" name="input-remark" class="form-control-plaintext" readonly>{{ $booking->remark }}</textarea>
                                        </div>
{{--                                        <div class="form-group">--}}
{{--                                            <label class="label-info font-weight-bold">Division / Unit</label>--}}
{{--                                            <input type="text" id="input-division" name="input-division" class="form-control-plaintext" placeholder="Enter meeting title" value="{{ $booking->division->name }}" readonly/>--}}
{{--                                             {!! Form::select('input-division', $division, $booking->division_id, ['class' => 'form-control-plaintext', 'disabled'=>'true']) !!}--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="label-info font-weight-bold">Room</label>--}}
{{--                                            <input type="text" id="input-room" name="input-room" class="form-control-plaintext" value="{{ $booking->room->name }}" readonly/>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="label-info font-weight-bold">Type of Setup</label>--}}
{{--                                            <input type="text" id="input-setup" name="input-setup" class="form-control-plaintext" placeholder="Enter meeting title" value="{{ $booking->room_setup->name }}" readonly/>--}}
{{--                                            {!! Form::select('input-setup', $setup, $booking->room_setup_id, ['class' => 'form-control-plaintext', 'disabled'=>'true']) !!}--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="label-info font-weight-bold">Chairperson</label>--}}
{{--                                            <input type="text" id="input-chairperson" name="input-chairperson" class="form-control-plaintext" placeholder="Enter chairperson's name" value="{{ $booking->chairperson }}" readonly/>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="label-info font-weight-bold">No. of Pax</label>--}}
{{--                                            <input type="number" id="input-paxnum" name="input-paxnum" min="0" oninput="validity.valid||(value='');" class="form-control" value="{{ $booking->pax_no }}">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="label-info font-weight-bold">Catering</label>--}}
{{--                                            <input type="text" id="input-catering" name="input-catering" class="form-control-plaintext"  value="{{ $booking->has_catering ? 'Yes' : 'No' }}" readonly/>--}}
{{--                                            <label class="form-control radio-inline"><input type="radio" name="input-catering" value="1" {{ $booking->has_catering ? 'checked' : '' }} disabled>Yes</label>--}}
{{--                                            <label class="form-control radio-inline"><input type="radio" name="input-catering" value="0" {{ $booking->has_catering ? '' : 'checked' }} disabled>No</label>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="label-info font-weight-bold">Details of Food</label>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="label-info font-weight-bold">Budget Line (Grant/Division)</label>--}}
{{--                                            <input type="text" id="input-budgetline" name="input-budgetline" class="form-control-plaintext" placeholder="Enter which grant/division" value="{{ $booking->budget_line }}" readonly/>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="label-info font-weight-bold">Remarks</label>--}}
{{--                                            <textarea type="text" id="input-remark" name="input-remark" class="form-control-plaintext" placeholder="-" readonly>{{ $booking->remarks }}</textarea>--}}
{{--                                        </div>--}}
                                    </div>
                                    <input id="selectedDateFrom" value="{{ $booking->car_booking_date_from }}" type="hidden"/>
                                    <input id="selectedDateTo" value="{{ $booking->car_booking_date_to }}" type="hidden"/>
                                </fieldset>
                            </form>
                            <button id="btn-next" type="button" class="btn btn-primary" data-target="#datePickDiv">Next</button>
                        </div>
                        <div id="carPickDiv" class="collapse">
                            <table id="dtVehicle" class="datatable table display" cellspacing="0" style="table-layout: auto; width: 100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Capacity</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Capacity</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                            <button id="btn-prev" type="button" class="btn btn-primary">Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
                format: 'DD/MM/YYYY h:mm A',
                ignoreReadonly: true
            });

            let dateFrom = $('#selectedDateFrom').val();
            let dateTo = $('#selectedDateTo').val();

            $('#datetimepicker_from').datetimepicker('date', moment(dateFrom).format('DD/MM/YYYY h:mm A'));
            $('#datetimepicker_to').datetimepicker('date', moment(dateTo).format('DD/MM/YYYY h:mm A'));

            $('#datetimepicker_from').on('change.datetimepicker', function (e) {
                $('#selectedDateFrom').val($("#datetimepicker_from").datetimepicker('date').format());
            });

            $('#datetimepicker_to').on('change.datetimepicker', function (e) {
                $('#selectedDateTo').val($("#datetimepicker_to").datetimepicker('date').format());
            });

            $('#btn-next').on('click', function () {
                $('#datePickDiv').collapse('hide');
                $('#carPickDiv').collapse('show');
            });

            $('#btn-prev').on('click', function () {
                $('#carPickDiv').collapse('hide');
                $('#datePickDiv').collapse('show');
            });

            $('#carPickDiv').on('show.bs.collapse', function () {
                let url = '{{ route('vehicle.api.get_available_vehicle') }}';
                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateTo').val();
                $("#dtVehicle").DataTable().destroy();
                $('#dtVehicle').DataTable({
                    scrollX: true,
                    processing : true,
                    serverSide : true,
                    pagingType : "full_numbers",
                    paging : true,
                    searching : false,
                    lengthMenu : [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                    ajax : {
                        url: url,
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            selectedDateFrom : moment(dateFrom).format('YYYY-MM-DD HH:mm:00'),
                            selectedDateTo : moment(dateTo).format('YYYY-MM-DD HH:mm:00'),
                            excludedId : '{{ $booking->id }}'
                        },
                        failure: function ( result ) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        }
                    },
                    columns : [
                        {
                            data: 'id',
                            width : '10%',
                            searchable : false,
                            orderable : false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: 'name'},
                        {
                            data: 'capacity',
                            width : '10%',
                        },
                        {
                            data: 'id',
                            width:'20%',
                            searchable: false,
                            orderable: false,
                            render : function(data,type,full,meta) {
                                return "<input type='button' class='btn btn-primary btn-sm pickVehicle' value='Select'/>"
                            }
                        }
                    ],
                    error: function(e) {
                        console.log(e.responseText);
                    }
                });
            });

            $('#dtVehicle tbody').on('click', '.pickVehicle', function () {
                let row = $(this).closest('tr');
                let id = $('#dtVehicle').DataTable().row( row ).data()["id"];
                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateTo').val();
                $.ajax({
                    method: "POST",
                    url: "{{ route('vehicle.api.edit_vehicle_booking') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        vehicle_id: id,
                        booking_id: '{{ $booking->id }}',
                        dateFrom: moment(dateFrom).format('YYYY-MM-DD HH:mm:00'),
                        dateTo: moment(dateTo).format('YYYY-MM-DD HH:mm:00'),
                        form: $('#frmSubmission').serialize()
                    }
                })
                    .done(function( msg ) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Your edit has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function () {
                            window.location.replace("{{ route('vehicle.admin.all') }}");
                        })
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });

            $('#btn-add-passenger').on('click', function () {
                // Finding total number of elements added
                let total_element = $('.passenger-div').children().length - 1; //exclude the add button

                let next_id = 0;
                if(total_element > 0) {
                    // last id
                    let last_id = $('.passenger-name:last').attr('id');
                    let split_id = last_id.split("_");
                    next_id = Number(split_id[1]) + 1;
                } else {
                    next_id = total_element + 1;
                }

                $('.passenger-div').append('<div class="row border-top pt-3 animated--grow-in">\n' +
                    '                          <div class="col">\n' +
                    '                             <button type="button" class="close remove-passenger" aria-label="Close">\n' +
                    '                                <span aria-hidden="true">&times;</span>\n' +
                    '                             </button>\n' +
                    '                             <div class="form-group">\n' +
                    '                                <label class="label-info">Name</label>\n' +
                    '                                <input type="text" id="input-name_'+ next_id +'" name="input-name[]" class="form-control passenger-name required" placeholder="Enter Passenger\'s Name"/>\n' +
                    '                             </div>\n' +
                    '                             <div class="form-group">\n' +
                    '                                <label class="label-info">Division / Unit</label>\n' +
                    '                                {!! Form::select('input-division[]', $division, null, ['class' => 'form-control']) !!} \n' +
                    '                             </div>\n' +
                    '                          </div>\n' +
                    '                       </div>');

            });

            $('.passenger-div').on('click', '.remove-passenger', function () {
                $(this).parent().parent().remove();
            });

            @foreach($booking->passenger as $passenger)
                // Finding total number of elements added
                total_element = $('.passenger-div').children().length - 1; //exclude the add button

                next_id = 0;
                if(total_element > 0) {
                    // last id
                    let last_id = $('.passenger-name:last').attr('id');
                    let split_id = last_id.split("_");
                    next_id = Number(split_id[1]) + 1;
                } else {
                    next_id = total_element + 1;
                }
                $('.passenger-div').append('<div class="row border-top pt-3 animated--grow-in">\n' +
                    '                          <div class="col">\n' +
                    '                             <button type="button" class="close remove-passenger" aria-label="Close">\n' +
                    '                                <span aria-hidden="true">&times;</span>\n' +
                    '                             </button>\n' +
                    '                             <div class="form-group">\n' +
                    '                                <label class="label-info">Name</label>\n' +
                    '                                <input type="text" id="input-name_'+ next_id +'" name="input-name[]" class="form-control passenger-name required" value="{{ $passenger->name }}" placeholder="Enter Passenger\'s Name"/>\n' +
                    '                             </div>\n' +
                    '                             <div class="form-group">\n' +
                    '                                <label class="label-info">Division / Unit</label>\n' +
                    '                                {!! Form::select('input-division[]', $division, $passenger->division_id, ['class' => 'form-control']) !!} \n' +
                    '                             </div>\n' +
                    '                          </div>\n' +
                    '                       </div>');
            @endforeach
        });
    </script>
@endsection
