@extends('layouts.app')

@section('styles')
    {!! Html::style(asset('https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css')) !!}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Vehicle</a></li>
    <li class="breadcrumb-item active" aria-current="page">Reporting</li>
@endsection

@section('content')
    <div style="overflow-x: auto; width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Usage Reporting</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2" style="overflow-x: auto;">
                        <div class="form-group">
                            <label class="label-info font-weight-bold">Vehicle</label>
                            <div>{!! Form::select('input-transport', $vehicle, '', ['class' => 'form-control input-transport']) !!}</div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="label-info font-weight-bold">From</label>
                                <div class="input-group date" id="datetimepicker_from" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_from" readonly/>
                                    <div class="input-group-append" data-target="#datetimepicker_from" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="label-info font-weight-bold">To</label>
                                <div class="input-group date" id="datetimepicker_to" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_to" readonly/>
                                    <div class="input-group-append" data-target="#datetimepicker_to" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="btn-group mb-3">
                            <button id="btn-generate" class="btn btn-primary"> Generate </button>
                        </div>

                        <table id="dtReport" class="datatable table display" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Trip Detail</th>
                                <th>From</th>
                                <th>To</th>
                                <th class="sum">Mileage (KM)</th>
                                <th class="sum">Touch 'n Go (RM)</th>
                                <th class="sum">Petrol (RM)</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>TOTAL</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/buttons.print.min.js"></script>
    <script>
        $(document).ready( function () {
            let dateFrom = null;
            let dateTo = null;

            $('#dtReport').DataTable({
                dom: 'Bfrti',
                searching: false,
                buttons: [
                    'excelHtml5'
                ]
            });

            $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
                format: 'DD/MM/YYYY h:mm A',
                ignoreReadonly: true
            });

            $('#datetimepicker_from').on('change.datetimepicker', function (e) {
                dateFrom = $("#datetimepicker_from").datetimepicker('date').format();
            });

            $('#datetimepicker_to').on('change.datetimepicker', function (e) {
                dateTo = $("#datetimepicker_to").datetimepicker('date').format();
            });

            $("#btn-generate").click(function () {
                $('#dtReport').DataTable().destroy();
                let table = $('#dtReport').DataTable({
                    processing: true,
                    serverSide: true,
                    pagingType: "full_numbers",
                    paging: false,
                    searching: false,
                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    ajax: {
                        url: '{{ route('vehicle.api.generate_report') }}',
                        type: "POST",
                        data: {
                            "_token" : "{{ csrf_token() }}",
                            vehicle_id : $('.input-transport').val(),
                            dateFrom: dateFrom ? moment(dateFrom).format('YYYY-MM-DD HH:mm:00') : null,
                            dateTo: dateTo ? moment(dateTo).format('YYYY-MM-DD HH:mm:00') : null,
                        },
                    },
                    order: [
                        [2,'desc']
                    ],
                    columns: [
                        {
                            data: 'id',
                            searchable: false,
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {   data: 'purpose_detail',
                            render: function (data, type, row, meta) {
                                let id = row.booking_id;
                                let url = '{{ url('vehicle/admin/view/:id') }}';
                                url = url.replace(':id', id)

                                return '<a href="' + url+ '">'+ data +'</a>';
                            }
                        },
                        {
                            data: 'car_booking_date_from',
                            render: function (data) {
                                return moment(data).format('DD/MM/YYYY h:mm A');
                            }
                        },
                        {
                            data: 'car_booking_date_to',
                            render: function (data) {
                                return moment(data).format('DD/MM/YYYY h:mm A');
                            }
                        },
                        {data: 'total_mileage_per_trip', className: 'text-right'},
                        {data: 'total_toll', className: 'text-right'},
                        {data: 'total_petrol', className: 'text-right'}
                    ],
                    drawCallback: function(row, data, start, end, display) {
                        let api = this.api();

                        api.columns('.sum', {
                            page: 'current'
                        }).every(function() {
                            let sum = this
                                .data()
                                .reduce(function(a, b) {
                                    let x = parseFloat(a) || 0;
                                    let y = parseFloat(b) || 0;
                                    return x + y;
                                }, 0);
                            $(this.footer()).html(sum);
                        });
                    },
                    dom: 'Blfrtip',
                    buttons: [
                        'excelHtml5'
                    ]
                });
            });


            $('#dtMyBooking tbody').on('click', '.view', function () {
                let row = $(this).closest('tr');
                let id = $('#dtMyBooking').DataTable().row( row ).data()["id"];
                let url = '{{ url('vehicle/admin/view/:id') }}';
                url = url.replace(':id', id);
                window.location.href = url;
            });

            $('#dtMyBooking tbody').on('click', '.delete', function () {
                var row = $(this).closest('tr');
                var id = $('#dtMyBooking').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('vehicle.api.delete_vehicle_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Deleted!',
                                    'Your booking has been deleted.',
                                    'success'
                                );
                                table.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#dtMyBooking tbody').on('click', '.cancel', function () {
                let row = $(this).closest('tr');
                let id = $('#dtMyBooking').DataTable().row( row ).data()["id"];

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, cancel it!',
                    input: 'textarea',
                    inputPlaceholder: 'Type your reason here...',
                    inputAttributes: {
                        'aria-label': 'Type your reason here'
                    },
                    inputValidator: (value) => {
                        if (!value) {
                            return 'You need to write something!'
                        }
                    }
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('vehicle.api.cancel_vehicle_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                                remarks: result.value
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Cancelled!',
                                    'Booking has been cancelled.',
                                    'success'
                                );
                                table.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

        });
    </script>
@endsection
