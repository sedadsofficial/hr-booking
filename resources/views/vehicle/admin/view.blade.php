@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Vehicle</a></li>
    <li class="breadcrumb-item active" aria-current="page">View</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">View Booking Details</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Booking Information</h6>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-borderless table-responsive">
                    <tr>
                        <td style="width: 20%"><label class="label-info font-weight-bold">Booking Status</label></td>
                        <td><h5><span class="badge badge-secondary">{{ $booking->booking_status }}</span></h5></td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">From</label></td>
                        <td>{{ Carbon\Carbon::parse($booking->car_booking_date_from)->format('d/m/Y H:i A')}}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">To</label></td>
                        <td>{{ Carbon\Carbon::parse($booking->car_booking_date_to)->format('d/m/Y H:i A')}}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Destination</label></td>
                        <td>{{ $booking->destination }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Purpose of Booking</label></td>
                        <td>
                            <ul class="list-group">
                                @foreach(json_decode($booking->purpose_booking) as $purpose)
                                    <li class="list-group-item">@if($purpose == 'Others')
                                            {{ $purpose }} :
                                            <div>{{$booking->purpose_booking_others}}</div>
                                        @else
                                            {{ $purpose }}
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Details of Purpose</label></td>
                        <td>{{ $booking->purpose_detail }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Vehicle</label></td>
                        <td>{{ $booking->vehicle->name }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Transport Type</label></td>
                        <td>{{ $booking->transport_type == 0 ? 'Transportation Only' : 'Transportation With Office Driver' }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Driver</label></td>
                        <td>{{ $booking->driver->user->name }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Total Passenger(s)</label></td>
                        <td>{{ $booking->passenger->count() }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Passenger Info</label></td>
                        <td>
                            <ul class="list-group">
                                @foreach($booking->passenger as $passenger)
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-5 font-weight-bold">Name:</div>
                                            <div class="col">{{ $passenger->name }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5 font-weight-bold">Division:</div>
                                            <div class="col">{{ $passenger->division->name }}</div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Remarks</label></td>
                        <td>{{ empty($booking->remarks) ? '-' : $booking->remarks }}</td>
                    </tr>
                </table>
                <div class="row">
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row p-3">
                                    <div class="col-sm text-nowrap font-weight-bold">Booked By :</div>
                                    <div class="col">
                                        <span class="font-italic">{{ $booking->user->name }}</span><br>
                                        <span>{{ $booking->user->division->name }}</span>
                                    </div>
                                </div>
                                <div class="row p-3">
                                    <div class="col-sm text-nowrap font-weight-bold">Booked On :</div>
                                    <div class="col">{{ Carbon\Carbon::parse($booking->created_at)->format('d/m/Y g:i A')}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if ($booking->vehicle_log)
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Vehicle Logbook</h6>
            </div>
            <div class="card-body table-responsive">
                <div class="form-group">
                    <label class="label-info font-weight-bold">Actual Date From</label>
                    <div class="input-group date" id="datetimepicker_from" data-target-input="nearest">
                        {{ Carbon\Carbon::parse($booking->vehicle_log->actual_date_from)->format('d/m/Y H:i A')}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="label-info font-weight-bold">Actual Date To</label>
                    <div class="input-group date" id="datetimepicker_to" data-target-input="nearest">
                        {{ Carbon\Carbon::parse($booking->vehicle_log->actual_date_to)->format('d/m/Y H:i A')}}
                    </div>
                </div>
                <div class="form-group">
                    <label class="label-info font-weight-bold">Total Mileage (KM)</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">KM</span>
                        </div>
                        <input type="number" id="input-total-mileage" name="input-total-mileage" class="form-control" placeholder="Enter total mileage" value="{{ $booking->vehicle_log->total_mileage_per_trip }}" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="label-info font-weight-bold">Total Touch 'n Go (RM)</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">RM</span>
                        </div>
                        <input type="number" id="input-total-toll" name="input-total-toll" class="form-control" placeholder="Enter total Touch 'n Go" value="{{ $booking->vehicle_log->total_toll }}" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="label-info font-weight-bold">Total Petrol (RM)</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">RM</span>
                        </div>
                        <input type="number" id="input-total-petrol" name="input-total-petrol" class="form-control" placeholder="Enter total petrol" value="{{ $booking->vehicle_log->total_petrol }}" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="label-info font-weight-bold">Odometer (KM)</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">KM</span>
                        </div>
                        <input type="number" id="input-odometer" name="input-odometer" class="form-control" placeholder="Enter vehicle's odometer reading" value="{{ $booking->vehicle_log->odometer }}" readonly/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="label-info font-weight-bold">Receipt of petrol</label>
                                    <div class="row">
                                        <div class="col" style="overflow-x: auto;">
                                            <table id="dtFile" class="datatable table display table-responsive-lg" width="100%">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>File Name</th>
                                                    <th>Uploaded File</th>
                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>File Name</th>
                                                    <th>Uploaded File</th>
                                                </tr>
                                                </tfoot>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if ($booking->activity()->count() > 0)
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Activity</h6>
            </div>
            <div class="card-body">
                @foreach ($booking->activity as $activity)
                <div class="row">
                    <div class="col">
                        <div class="border-bottom">
                            <div class="row p-3">
                                <div class="col-sm-2"><label class="label-info font-weight-bold">Action</label></div>
                                <div class="col"><h5><span class="badge badge-secondary">{{ $activity->activity_name }}</span></h5></div>
                            </div>
                            <div class="row p-3">
                                <div class="col-sm-2"><label class="label-info font-weight-bold">By</label></div>
                                <div class="col">{{ $activity->user->name }}</div>
                            </div>
                            <div class="row p-3">
                                <div class="col-sm-2"><label class="label-info font-weight-bold">On</label></div>
                                <div class="col">{{ Carbon\Carbon::parse($activity->created_at)->format('d/m/Y h:i A')}}</div>
                            </div>
                            <div class="row p-3">
                                <div class="col-sm-2"><label class="label-info font-weight-bold">Remark</label></div>
                                <div class="col">{{ empty($activity->activity_remark) ? '-' : $activity->activity_remark }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif

        <div class="mt-5 mb-5">
            @if ($booking->is_allowed_to_edit)
                <a class="btn btn-warning" href="{{ route('vehicle.admin.edit',$booking->id) }}" role="button">Edit</a>
            @endif
            @if ($booking->is_allowed_to_cancel)
                <input id="btn-cancel-room-booking" class="btn btn-warning cancel" type="button" value="Cancel"/>
            @endif
            @if ($booking->is_allowed_to_delete)
                <input id="btn-delete-car-booking" class="btn btn-danger delete" type="button" value="Delete"/>
            @endif
            @if ($booking->is_allowed_to_verify && $booking->booking_status != config('constant.status.PENDING_CATERER_CONFIRMATION'))
                <input id="btn-verify-room-booking" class="btn btn-success verify" type="button" value="Verify"/>
            @endif
        </div>
    </div>

    @if($booking->transport_type == 1)
    <!-- Verify Transportation With Driver Modal -->
    <div id="verifyTransportationWithDriverModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modalTitle" class="modal-title">Verification</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                </div>
                <div id="modalBody" class="modal-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-2 font-weight-bold">Action</div>
                                    <div class="col">
                                        <select id="sel-action-transport-with-driver" class="custom-select">
                                            <option value="1" selected>Approve</option>
                                            <option value="0">Reject</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr id="approve-tr">
                            <td>
                                <form role="form" id="frmVerifyTransportationWithDriver" action="" method="post" class="registration-form">
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Requested Driver</label>
                                            <input type="text" name="input-driver-name" class="form-control-plaintext" placeholder="" value="{{ $booking->driver->user->name }}" readonly/>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Available Driver</label>
                                            {!! Form::select('input-driver', $driver, null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Transport</label>
                                            {!! Form::select('input-transport', $vehicle, $booking->vehicle_id, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Waiting Area</label>
                                            <label class="form-control radio-inline"><input type="radio" name="input-waiting-area" id="input-catering-yes" value="B2" checked>Basement Parking (B2)</label>
                                            <label class="form-control radio-inline"><input type="radio" name="input-waiting-area" id="input-catering-no" value="Padi">PADI House</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Remark</label>
                                            <textarea type="text" name="input-remark" class="form-control input-remark-approve" placeholder="Enter your reason..."></textarea>
                                        </div>
                                    </fieldset>
                                </form>
                            </td>
                        </tr>
                        <tr id="reject-tr" class="animated--grow-in" style="display: none">
                            <td>
                                <div class="row">
                                    <div class="col-2">Remark</div>
                                    <div class="col">
                                        <form role="form" id="frmRejectTransportationWithDriver" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <textarea type="text" name="input-remark" class="form-control input-remark-reject" placeholder="Enter your reason..."></textarea>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn-confirm" data-id="">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    @else
    <!-- Verify Transportation Only Modal -->
    <div id="verifyTransportationOnlyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modalTitle" class="modal-title">Verification</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                </div>
                <div id="modalBody" class="modal-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Action</td>
                            <td>
                                <select id="sel-action" class="custom-select">
                                    <option value="1" selected>Approve</option>
                                    <option value="0">Reject</option>
                                </select>
                            </td>
                        </tr>
                        <tr id="remark-tr" style="display: none">
                            <td>Remark</td>
                            <td>
                                <form role="form" id="frmVerifyPendingApproval" action="" method="post" class="registration-form">
                                    <fieldset style="display: block;">
                                        <textarea type="text" id="input-remark" name="input-remark" class="form-control" placeholder="Enter your reason..."></textarea>
                                    </fieldset>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn-confirm" data-id="">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let dt = $('#dtFile').DataTable({
                processing : true,
                serverSide : true,
                lengthChange: false,
                info: false,
                pagingType : "full_numbers",
                paging : true,
                searching : false,
                lengthMenu : [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                ajax : {
                    url: '{{ route('vehicle.api.get_vehicle_log_files') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'booking_id': '{{ $booking->id }}'
                    },
                    failure: function ( result ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Failed loading the table!',
                        })
                    }
                },
                columns : [
                    {
                        data: 'id',
                        width : '10%',
                        searchable : false,
                        orderable : false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {data: 'file_title'},
                    {
                        data: 'file_name',
                        searchable: false,
                        orderable: false,
                        render : function(data,type,full,meta) {
                            let url = '{{ asset('storage/:id') }}';
                            url = url.replace(':id', data);

                            let link = "<a href='"+ url +"' class='badge badge-primary' target='_blank'>View</a>";

                            return link;
                        }
                    }
                ],
                error: function(e) {
                    console.log(e.responseText);
                }
            });

            $('#btn-cancel-room-booking').on('click', function () {
                let id = '{{ $booking->id }}';

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, cancel it!',
                    input: 'textarea',
                    inputPlaceholder: 'Type your reason here...',
                    inputAttributes: {
                        'aria-label': 'Type your reason here'
                    },
                    inputValidator: (value) => {
                        if (!value) {
                            return 'You need to write something!'
                        }
                    }
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('vehicle.api.cancel_vehicle_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                                remarks: result.value
                            }
                        })
                            .done(function( msg ) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your booking has been cancelled.',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(function () {
                                    window.location.replace("{{ route('vehicle.admin.view',$booking->id) }}");
                                })
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-delete-car-booking').on('click', function () {
                let id = '{{ $booking->id }}';

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('vehicle.api.delete_vehicle_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                            }
                        })
                            .done(function( msg ) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your booking has been deleted.',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(function () {
                                    window.location.replace("{{ route('room.admin.api.get_all_booking') }}");
                                })
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-verify-room-booking').on('click', function () {
                let transport_type = '{{ $booking->transport_type }}';

                if (transport_type == 0) {
                    $('#verifyTransportationOnlyModal').modal('show');
                } else  {
                    $('#verifyTransportationWithDriverModal').modal('show');
                }
            });

            $('#sel-action').on('change', function (data) {
                $('#remark-tr').toggle('show');
            });

            $('#sel-action-transport-with-driver').on('change', function (data) {
                $('#reject-tr').toggle('show');
                $('#approve-tr').toggle('show');
            });

            $('.btn-confirm').on('click', function () {
                let allowSubmission = true;
                let data = {
                    "_token": "{{ csrf_token() }}",
                    booking_id: '{{ $booking->id }}',
                };

                if ($('#verifyTransportationOnlyModal').is(':visible')) { //Transportation only
                    $.extend(data, {
                        action: $('#sel-action').val()
                    });

                    if ($('#remark-tr').is(':visible')) {  //If rejected
                        if (!$('#frmVerifyPendingApproval').valid()) {
                            allowSubmission = false;
                        }
                        $.extend(data, {
                            remark: $('#input-remark').val()
                        });
                    }
                }

                $('#frmVerifyTransportationWithDriver').validate({
                    rules: {
                        'input-driver': {
                            required: true
                        },
                        'input-transport': {
                            required: true
                        }
                    }
                });

                if ($('#verifyTransportationWithDriverModal').is(':visible')) { //Transportation with office driver
                    if ($('#approve-tr').is(':visible')) { //If approved
                        if (!$('#frmVerifyTransportationWithDriver').valid()) {
                            allowSubmission = false;
                        }
                        $.extend(data, {
                            action: $('#sel-action-transport-with-driver').val(),
                            remark: $('.input-remark-approve').val(),
                            form: $('#frmVerifyTransportationWithDriver').serialize()
                        });
                    } else { //If rejected
                        if (!$('#frmRejectTransportationWithDriver').valid()) {
                            allowSubmission = false;
                        }
                        $.extend(data, {
                            action: $('#sel-action-transport-with-driver').val(),
                            remark: $('.input-remark-reject').val()
                        });
                    }

                }

                if (allowSubmission) {
                    if ($('#verifyTransportationOnlyModal').is(':visible')) {
                        verifyTransportationOnly(data);
                    } else if ($('#verifyTransportationWithDriverModal').is(':visible')) {
                        verifyTransportationWithDriver(data);
                    }
                }
            });

            function verifyTransportationOnly(data) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('vehicle.admin.api.verify_booking') }}",
                    data: data
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Car booking has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function () {
                            window.location.replace("{{ route('vehicle.admin.view',$booking->id) }}");
                        })
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            }

            function verifyTransportationWithDriver(data) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('vehicle.admin.api.verify_booking_with_driver') }}",
                    data: data
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Car booking has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function () {
                            window.location.replace("{{ route('vehicle.admin.view',$booking->id) }}");
                        })
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            }

            $('#frmVerifyPendingApproval').validate({
                rules: {
                    'input-remark': {
                        required: true
                    }
                }
            });

            $('#frmRejectTransportationWithDriver').validate({
                rules: {
                    'input-remark': {
                        required: true
                    }
                }
            })

        });
    </script>
@endsection
