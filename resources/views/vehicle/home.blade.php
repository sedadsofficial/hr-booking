@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Vehicle</a></li>
    <li class="breadcrumb-item active" aria-current="page">Apply</li>
@endsection

@section('content')
    <style>
        .fc-event {
            cursor: pointer;
        }
    </style>

    <div style="width: 100%;">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Book a Vehicle</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">

        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        @if(!$vehicles->isEmpty())
                            <ul class="list-group list-group-horizontal-md">
                                @foreach($vehicles as $color => $vehicle)
                                    <li class="list-group-item flex-fill"><span class="badge" style="background-color: {{ $color }}">&nbsp;&nbsp;&nbsp;&nbsp;</span> {{ $vehicle }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <div class="mt-3" id='calendar'></div>

                        <!-- Modal: datetimeSelectionModal-->
                        <div class="modal fade .modal-lg" id="datetimeSelectionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Vehicle Booking Request</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">From</label>
                                                        <div class="input-group date" id="datetimepicker_from" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_from" readonly/>
                                                            <div class="input-group-append" data-target="#datetimepicker_from" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">To</label>
                                                        <div class="input-group date" id="datetimepicker_to" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_to" readonly/>
                                                            <div class="input-group-append" data-target="#datetimepicker_to" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Destination</label>
                                                        <input type="text" name="input-destination" class="form-control" placeholder="Enter Destination"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Purpose of Booking</label>
                                                        <div class="form-group form-check">
                                                            <input type="checkbox" class="form-check-input" id="check-meeting-training-seminar" name="check-purpose[]" value="Meeting/Training/Seminar">
                                                            <label class="form-check-label" for="check-meeting-training-seminar">Meeting/Training/Seminar</label>
                                                        </div>
                                                        <div class="form-group form-check">
                                                            <input type="checkbox" class="form-check-input" id="check-site-visit" name="check-purpose[]" value="Site Visit">
                                                            <label class="form-check-label" for="check-site-visit">Site Visit</label>
                                                        </div>
                                                        <div class="form-group form-check">
                                                            <input type="checkbox" class="form-check-input" id="check-others" name="check-purpose[]" value="Others">
                                                            <label class="form-check-label" for="check-others">Others</label>
                                                            <input type="text" id="input-others" name="input-others" class="form-control" placeholder="Enter purpose of booking"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Details of Purpose</label>
                                                        <textarea type="text" id="input-detail-of-purpose" name="input-detail-of-purpose" class="form-control" placeholder="Enter details of purpose"></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Transport Type</label>
                                                        <label class="form-control radio-inline"><input type="radio" name="input-transport-type" id="input-transport-with-driver" value="1">Transportation With Office Driver</label>
                                                        <div class="form-group animated--grow-in transport-with-driver" style="display: none">
                                                            <label class="label-info">Select a Driver</label>
                                                            {!! Form::select('input-driver', $driver, null, ['class' => 'form-control']) !!}
                                                        </div>
                                                        <label class="form-control radio-inline"><input type="radio" name="input-transport-type" id="input-transport-only" value="0">Transportation Only</label>
                                                        <div class="form-group animated--grow-in transport-only" style="display: none">
                                                            <label class="label-info">Select a Driver</label>
                                                            {!! Form::select('input-driver-transport-only', $replacementDriver, null, ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
{{--                                                    <div class="form-group">--}}
{{--                                                        <label class="label-info font-weight-bold">No. of Passenger(s)</label>--}}
{{--                                                        <input type="number" id="input-paxnum" name="input-paxnum" min="0" oninput="validity.valid||(value='');" class="form-control">--}}
{{--                                                    </div>--}}
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Passenger(s) info</label>
                                                        <div class="form-group border rounded p-3 passenger-div">
                                                            <button type="button" id="btn-add-passenger" class="btn btn-success mb-3"><i class="fa fa-plus-circle"></i> Add Passenger</button>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info font-weight-bold">Remarks</label>
                                                        <textarea type="text" id="input-remark" name="input-remark" class="form-control" placeholder="Enter remarks (if any)"></textarea>
                                                    </div>
                                                </div>
                                                <input id="selectedDateFrom" type="hidden"/>
                                                <input id="selectedDateTo" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-next">Next</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal: carSelectionModal -->
                        <div class="modal fade .modal-lg" id="carSelectionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Pick a Vehicle</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table id="dtVehicle" class="datatable table display" cellspacing="0" style="table-layout: auto">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Capacity</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Capacity</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-prev">Back</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Event Modal -->
                        <div id="fullCalModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body"></div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        {{--<button id="btn-view" class="btn btn-primary" data-id="">View</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let calendarEl = document.getElementById('calendar');

            let calendar = new FullCalendar.Calendar(calendarEl, {
                header: {
                    center: 'dayGridMonth,timeGridWeek' // buttons for switching between views
                },
                plugins: [ 'dayGrid','interaction','timeGrid','moment' ],
                nowIndicator: true,
                eventLimit: 5,
                views: {
                    dayGrid: {
                    },
                    week: {
                    }
                },
                dateClick: function (info) {
                    calendar.changeView('timeGridWeek', info.dateStr);
                    if (info.view.type == 'timeGridWeek') {
                        $('#datetimeSelectionModal').modal();
                        $('#selectedDateFrom').val(info.dateStr);
                    }
                },
                firstDay: 1,
                startParam: 'agenda.start',
                limitParam : 'agenda.limit',
                events: {
                    url: '{{ route('vehicle.api.show_all_vehicle_booking') }}',
                    method: 'GET',
                    failure: function() {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Unable to fetch calendar! Refresh the page and try again',
                        })
                    },
                    textColor: '#ffffff',
                },
                eventClick: function (event) {
                    $('#modalTitle').html(event.event.title);
                    $('#modalBody').html(event.event.extendedProps.description);
                    $('#btn-remove').attr('data-id',event.event.id);
                    $('#btn-edit').attr('data-id',event.event.id);
                    $('#btn-view').attr('data-id',event.event.id);
                    $('#fullCalModal').modal();
                },
            });

            calendar.render();

            $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
                format: 'DD/MM/YYYY h:mm A',
                ignoreReadonly: true
            });

            $('#frmSubmission').validate({
                rules: {
                    'input-destination':  {
                        required: true
                    },
                    'input-others': {
                        required: '#check-others:checked'
                    },
                    'input-detail-of-purpose': {
                        required: true
                    },
                    'input-transport-type': {
                        required: true
                    },
                    'check-purpose[]': {
                        required: true,
                        minlength: 1,
                    },
                    'input-driver': {
                        required: '#input-transport-with-driver:checked'
                    },
                    'input-driver-transport-only': {
                        required: '#input-transport-only:checked'
                    }
                },
                messages: {
                    'check-purpose[]': 'Choose at least one (1)',
                },
                errorPlacement: function(error, element)
                {
                    if ( element.is(":radio")) {
                        error.appendTo(element.parents('.form-group')[0]);
                    }
                    else if (element.is(":checkbox")) {
                        error.insertAfter(element.parents().parents('.form-group').prev($('.label-info')));
                    }
                    else { // This is the default behavior
                        error.insertAfter( element );
                    }
                }
            });

            $('#datetimeSelectionModal').on('shown.bs.modal', function (e) {
                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateFrom').val();

                $('#datetimepicker_from').datetimepicker('date', moment(dateFrom).format('DD/MM/YYYY h:mm A'));
                $('#datetimepicker_to').datetimepicker('date', moment(dateTo).format('DD/MM/YYYY h:mm A'));
            });

            $('#carSelectionModal').on('shown.bs.modal', function (e) {
                let url = '{{ route('vehicle.api.get_available_vehicle') }}';
                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateTo').val();
                $("#dtVehicle").DataTable().destroy();
                $('#dtVehicle').DataTable({
                    scrollX: true,
                    processing : true,
                    serverSide : true,
                    pagingType : "full_numbers",
                    paging : true,
                    searching : false,
                    lengthMenu : [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                    ajax : {
                        url: url,
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            selectedDateFrom : moment(dateFrom).format('YYYY-MM-DD HH:mm:00'),
                            selectedDateTo : moment(dateTo).format('YYYY-MM-DD HH:mm:00')
                        },
                        failure: function ( result ) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        }
                    },
                    columns : [
                        {
                            data: 'id',
                            width : '10%',
                            searchable : false,
                            orderable : false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: 'name'},
                        {
                            data: 'capacity',
                            width : '10%',
                        },
                        {
                            data: 'id',
                            width:'20%',
                            searchable: false,
                            orderable: false,
                            render : function(data,type,full,meta) {
                                return "<input type='button' class='btn btn-primary btn-sm pickVehicle' value='Select'/>"
                            }
                        }
                    ],
                    error: function(e) {
                        console.log(e.responseText);
                    }
                });
            });

            $('input:radio[name="input-transport-type"]').on('change', function () {
                if ($(this).val() == 1) { //transport with driver
                    $('.transport-with-driver').show();
                    $('.transport-only').hide();
                } else {
                    $('.transport-only').show();
                    $('.transport-with-driver').hide();
                }
            });

            $('#btn-add-passenger').on('click', function () {
                // Finding total number of elements added
                let total_element = $('.passenger-div').children().length - 1; //exclude the add button

                let next_id = 0;
                if(total_element > 0) {
                    // last id
                    let last_id = $('.passenger-name:last').attr('id');
                    let split_id = last_id.split("_");
                    next_id = Number(split_id[1]) + 1;
                } else {
                    next_id = total_element + 1;
                }

                $('.passenger-div').append('<div class="row border-top pt-3 animated--grow-in">\n' +
                    '                          <div class="col">\n' +
                    '                             <button type="button" class="close remove-passenger" aria-label="Close">\n' +
                    '                                <span aria-hidden="true">&times;</span>\n' +
                    '                             </button>\n' +
                    '                             <div class="form-group">\n' +
                    '                                <label class="label-info">Name</label>\n' +
                    '                                <input type="text" id="input-name_'+ next_id +'" name="input-name[]" class="form-control passenger-name required" placeholder="Enter Passenger\'s Name"/>\n' +
                    '                             </div>\n' +
                    '                             <div class="form-group">\n' +
                    '                                <label class="label-info">Division / Unit</label>\n' +
                    '                                {!! Form::select('input-division[]', $division, null, ['class' => 'form-control']) !!} \n' +
                    '                             </div>\n' +
                    '                          </div>\n' +
                    '                       </div>');

            });

            $('.passenger-div').on('click', '.remove-passenger', function () {
                $(this).parent().parent().remove();
            });

            $("div[id*='SelectionModal']").each(function() {
                let currentModal = $(this);

                //click next
                currentModal.find('.btn-next').click(function(){
                    if ($('#frmSubmission').valid()) {
                        currentModal.modal('hide');
                        currentModal.closest("div[id*='SelectionModal']").nextAll("div[id*='SelectionModal']").first().modal('show');
                    }
                });

                //click prev
                currentModal.find('.btn-prev').click(function(){
                    currentModal.modal('hide');
                    currentModal.closest("div[id*='SelectionModal']").prevAll("div[id*='SelectionModal']").first().modal('show');
                });
            });

            $('body').on('hidden.bs.modal', function () {
                if($('.modal.show').length > 0) {
                    $('body').addClass('modal-open');
                }
            });

            $('#datetimepicker_from').on('change.datetimepicker', function (e) {
                $('#selectedDateFrom').val($("#datetimepicker_from").datetimepicker('date').format());
            });

            $('#datetimepicker_to').on('change.datetimepicker', function (e) {
                $('#selectedDateTo').val($("#datetimepicker_to").datetimepicker('date').format());
            });

            $('#datetimepicker_from').on('error.datetimepicker', function (e) {
                console.log(e);
            });

            $('#dtVehicle tbody').on('click', '.pickVehicle', function () {
                var row = $(this).closest('tr');
                var id = $('#dtVehicle').DataTable().row( row ).data()["id"];

                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateTo').val();

                $.blockUI({baseZ: 2000});
                $.ajax({
                    method: "POST",
                    url: "{{ route('vehicle.api.save_vehicle_booking') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        vehicle_id: id,
                        dateFrom: moment(dateFrom).format('YYYY-MM-DD HH:mm:00'),
                        dateTo: moment(dateTo).format('YYYY-MM-DD HH:mm:00'),
                        form: $('#frmSubmission').serialize()
                    }
                })
                    .done(function( msg ) {
                        $.unblockUI();
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Vehicle booking has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        calendar.refetchEvents();
                    })
                    .fail(function( msg ) {
                        $.unblockUI();
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        });
                    });
            });

        });
    </script>
@endsection
