@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Car Booking</a></li>
    <li class="breadcrumb-item active" aria-current="page">View</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">View Booking Details</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <table class="table table-borderless table-responsive">
                            <tr>
                                <td><label class="label-info font-weight-bold">Booking Status</label></td>
                                <td><h5><span class="badge badge-secondary">{{ $booking->booking_status }}</span></h5></td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">From</label></td>
                                <td>{{ Carbon\Carbon::parse($booking->car_booking_date_from)->format('d/m/Y H:i A')}}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">To</label></td>
                                <td>{{ Carbon\Carbon::parse($booking->car_booking_date_to)->format('d/m/Y H:i A')}}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Destination</label></td>
                                <td>{{ $booking->destination }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Purpose of Booking</label></td>
                                <td>
                                    <ul class="list-group">
                                        @foreach(json_decode($booking->purpose_booking) as $purpose)
                                            <li class="list-group-item">
                                                @if($purpose == 'Others')
                                                    {{ $purpose }} :
                                                    <div>{{$booking->purpose_booking_others}}</div>
                                                @else
                                                    {{ $purpose }}
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Details of Purpose</label></td>
                                <td>{{ $booking->purpose_detail }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Vehicle</label></td>
                                <td>{{ $booking->vehicle->name }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Transport Type</label></td>
                                <td>{{ $booking->transport_type == 0 ? 'Transportation Only' : 'Transportation With Office Driver' }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Driver</label></td>
                                <td>{{ $booking->driver->user->name }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Total Passenger(s)</label></td>
                                <td>{{ $booking->passenger->count() }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Passenger Info</label></td>
                                <td>
                                    <ul class="list-group">
                                    @foreach($booking->passenger as $passenger)
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-5 font-weight-bold">Name:</div>
                                                <div class="col">{{ $passenger->name }}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5 font-weight-bold">Division:</div>
                                                <div class="col">{{ $passenger->division->name }}</div>
                                            </div>
                                        </li>
                                    @endforeach
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Remarks</label></td>
                                <td>{{ empty($booking->remarks) ? '-' : $booking->remarks }}</td>
                            </tr>
                        </table>
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row p-3">
                                            <div class="col-sm text-nowrap font-weight-bold">Booked By :</div>
                                            <div class="col">
                                                <span class="font-italic">{{ $booking->user->name }}</span><br>
                                                <span>{{ $booking->user->division->name }}</span>
                                            </div>
                                        </div>
                                        <div class="row p-3">
                                            <div class="col-sm text-nowrap font-weight-bold">Booked On :</div>
                                            <div class="col">{{ Carbon\Carbon::parse($booking->created_at)->format('d/m/Y g:i A')}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-5">
                            @if ($booking->is_allowed_to_edit)
                                <a class="btn btn-warning" href="{{ route('vehicle.edit',$booking->id) }}" role="button">Edit</a>
                            @endif
                            @if ($booking->is_allowed_to_cancel)
                                <input id="btn-cancel-car-booking" class="btn btn-warning cancel" type="button" value="Cancel"/>
                            @endif
                            @if ($booking->is_allowed_to_delete)
                                <input id="btn-delete-car-booking" class="btn btn-danger delete" type="button" value="Delete"/>
                            @endif
                            @if($booking->booking_status == config('constant.status.PENDING_CATERER_CONFIRMATION'))
                                <input id="btn-verify-room-booking" class="btn btn-success verify" type="button" value="Verify"/>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pending Confirmation Modal -->
    <div id="verifyPendingConfirmation" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modalTitle" class="modal-title">Verification</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                </div>
                <div id="modalBody" class="modal-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Action</td>
                            <td>
                                <span><em>* I hereby acknowledge, by my action below, that I have agreed to the given caterer quotation</em></span>
                                <select id="sel-action" class="custom-select">
                                    <option value="1" selected>Confirm</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="btn-confirm" class="btn btn-primary" data-id="">Confirm</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $('#btn-cancel-car-booking').on('click', function () {
                let id = '{{ $booking->id }}';

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, cancel it!',
                    input: 'textarea',
                    inputPlaceholder: 'Type your reason here...',
                    inputAttributes: {
                        'aria-label': 'Type your reason here'
                    },
                    inputValidator: (value) => {
                        if (!value) {
                            return 'You need to write something!'
                        }
                    }
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('vehicle.api.cancel_vehicle_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                                remarks: result.value
                            }
                        })
                            .done(function( msg ) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your booking has been cancelled.',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(function () {
                                    window.location.replace("{{ route('vehicle.mybooking') }}");
                                })
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-delete-car-booking').on('click', function () {
                let id = '{{ $booking->id }}';

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('vehicle.api.delete_vehicle_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                            }
                        })
                            .done(function( msg ) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your booking has been deleted.',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(function () {
                                    window.location.replace("{{ route('vehicle.mybooking') }}");
                                })
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-verify-room-booking').on('click', function () {
                $('#verifyPendingConfirmation').modal('show');
            });

            $('#btn-confirm').on('click', function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('room.api.verify_booking') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        booking_id: '{{ $booking->id }}',
                    }
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Room booking has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function () {
                            window.location.replace("{{ route('room.view',$booking->id) }}");
                        })
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });

            });
        });
    </script>
@endsection
