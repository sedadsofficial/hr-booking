@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Car Booking</a></li>
    <li class="breadcrumb-item active" aria-current="page">View</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">View Job Details</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <table class="table table-borderless table-responsive">
                            <tr>
                                <td><label class="label-info font-weight-bold">Booking Status</label></td>
                                <td><h5><span class="badge badge-secondary">{{ $booking->booking_status }}</span></h5></td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">From</label></td>
                                <td>{{ Carbon\Carbon::parse($booking->car_booking_date_from)->format('d/m/Y H:i A')}}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">To</label></td>
                                <td>{{ Carbon\Carbon::parse($booking->car_booking_date_to)->format('d/m/Y H:i A')}}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Destination</label></td>
                                <td>{{ $booking->destination }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Purpose of Booking</label></td>
                                <td>
                                    <ul class="list-group">
                                        @foreach(json_decode($booking->purpose_booking) as $purpose)
                                            <li class="list-group-item">{{ $purpose }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Details of Purpose</label></td>
                                <td>{{ $booking->purpose_detail }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Vehicle</label></td>
                                <td>{{ $booking->vehicle->name }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Transport type</label></td>
                                <td>{{ $booking->transport_type == 0 ? 'Transportation Only' : 'Transportation With Office Driver' }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Driver</label></td>
                                <td>{{ $booking->driver->user->name }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Passenger Info</label></td>
                                <td>
                                    <ul class="list-group">
                                    @foreach($booking->passenger as $passenger)
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-5 font-weight-bold">Name:</div>
                                                <div class="col">{{ $passenger->name }}</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5 font-weight-bold">Division:</div>
                                                <div class="col">{{ $passenger->division->name }}</div>
                                            </div>
                                        </li>
                                    @endforeach
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Remarks</label></td>
                                <td>{{ empty($booking->remarks) ? '-' : $booking->remarks }}</td>
                            </tr>
                        </table>
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row p-3">
                                            <div class="col-sm text-nowrap font-weight-bold">Booked By :</div>
                                            <div class="col">
                                                <span class="font-italic">{{ $booking->user->name }}</span><br>
                                                <span>{{ $booking->user->division->name }}</span>
                                            </div>
                                        </div>
                                        <div class="row p-3">
                                            <div class="col-sm text-nowrap font-weight-bold">Booked On :</div>
                                            <div class="col">{{ Carbon\Carbon::parse($booking->created_at)->format('d/m/Y g:i A')}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-5">
                            <button id="btn-logbook" class="btn btn-primary mb-5"><i class="fa fa-tachometer-alt"></i> Vehicle Logbook</button>
                            @if(\Carbon\Carbon::parse($booking->car_booking_date_to) > \Carbon\Carbon::now() )
                            <button id="btn-job-done" class="btn btn-success mb-5"><i class="fa fa-check"></i> Job Done</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Vehicle Logbook Modal -->
    <div id="vehicleLogbookModal" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modalTitle" class="modal-title">Vehicle Logbook</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                </div>
                <div id="modalBody" class="modal-body">
                    <form role="form" id="frmAddVehicleLogbook" action="" method="post" class="registration-form" enctype="multipart/form-data">
                        <fieldset style="display: block;">
                            <div class="form-group">
                                <label class="label-info font-weight-bold">Destination Details</label>
                                <textarea type="text" id="input-destination-details" name="input-destination-details" class="form-control" placeholder="Key in for multiple destination. e.g: During site visit">{{ $booking->vehicle_log ? $booking->vehicle_log->destination_details : '' }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="label-info font-weight-bold">Actual Date From</label>
                                <div class="input-group date" id="datetimepicker_from" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_from" readonly/>
                                    <div class="input-group-append" data-target="#datetimepicker_from" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label-info font-weight-bold">Actual Date To</label>
                                <div class="input-group date" id="datetimepicker_to" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_to" readonly/>
                                    <div class="input-group-append" data-target="#datetimepicker_to" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label-info font-weight-bold">Total Mileage (KM)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">KM</span>
                                    </div>
                                    <input type="number" id="input-total-mileage" name="input-total-mileage" class="form-control" placeholder="Enter total mileage" value="{{ $booking->vehicle_log ? $booking->vehicle_log->total_mileage_per_trip : '0.00' }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label-info font-weight-bold">Total Touch 'n Go (RM)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">RM</span>
                                    </div>
                                    <input type="number" id="input-total-toll" name="input-total-toll" class="form-control" placeholder="Enter total Touch 'n Go" value="{{ $booking->vehicle_log ? $booking->vehicle_log->total_toll : '0.00' }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label-info font-weight-bold">Total Petrol (RM)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">RM</span>
                                    </div>
                                    <input type="number" id="input-total-petrol" name="input-total-petrol" class="form-control" placeholder="Enter total petrol" value="{{ $booking->vehicle_log ? $booking->vehicle_log->total_petrol : '0.00' }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label-info font-weight-bold">Odometer (KM)</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">KM</span>
                                    </div>
                                    <input type="number" id="input-odometer" name="input-odometer" class="form-control" placeholder="Enter vehicle's odometer reading" value="{{ $booking->vehicle_log ? $booking->vehicle_log->odometer : '0' }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="label-info font-weight-bold">Purchase of Petrol</label>
                                <label class="form-control radio-inline"><input type="radio" name="input-petrol-purchase-method" id="input-petrol-smart-pay" value="Smart Pay" {{ $booking->vehicle_log ? ($booking->vehicle_log->method_of_petrol_purchase == 'Smart Pay' ? 'checked' : '') : '' }}>Smart Pay</label>
                                <label class="form-control radio-inline"><input type="radio" name="input-petrol-purchase-method" id="input-petrol-cash" value="Cash" {{ $booking->vehicle_log ? ($booking->vehicle_log->method_of_petrol_purchase == 'Cash' ? 'checked' : '') : '' }}>Cash</label>
                            </div>
                            <input id="selectedDateFrom" value="{{ $booking->vehicle_log ? $booking->vehicle_log->actual_date_from :  $booking->car_booking_date_from }}" type="hidden"/>
                            <input id="selectedDateTo" value="{{ $booking->vehicle_log ? $booking->vehicle_log->actual_date_to : $booking->car_booking_date_to }}" type="hidden"/>
                        </fieldset>
                    </form>
                    <div class="row">
                        <div class="col-xl">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="label-info font-weight-bold">Receipt of petrol</label>
                                        <div class="row mt-2">
                                            <div class="col">
                                                <form role="form" id="frmReceiptUpload" action="" method="post" class="registration-form" enctype="multipart/form-data">
                                                    <fieldset style="display: block;">
                                                        <div class="form-group">
                                                            <label class="label-info">File Name</label>
                                                            <input type="text" id="input-file-name" name="input-file-name" class="form-control" placeholder="Enter file name"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="label-info">Upload file</label>
                                                            <input type="file" id="input-file" name="input-file" class="form-control-file"/>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                                <button id="btn-add" class="btn btn-success mb-5"><i class="fa fa-plus-circle"></i> Add</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col" style="overflow-x: auto;">
                                                <table id="dtFile" class="datatable table display table-responsive-lg" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>File Name</th>
                                                        <th>Uploaded File</th>
                                                    </tr>
                                                    </thead>
                                                    <tfoot>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>File Name</th>
                                                        <th>Uploaded File</th>
                                                    </tr>
                                                    </tfoot>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="btn-confirm" class="btn btn-primary" data-id="">Confirm</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let dt = $('#dtFile').DataTable({
                processing : true,
                serverSide : true,
                lengthChange: false,
                info: false,
                pagingType : "full_numbers",
                paging : true,
                searching : false,
                lengthMenu : [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                ajax : {
                    url: '{{ route('vehicle.api.get_vehicle_log_files') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'booking_id': '{{ $booking->id }}'
                    },
                    failure: function ( result ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Failed loading the table!',
                        })
                    }
                },
                columns : [
                    {
                        data: 'id',
                        width : '10%',
                        searchable : false,
                        orderable : false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {data: 'file_title'},
                    {
                        data: 'file_name',
                        searchable: false,
                        orderable: false,
                        render : function(data,type,full,meta) {
                            let url = '{{ asset('storage/:id') }}';
                            url = url.replace(':id', data);

                            let link = "<a href='"+ url +"' class='badge badge-primary' target='_blank'>View</a> | <a href='#' class='badge badge-danger link-del'>Delete</a>";

                            return link;
                        }
                    }
                ],
                error: function(e) {
                    console.log(e.responseText);
                }
            });

            $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
                format: 'DD/MM/YYYY h:mm A',
                ignoreReadonly: true
            });

            let dateFrom = $('#selectedDateFrom').val();
            let dateTo = $('#selectedDateTo').val();

            $('#datetimepicker_from').datetimepicker('date', moment(dateFrom).format('DD/MM/YYYY h:mm A'));
            $('#datetimepicker_to').datetimepicker('date', moment(dateTo).format('DD/MM/YYYY h:mm A'));

            $('#datetimepicker_from').on('change.datetimepicker', function (e) {
                $('#selectedDateFrom').val($("#datetimepicker_from").datetimepicker('date').format());
            });

            $('#datetimepicker_to').on('change.datetimepicker', function (e) {
                $('#selectedDateTo').val($("#datetimepicker_to").datetimepicker('date').format());
            });

            $('#btn-logbook').on('click', function () {
                $('#vehicleLogbookModal').modal('show');
            });

            $('#btn-job-done').on('click', function () {
                if ( moment('{{ \Carbon\Carbon::parse($booking->car_booking_date_to) }}') > moment() ) {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You will be available in the driver's list for new booking application",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, job done!',
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                method: "POST",
                                url: "{{ route('vehicle.api.job_done') }}",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    bookingId: '{{ $booking->id }}',
                                    newDateTo: "{{ \Carbon\Carbon::now()->format('Y-m-d H:i:s') }}"
                                }
                            })
                                .done(function( msg ) {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Job marked as done',
                                        showConfirmButton: false,
                                        timer: 1000
                                    }).then(function () {
                                        window.location.reload();
                                    })
                                })
                                .fail(function( msg ) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Something went wrong!',
                                    })
                                });
                        }
                    })
                } else {
                    window.location.reload();
                }

            });

            $('#dtFile tbody').on('click', '.link-del', function (e) {
                e.preventDefault();
                let row = dt.row($(this).closest('tr')).data();

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('vehicle.api.delete_vehicle_log_file') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                log_file_id: row.id,
                            }
                        })
                            .done(function( msg ) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'File has been deleted.',
                                    showConfirmButton: false,
                                    timer: 1000
                                }).then(function () {
                                    dt.ajax.reload();
                                })
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-add').on('click', function () {
                if ($('#frmReceiptUpload').valid()) {
                    let formData = new FormData();
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('booking_id', '{{ $booking->id }}');
                    formData.append('input-file-name', $('#input-file-name').val());
                    formData.append('file', $('input[type=file]')[0].files[0]);

                    $.ajax({
                        method: 'post',
                        url: '{{ route('vehicle.api.upload_vehicle_log_files') }}',
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        data: formData,
                    })
                        .done(function( msg ){
                            Swal.fire({
                                icon: 'success',
                                title: 'File uploaded successfully!',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function () {
                                dt.ajax.reload();
                            })
                        })
                        .fail(function( msg ) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        });
                }
            });

            $('#frmReceiptUpload').validate({
                rules: {
                    'input-file-name': {
                        required: true
                    },
                    'input-file': {
                        required: true
                    },
                }
            });

            $('#btn-confirm').on('click', function () {
                if ($('#frmAddVehicleLogbook').valid()) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('vehicle.api.save_vehicle_logbook') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            booking_id: '{{ $booking->id }}',
                            dateFrom: moment($('#selectedDateFrom').val()).format('YYYY-MM-DD HH:mm:00'),
                            dateTo: moment($('#selectedDateTo').val()).format('YYYY-MM-DD HH:mm:00'),
                            form: $('#frmAddVehicleLogbook').serialize()
                        }
                    })
                        .done(function( msg ) {
                            $('.modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Vehicle log book has been saved',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function () {
                                window.location.replace("{{ route('vehicle.jobs.view',$booking->id) }}");
                            })
                        })
                        .fail(function( msg ) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        });
                }
            });

            $('#frmAddVehicleLogbook').validate({
                rules: {
                    'input-total-mileage': {
                        required: true
                    },
                },
                errorPlacement: function(error, element)
                {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else { // This is the default behavior
                        error.insertAfter( element );
                    }
                }
            });
        });
    </script>
@endsection
