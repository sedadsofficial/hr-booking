@extends('emails._master')

@section('content')
    <div style="Margin-left: 20px;Margin-right: 20px;">
        <div style="mso-line-height-rule: exactly;mso-text-raise: 11px;vertical-align: middle;">
            <h1 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #565656;font-size: 30px;line-height: 38px;text-align: center;">
                <strong>SEDA Booking System</strong>
            </h1><p style="Margin-top: 20px;Margin-bottom: 0;">&nbsp;</p>
            <p style="Margin-top: 20px;Margin-bottom: 0;">
                Dear {{ $user->name }},
            </p>
            <p style="Margin-top: 20px;Margin-bottom: 0;">
                Please be informed that below booking has been cancelled:
            </p>
            <p style="Margin-top: 20px;Margin-bottom: 20px;">
                <strong>Purpose Detail :</strong> {{ $booking->purpose_detail }}<br/>
                <strong>Name of Applicant :</strong> {{ $user->name }}<br/>
                <strong>Division/ Unit :</strong> {{ $user->division->name }}<br/>
                <strong>Booking Date : </strong>{{ $booking->booking_date }}<br/>
                <strong>Booking Time : </strong>{{ $booking->booking_time }}<br/>
                <strong>Vehicle : </strong>{{ $booking->vehicle->name }}<br/>
                <strong>Cancel Remark : </strong>{{ $activity->activity_remark }}<br/><br/>
            </p>
        </div>
    </div>
@endsection
