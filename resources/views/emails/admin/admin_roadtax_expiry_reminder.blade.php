@extends('emails._master')

@section('content')
    <div style="Margin-left: 20px;Margin-right: 20px;">
        <div style="mso-line-height-rule: exactly;mso-text-raise: 11px;vertical-align: middle;">
            <h1 style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #565656;font-size: 30px;line-height: 38px;text-align: center;">
                <strong>SEDA Booking System</strong>
            </h1><p style="Margin-top: 20px;Margin-bottom: 0;">&nbsp;</p>
            <p style="Margin-top: 20px;Margin-bottom: 0;">
                Dear HRA Administrator,
            </p>
            <p style="Margin-top: 20px;Margin-bottom: 0;">
                Please be reminded that insurance and roadtax for Car <strong>{{ $v->name }} ({{ $v->plate_number }})</strong> will be expired on <strong>{{ \Carbon\Carbon::parse($v->roadtax_expiry_date)->format('d-m-Y') }}</strong>.
            </p>
            <p style="Margin-top: 20px;Margin-bottom: 20px;">
                Thank you<br/><br/>
            </p>
        </div>
    </div>
@endsection
