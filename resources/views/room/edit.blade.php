@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Meeting Room</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Booking Details</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div id="datePickDiv" class="collapse show">
                            <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                <fieldset style="display: block;">
                                    <div class="form-bottom">
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">From</label>
                                            <div class="input-group date" id="datetimepicker_from" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_from" readonly/>
                                                <div class="input-group-append" data-target="#datetimepicker_from" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">To</label>
                                            <div class="input-group date" id="datetimepicker_to" data-target-input="nearest">
                                                <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_to" readonly/>
                                                <div class="input-group-append" data-target="#datetimepicker_to" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Title</label>
                                            <input type="text" id="input-title" name="input-title" class="form-control-plaintext" placeholder="Enter meeting title" value="{{ $booking->meeting_desc }}" readonly/>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Room</label>
                                            <input type="text" id="input-room" name="input-room" class="form-control-plaintext" value="{{ $booking->room->name }}" readonly/>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Type of Setup</label>
                                            <input type="text" id="input-setup" name="input-setup" class="form-control-plaintext" placeholder="Enter meeting title" value="{{ $booking->room_setup->name }}" readonly/>
                                            {{--{!! Form::select('input-setup', $setup, $booking->room_setup_id, ['class' => 'form-control-plaintext', 'disabled'=>'true']) !!}--}}
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Chairperson</label>
                                            <input type="text" id="input-chairperson" name="input-chairperson" class="form-control-plaintext" placeholder="Enter chairperson's name" value="{{ $booking->chairperson }}" readonly/>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">No. of Pax</label>
                                            <input type="number" id="input-paxnum" name="input-paxnum" min="0" oninput="validity.valid||(value='');" class="form-control" value="{{ $booking->pax_no }}">
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Catering</label>
                                            <input type="text" id="input-catering" name="input-catering" class="form-control-plaintext"  value="{{ $booking->has_catering ? 'Yes' : 'No' }}" readonly/>
                                            {{--<label class="form-control radio-inline"><input type="radio" name="input-catering" value="1" {{ $booking->has_catering ? 'checked' : '' }} disabled>Yes</label>--}}
                                            {{--<label class="form-control radio-inline"><input type="radio" name="input-catering" value="0" {{ $booking->has_catering ? '' : 'checked' }} disabled>No</label>--}}
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Details of Food</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Budget Line (Grant/Division)</label>
                                            <input type="text" id="input-budgetline" name="input-budgetline" class="form-control-plaintext" placeholder="Enter which grant/division" value="{{ $booking->budget_line }}" readonly/>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info font-weight-bold">Remarks</label>
                                            <textarea type="text" id="input-remark" name="input-remark" class="form-control-plaintext" placeholder="-" readonly>{{ $booking->remarks }}</textarea>
                                        </div>
                                    </div>
                                    <input id="selectedDateFrom" value="{{ $booking->meeting_date_from }}" type="hidden"/>
                                    <input id="selectedDateTo" value="{{ $booking->meeting_date_to }}" type="hidden"/>
                                </fieldset>
                            </form>
                            <button id="btn-next" type="button" class="btn btn-primary" data-target="#datePickDiv">Next</button>
                        </div>
                        <div id="roomPickDiv" class="collapse">
                            <table id="dtRoom" class="datatable table display" cellspacing="0" style="table-layout: auto; width: 100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Room</th>
                                    <th>Capacity</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Room</th>
                                    <th>Capacity</th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                            <button id="btn-prev" type="button" class="btn btn-primary">Back</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
                format: 'DD/MM/YYYY h:mm A',
                ignoreReadonly: true
            });

            let dateFrom = $('#selectedDateFrom').val();
            let dateTo = $('#selectedDateTo').val();

            $('#datetimepicker_from').datetimepicker('date', moment(dateFrom).format('DD/MM/YYYY h:mm A'));
            $('#datetimepicker_to').datetimepicker('date', moment(dateTo).format('DD/MM/YYYY h:mm A'));

            $('#datetimepicker_from').on('change.datetimepicker', function (e) {
                $('#selectedDateFrom').val($("#datetimepicker_from").datetimepicker('date').format());
            });

            $('#datetimepicker_to').on('change.datetimepicker', function (e) {
                $('#selectedDateTo').val($("#datetimepicker_to").datetimepicker('date').format());
            });

            $('#btn-next').on('click', function () {
                $('#datePickDiv').collapse('hide');
                $('#roomPickDiv').collapse('show');
            });

            $('#btn-prev').on('click', function () {
                $('#roomPickDiv').collapse('hide');
                $('#datePickDiv').collapse('show');
            });

            $('#roomPickDiv').on('show.bs.collapse', function () {
                let url = '{{ route('room.api.get_available_room') }}';
                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateTo').val();
                $("#dtRoom").DataTable().destroy();
                $('#dtRoom').DataTable({
                    scrollX: true,
                    processing : true,
                    serverSide : true,
                    pagingType : "full_numbers",
                    paging : true,
                    searching : false,
                    lengthMenu : [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                    ajax : {
                        url: url,
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            selectedDateFrom : moment(dateFrom).subtract(10,'m').format('YYYY-MM-DD HH:mm:00'),
                            selectedDateTo : moment(dateTo).add(10,'m').format('YYYY-MM-DD HH:mm:00'),
                            excludedId : '{{ $booking->id }}'
                        },
                        failure: function ( result ) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        }
                    },
                    columns : [
                        {
                            data: 'id',
                            width : '10%',
                            searchable : false,
                            orderable : false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: 'name'},
                        {
                            data: 'capacity',
                            width : '10%',
                        },
                        {
                            data: 'id',
                            width:'20%',
                            searchable: false,
                            orderable: false,
                            render : function(data,type,full,meta) {
                                return "<input type='button' class='btn btn-primary btn-sm pickRoom' value='Select'/>"
                            }
                        }
                    ],
                    error: function(e) {
                        console.log(e.responseText);
                    }
                });
            });

            $('#dtRoom tbody').on('click', '.pickRoom', function () {
                let row = $(this).closest('tr');
                let id = $('#dtRoom').DataTable().row( row ).data()["id"];
                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateTo').val();
                $.ajax({
                    method: "POST",
                    url: "{{ route('room.api.edit_room_booking') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        room_id: id,
                        booking_id: '{{ $booking->id }}',
                        dateFrom: moment(dateFrom).format('YYYY-MM-DD HH:mm:00'),
                        dateTo: moment(dateTo).format('YYYY-MM-DD HH:mm:00'),
                        form: $('#frmSubmission').serializeArray()
                    }
                })
                    .done(function( msg ) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Your edit has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function () {
                            window.location.replace("{{ route('room.mybooking') }}");
                        })
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });
        });
    </script>
@endsection
