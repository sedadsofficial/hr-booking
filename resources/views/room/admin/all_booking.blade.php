@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Meeting Room</a></li>
    <li class="breadcrumb-item active" aria-current="page">All Booking</li>
@endsection

@section('content')
    <div style="overflow-x: auto; width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">All Booking</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2" style="overflow-x: auto;">
                        <table id="dtMyBooking" class="datatable table display" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Booking No</th>
                                <th>Booking Title</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Room</th>
                                <th>Booking Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Booking No</th>
                                <th>Booking Title</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Room</th>
                                <th>Booking Status</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let table = $('#dtMyBooking').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                ajax: {
                    url: '{{ route('room.admin.api.get_all_booking') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                order: [
                    [3,'desc']
                ],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data) {
                            return 'RB' + data;
                        }
                    },
                    {
                        data: 'meeting_desc',
                        orderable: false,
                    },
                    {
                        data: 'meeting_date_from',
                        render: function (data) {
                            return moment(data).format('DD/MM/YYYY h:mm A');
                        }
                    },
                    {
                        data: 'meeting_date_to',
                        render: function (data) {
                            return moment(data).format('DD/MM/YYYY h:mm A');
                        }
                    },
                    {
                        data: 'name',
                        orderable: false,
                    },
                    {data: 'booking_status'},
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            let button = "<input type='button' class='btn btn-primary btn-sm view' value='View'/>";

                            if (row.is_allowed_to_cancel) {
                                button += "&nbsp<input type='button' class='btn btn-warning btn-sm cancel' value='Cancel'/>";
                            }

                            if(row.is_allowed_to_delete) {
                                button += "&nbsp<input type='button' class='btn btn-danger btn-sm delete' value='Delete'/>";
                            }
                            return button;
                        }
                    }
                ],
            });

            $('#dtMyBooking tbody').on('click', '.view', function () {
                let row = $(this).closest('tr');
                let id = $('#dtMyBooking').DataTable().row( row ).data()["id"];
                let url = '{{ url('room/admin/view/:id') }}';
                url = url.replace(':id', id);
                window.location.href = url;
            });

            $('#dtMyBooking tbody').on('click', '.delete', function () {
                var row = $(this).closest('tr');
                var id = $('#dtMyBooking').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.api.delete_room_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Deleted!',
                                    'Your booking has been deleted.',
                                    'success'
                                );
                                table.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#dtMyBooking tbody').on('click', '.cancel', function () {
                let row = $(this).closest('tr');
                let id = $('#dtMyBooking').DataTable().row( row ).data()["id"];

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, cancel it!',
                    input: 'textarea',
                    inputPlaceholder: 'Type your reason here...',
                    inputAttributes: {
                        'aria-label': 'Type your reason here'
                    },
                    inputValidator: (value) => {
                        if (!value) {
                            return 'You need to write something!'
                        }
                    }
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.api.cancel_room_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                                remarks: result.value
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Cancelled!',
                                    'Booking has been cancelled.',
                                    'success'
                                );
                                table.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

        });
    </script>
@endsection
