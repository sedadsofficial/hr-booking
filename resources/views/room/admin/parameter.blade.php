@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Meeting Room</a></li>
    <li class="breadcrumb-item active" aria-current="page">Parameter Management</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Parameter Management</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="row no-gutters">
                            <div class="nav flex-column nav-pills pr-4 border-right" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-rooms-tab" data-toggle="pill" href="#v-pills-rooms" role="tab" aria-controls="v-pills-rooms" aria-selected="true">Rooms</a>
                                <a class="nav-link" id="v-pills-rooms-setup-tab" data-toggle="pill" href="#v-pills-rooms-setup" role="tab" aria-controls="v-pills-rooms-setup" aria-selected="false">Room Setup</a>
                                <a class="nav-link" id="v-pills-divisions-tab" data-toggle="pill" href="#v-pills-divisions" role="tab" aria-controls="v-pills-divisions" aria-selected="false">Divisions</a>
                                <a class="nav-link" id="v-pills-food-setup-tab" data-toggle="pill" href="#v-pills-food-setup" role="tab" aria-controls="v-pills-food-setup" aria-selected="false">Food Setup</a>
                                <a class="nav-link" id="v-pills-foods-tab" data-toggle="pill" href="#v-pills-foods" role="tab" aria-controls="v-pills-foods" aria-selected="false">Foods</a>
                                <a class="nav-link" id="v-pills-drinks-tab" data-toggle="pill" href="#v-pills-drinks" role="tab" aria-controls="v-pills-drinks" aria-selected="false">Drinks</a>
                            </div>
                            <div class="tab-content ml-4" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-rooms" role="tabpanel" aria-labelledby="v-pills-rooms-tab">
                                    <div class="flex-row-reverse ml-2 mb-2">
                                        <button id="btn-add-rooms" class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> Add</button>
                                    </div>
                                    <table id="dtRooms" class="datatable table display w-100" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Room Name</th>
                                                <th>Capacity</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>Room Name</th>
                                                <th>Capacity</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="v-pills-rooms-setup" role="tabpanel" aria-labelledby="v-pills-rooms-setup-tab">
                                    <div class="flex-row-reverse ml-2 mb-2">
                                        <button id="btn-add-room-setups" class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> Add</button>
                                    </div>
                                    <table id="dtRoomSetup" class="datatable table display w-100" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="v-pills-divisions" role="tabpanel" aria-labelledby="v-pills-divisions-tab">
                                    <div class="flex-row-reverse ml-2 mb-2">
                                        <button id="btn-add-divisions" class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> Add</button>
                                    </div>
                                    <table id="dtDivisions" class="datatable table display w-100" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="v-pills-food-setup" role="tabpanel" aria-labelledby="v-pills-food-setup-tab">
                                    <div class="flex-row-reverse ml-2 mb-2">
                                        <button id="btn-add-food-setups" class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> Add</button>
                                    </div>
                                    <table id="dtFoodSetup" class="datatable table display w-100" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="v-pills-foods" role="tabpanel" aria-labelledby="v-pills-foods-tab">
                                    <div class="flex-row-reverse ml-2 mb-2">
                                        <button id="btn-add-foods" class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> Add</button>
                                    </div>
                                    <table id="dtFoods" class="datatable table display w-100" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="v-pills-drinks" role="tabpanel" aria-labelledby="v-pills-drinks-tab">
                                    <div class="flex-row-reverse ml-2 mb-2">
                                        <button id="btn-add-drinks" class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> Add</button>
                                    </div>
                                    <table id="dtDrinks" class="datatable table display w-100" width="100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- Edit Rooms Modal -->
                        <div id="roomsModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info">Room Name</label>
                                                        <input type="text" name="input-name" class="form-control input-name" placeholder="Enter room name"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info">Capacity</label>
                                                        <input type="text" name="input-capacity" class="form-control input-capacity" placeholder="Enter capacity"/>
                                                    </div>
                                                </div>
                                                <input id="roomId" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Edit Rooms Setups Modal -->
                        <div id="roomSetupsModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info">Name</label>
                                                        <input type="text" name="input-name" class="form-control input-name" placeholder="Enter room setup name"/>
                                                    </div>
                                                </div>
                                                <input id="dataId" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Edit Divisions Modal -->
                        <div id="divisionsModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info">Name</label>
                                                        <input type="text" name="input-name" class="form-control input-name" placeholder="Enter division name"/>
                                                    </div>
                                                </div>
                                                <input class="data-id" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Edit Food Setups Modal -->
                        <div id="foodSetupsModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info">Name</label>
                                                        <input type="text" name="input-name" class="form-control input-name" placeholder="Enter food setup name"/>
                                                    </div>
                                                </div>
                                                <input class="data-id" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Edit Foods Modal -->
                        <div id="foodsModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info">Name</label>
                                                        <input type="text" name="input-name" class="form-control input-name" placeholder="Enter food name"/>
                                                    </div>
                                                </div>
                                                <input class="data-id" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Edit Drinks Modal -->
                        <div id="drinksModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info">Name</label>
                                                        <input type="text" name="input-name" class="form-control input-name" placeholder="Enter drink name"/>
                                                    </div>
                                                </div>
                                                <input class="data-id" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="btn-save" class="btn btn-primary save" data-id="">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let rooms = $('#dtRooms').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: false,
                lengthChange:false,
                ajax: {
                    url: '{{ route('room.admin.api.get_rooms_parameter') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                order: [1,'asc'],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                        orderable: false
                    },
                    {
                        data: 'capacity',
                        orderable: false
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type=\'button\' class=\'btn btn-warning btn-sm edit\' value=\'Edit\'/> <input type=\'button\' class=\'btn btn-danger btn-sm delete\' value=\'Delete\'/>';
                        }
                    }
                ],
            });

            let rooms_setup = $('#dtRoomSetup').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: false,
                lengthChange:false,
                ajax: {
                    url: '{{ route('room.admin.api.get_room_setup_parameter') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                order: [1,'asc'],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                        orderable: false
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type=\'button\' class=\'btn btn-warning btn-sm edit\' value=\'Edit\'/> <input type=\'button\' class=\'btn btn-danger btn-sm delete\' value=\'Delete\'/>';
                        }
                    }
                ],
            });

            let divisions = $('#dtDivisions').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: false,
                lengthChange:false,
                ajax: {
                    url: '{{ route('room.admin.api.get_divisions_parameter') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                order: [1,'asc'],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                        orderable: false
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type=\'button\' class=\'btn btn-warning btn-sm edit\' value=\'Edit\'/> <input type=\'button\' class=\'btn btn-danger btn-sm delete\' value=\'Delete\'/>';
                        }
                    }
                ],
            });

            let food_setup = $('#dtFoodSetup').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: false,
                lengthChange:false,
                ajax: {
                    url: '{{ route('room.admin.api.get_food_setup_parameter') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                order: [1,'asc'],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                        orderable: false
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type=\'button\' class=\'btn btn-warning btn-sm edit\' value=\'Edit\'/> <input type=\'button\' class=\'btn btn-danger btn-sm delete\' value=\'Delete\'/>';
                        }
                    }
                ],
            });

            let foods = $('#dtFoods').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: false,
                lengthChange:false,
                ajax: {
                    url: '{{ route('room.admin.api.get_foods_parameter') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                order: [1,'asc'],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                        orderable: false
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type=\'button\' class=\'btn btn-warning btn-sm edit\' value=\'Edit\'/> <input type=\'button\' class=\'btn btn-danger btn-sm delete\' value=\'Delete\'/>';
                        }
                    }
                ],
            });

            let drinks = $('#dtDrinks').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                paging: true,
                searching: false,
                lengthChange:false,
                ajax: {
                    url: '{{ route('room.admin.api.get_drinks_parameter') }}',
                    type: "POST",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                },
                order: [1,'asc'],
                columns: [
                    {
                        data: 'id',
                        searchable: false,
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: 'name',
                        orderable: false
                    },
                    {
                        data: 'id',
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type=\'button\' class=\'btn btn-warning btn-sm edit\' value=\'Edit\'/> <input type=\'button\' class=\'btn btn-danger btn-sm delete\' value=\'Delete\'/>';
                        }
                    }
                ],
            });

            /**
             * Rooms START
             */
            $('#dtRooms tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtRooms').DataTable().row( row ).data()["id"];
                let name = $('#dtRooms').DataTable().row( row ).data()["name"];
                let capacity = $('#dtRooms').DataTable().row( row ).data()["capacity"];

                $('#roomsModal .modal-title').html('Edit');
                $('#roomsModal .input-name').val(name);
                $('#roomsModal .input-capacity').val(capacity);
                $('#roomId').val(id);

                $('#roomsModal').modal();
            });

            $('#btn-add-rooms').on('click', function () {
                $('#roomsModal .modal-title').html('Add');
                $('#roomsModal .registration-form').trigger('reset');
                $('#roomsModal').modal();
            });

            $('#roomsModal .save').on('click', function() {
                let data = {
                    "_token": "{{ csrf_token() }}",
                    form: $('#roomsModal .registration-form').serializeArray()
                };

                if ($('#roomsModal .modal-title').html() == 'Edit') {
                    data = { ...data, room_id: $('#roomId').val() };
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('room.admin.api.save_room_parameter') }}",
                    data: data
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Data has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        rooms.ajax.reload();
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });

            $('#dtRooms tbody').on('click', '.delete', function () {
                var row = $(this).closest('tr');
                var id = $('#dtRooms').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.admin.api.delete_room_parameter') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                room_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Deleted!',
                                    'Data has been deleted.',
                                    'success'
                                );
                                room.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            /**
             * Room Setups START
             */
            $('#dtRoomSetup tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtRoomSetup').DataTable().row( row ).data()["id"];
                let name = $('#dtRoomSetup').DataTable().row( row ).data()["name"];

                $('#roomSetupsModal .modal-title').html('Edit');
                $('#roomSetupsModal .input-name').val(name);
                $('#dataId').val(id);

                $('#roomSetupsModal').modal();
            });

            $('#btn-add-room-setups').on('click', function () {
                $('#roomSetupsModal .modal-title').html('Add');
                $('#roomSetupsModal .registration-form').trigger('reset');
                $('#roomSetupsModal').modal();
            });

            $('#roomSetupsModal .save').on('click', function() {
                let data = {
                    "_token": "{{ csrf_token() }}",
                    form: $('#roomSetupsModal .registration-form').serializeArray()
                };

                if ($('#roomSetupsModal .modal-title').html() == 'Edit') {
                    data = { ...data, data_id: $('#dataId').val() };
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('room.admin.api.save_room_setup_parameter') }}",
                    data: data
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Data has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        rooms_setup.ajax.reload();
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });

            $('#dtRoomSetup tbody').on('click', '.delete', function () {
                var row = $(this).closest('tr');
                var id = $('#dtRoomSetup').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.admin.api.delete_room_setup_parameter') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                data_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire(
                                    'Deleted!',
                                    'Data has been deleted.',
                                    'success'
                                );
                                rooms_setup.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            /**
             * Divisions START
             */
            $('#dtDivisions tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtDivisions').DataTable().row( row ).data()["id"];
                let name = $('#dtDivisions').DataTable().row( row ).data()["name"];

                $('#divisionsModal .modal-title').html('Edit');
                $('#divisionsModal .input-name').val(name);
                $('#divisionsModal .data-id').val(id);

                $('#divisionsModal').modal();
            });

            $('#btn-add-divisions').on('click', function () {
                $('#divisionsModal .modal-title').html('Add');
                $('#divisionsModal .registration-form').trigger('reset');
                $('#divisionsModal').modal();
            });

            $('#divisionsModal .save').on('click', function() {
                let data = {
                    "_token": "{{ csrf_token() }}",
                    form: $('#divisionsModal .registration-form').serializeArray()
                };

                if ($('#divisionsModal .modal-title').html() == 'Edit') {
                    data = { ...data, data_id: $('#divisionsModal .data-id').val() };
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('room.admin.api.save_divisions_parameter') }}",
                    data: data
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Data has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        divisions.ajax.reload();
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });

            $('#dtDivisions tbody').on('click', '.delete', function () {
                var row = $(this).closest('tr');
                var id = $('#dtDivisions').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.admin.api.delete_divisions_parameter') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                data_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Data has been deleted',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                divisions.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            /**
             * Food Setups START
             */
            $('#dtFoodSetup tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtFoodSetup').DataTable().row( row ).data()["id"];
                let name = $('#dtFoodSetup').DataTable().row( row ).data()["name"];

                $('#foodSetupsModal .modal-title').html('Edit');
                $('#foodSetupsModal .input-name').val(name);
                $('#foodSetupsModal .data-id').val(id);

                $('#foodSetupsModal').modal();
            });

            $('#dtFoodSetup tbody').on('click', '.delete', function () {
                var row = $(this).closest('tr');
                var id = $('#dtFoodSetup').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.admin.api.delete_food_setup_parameter') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                data_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Data has been deleted',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                food_setup.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-add-food-setups').on('click', function () {
                $('#foodSetupsModal .modal-title').html('Add');
                $('#foodSetupsModal .registration-form').trigger('reset');
                $('#foodSetupsModal').modal();
            });

            $('#foodSetupsModal .save').on('click', function() {
                let data = {
                    "_token": "{{ csrf_token() }}",
                    form: $('#foodSetupsModal .registration-form').serializeArray()
                };

                if ($('#foodSetupsModal .modal-title').html() == 'Edit') {
                    data = { ...data, data_id: $('#foodSetupsModal .data-id').val() };
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('room.admin.api.save_food_setup_parameter') }}",
                    data: data
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Data has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        food_setup.ajax.reload();
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });


            /**
             * Foods START
             */
            $('#dtFoods tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtFoods').DataTable().row( row ).data()["id"];
                let name = $('#dtFoods').DataTable().row( row ).data()["name"];

                $('#foodsModal .modal-title').html('Edit');
                $('#foodsModal .input-name').val(name);
                $('#foodsModal .data-id').val(id);

                $('#foodsModal').modal();
            });

            $('#dtFoods tbody').on('click', '.delete', function () {
                let row = $(this).closest('tr');
                let id = $('#dtFoods').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.admin.api.delete_foods_parameter') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                data_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Data has been deleted',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                foods.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-add-foods').on('click', function () {
                $('#foodsModal .modal-title').html('Add');
                $('#foodsModal .registration-form').trigger('reset');
                $('#foodsModal').modal();
            });

            $('#foodsModal .save').on('click', function() {
                let data = {
                    "_token": "{{ csrf_token() }}",
                    form: $('#foodsModal .registration-form').serializeArray()
                };

                if ($('#foodsModal .modal-title').html() == 'Edit') {
                    data = { ...data, data_id: $('#foodsModal .data-id').val() };
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('room.admin.api.save_foods_parameter') }}",
                    data: data
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Data has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        foods.ajax.reload();
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });

            /**
             * Drinks START
             */
            $('#dtDrinks tbody').on('click', '.edit', function () {
                let row = $(this).closest('tr');
                let id = $('#dtDrinks').DataTable().row( row ).data()["id"];
                let name = $('#dtDrinks').DataTable().row( row ).data()["name"];

                $('#drinksModal .modal-title').html('Edit');
                $('#drinksModal .input-name').val(name);
                $('#drinksModal .data-id').val(id);

                $('#drinksModal').modal();
            });

            $('#dtDrinks tbody').on('click', '.delete', function () {
                var row = $(this).closest('tr');
                var id = $('#dtDrinks').DataTable().row( row ).data()["id"];
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.admin.api.delete_drinks_parameter') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                data_id: id,
                            }
                        })
                            .done(function( msg ) {
                                $('.modal').modal('hide');
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Data has been deleted',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                drinks.ajax.reload();
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-add-drinks').on('click', function () {
                $('#drinksModal .modal-title').html('Add');
                $('#drinksModal .registration-form').trigger('reset');
                $('#drinksModal').modal();
            });

            $('#drinksModal .save').on('click', function() {
                let data = {
                    "_token": "{{ csrf_token() }}",
                    form: $('#drinksModal .registration-form').serializeArray()
                };

                if ($('#drinksModal .modal-title').html() == 'Edit') {
                    data = { ...data, data_id: $('#drinksModal .data-id').val() };
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('room.admin.api.save_drinks_parameter') }}",
                    data: data
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Data has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        drinks.ajax.reload();
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });

        });
    </script>
@endsection
