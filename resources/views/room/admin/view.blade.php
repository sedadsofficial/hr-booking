@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Meeting Room</a></li>
    <li class="breadcrumb-item active" aria-current="page">View</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">View Booking Details</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Booking Information</h6>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-borderless table-responsive">
                    <tr>
                        <td><label class="label-info font-weight-bold">Booking Status</label></td>
                        <td><h5><span class="badge badge-secondary">{{ $booking->booking_status }}</span></h5></td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">From</label></td>
                        <td>{{ Carbon\Carbon::parse($booking->meeting_date_from)->format('d/m/Y h:i A')}}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">To</label></td>
                        <td>{{ Carbon\Carbon::parse($booking->meeting_date_to)->format('d/m/Y h:i A')}}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Title</label></td>
                        <td>{{ $booking->meeting_desc }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Room</label></td>
                        <td>{{ $booking->room->name }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Type of Setup</label></td>
                        <td>{{ $booking->room_setup->name }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Chairperson</label></td>
                        <td>{{ $booking->chairperson }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">No of Pax</label></td>
                        <td>{{ $booking->pax_no }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Catering</label></td>
                        <td>{{ $booking->catering ? 'Yes' : 'No' }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Remarks</label></td>
                        <td>{{ $booking->remarks }}</td>
                    </tr>
                </table>
                <div class="row">
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row p-3">
                                    <div class="col-sm text-nowrap font-weight-bold">Booked By :</div>
                                    <div class="col">
                                        <span class="font-italic">{{ $booking->user->name }}</span><br>
                                        <span>{{ $booking->user->division->name }}</span>
                                    </div>
                                </div>
                                <div class="row p-3">
                                    <div class="col-sm text-nowrap font-weight-bold">Booked On :</div>
                                    <div class="col">{{ Carbon\Carbon::parse($booking->created_at)->format('d/m/Y h:i A')}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if( $booking->catering || $booking->refreshment)
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Details of Food</h6>
            </div>
            <div class="card-body table-responsive">
                @if($booking->refreshment)
                <table class="table table-borderless table-responsive">
                    <tr>
                        <td><label class="label-info font-weight-bold">Food</label></td>
                        <td>{{ $booking->refreshment->food->name }}</td>
                    </tr>
                    <tr>
                        <td><label class="label-info font-weight-bold">Drink</label></td>
                        <td>{{ $booking->refreshment->drink->name }}</td>
                    </tr>
                </table>
                @endif

                @if($booking->catering)
                    <div class="row">
                        <div class="col-sm table-responsive">
                            <label class="label-info font-weight-bold">Food</label>
                            <table class="table table-borderless border rounded">
                                @foreach($booking->catering->catering_item->where('item_type','food') as $item)
                                    <tr>
                                        <td><label class="label-info font-weight-bold">Slot</label></td>
                                        <td>{{ $item->item_name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom rounded"><label class="label-info font-weight-bold">Remark</label></td>
                                        <td class="border-bottom rounded">{{ $item->item_remark }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="col-sm">
                            <label class="label-info font-weight-bold">Drink</label>
                            <table class="table table-borderless border rounded">
                                @foreach($booking->catering->catering_item->where('item_type','drink') as $item)
                                    <tr>
                                        <td><label class="label-info font-weight-bold">Slot</label></td>
                                        <td>{{ $item->item_name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="border-bottom rounded"><label class="label-info font-weight-bold">Remark</label></td>
                                        <td class="border-bottom rounded">{{ $item->item_remark }}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                    @if($booking->catering_quotation->count() > 0)
                        <div class="row">
                            <div class="col-sm">
                                <label class="label-info font-weight-bold">Caterer Info</label>
                                <div class="table border rounded">
                                    @foreach($booking->catering_quotation->sortByDesc('is_selected') as $item)
                                        <div class="border-bottom">
                                            <div class="row p-3">
                                                <div class="col-sm-3"><label class="label-info font-weight-bold">Caterer Name</label></div>
                                                <div class="col-sm">{{ $item->caterer_name }} @if($item->is_selected)<span class="badge badge-success">Selected</span>@endif</div>
                                            </div>
                                            <div class="row p-3">
                                                <div class="col-sm-3"><label class="label-info font-weight-bold">Cost (RM)</label></div>
                                                <div class="col-sm">{{ $item->cost }}</div>
                                            </div>
                                            <div class="row p-3">
                                                <div class="col-sm-3"><label class="label-info font-weight-bold">Quotation File</label></div>
                                                <div class="col-sm"><a href="{{ asset('storage/'.$item->file_name) }}" target="_blank">View</a></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <table class="table table-borderless table-responsive">
                            <tr>
                                <td><label class="label-info font-weight-bold">Food Setup</label></td>
                                <td>{{ $booking->catering->food_setup->name }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Budget Line</label></td>
                                <td>{{ $booking->catering->budget_line }}</td>
                            </tr>
                        </table>
                    </div>
                @endif
            </div>
        </div>
        @endif

        @if ($booking->activity()->count() > 0)
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Activity</h6>
            </div>
            <div class="card-body">
                @foreach ($booking->activity as $activity)
                <div class="row">
                    <div class="col">
                        <div class="border-bottom">
                            <div class="row p-3">
                                <div class="col-sm-2"><label class="label-info font-weight-bold">Action</label></div>
                                <div class="col"><h5><span class="badge badge-secondary">{{ $activity->activity_name }}</span></h5></div>
                            </div>
                            <div class="row p-3">
                                <div class="col-sm-2"><label class="label-info font-weight-bold">By</label></div>
                                <div class="col">{{ $activity->user->name }}</div>
                            </div>
                            <div class="row p-3">
                                <div class="col-sm-2"><label class="label-info font-weight-bold">On</label></div>
                                <div class="col">{{ Carbon\Carbon::parse($activity->created_at)->format('d/m/Y h:i A')}}</div>
                            </div>
                            <div class="row p-3">
                                <div class="col-sm-2"><label class="label-info font-weight-bold">Remark</label></div>
                                <div class="col">{{ empty($activity->activity_remark) ? '-' : $activity->activity_remark }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endif

        <div class="mt-5 mb-5">
            @if ($booking->is_allowed_to_cancel)
                <input id="btn-cancel-room-booking" class="btn btn-warning cancel" type="button" value="Cancel"/>
            @endif
            @if ($booking->is_allowed_to_delete)
                <input id="btn-delete-room-booking" class="btn btn-danger delete" type="button" value="Delete"/>
            @endif
            @if ($booking->is_allowed_to_verify && $booking->booking_status != config('constant.status.PENDING_CATERER_CONFIRMATION'))
                <input id="btn-verify-room-booking" class="btn btn-success verify" type="button" value="Verify"/>
            @endif
        </div>
    </div>

    <!-- Pending Approval Modal -->
    <div id="verifyPendingApprovalModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modalTitle" class="modal-title">Verification</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                </div>
                <div id="modalBody" class="modal-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Action</td>
                            <td>
                                <select id="sel-action" class="custom-select">
                                    <option value="1" selected>Approve</option>
                                    <option value="0">Reject</option>
                                </select>
                            </td>
                        </tr>
                        <tr id="remark-tr" style="display: none">
                            <td>Remark</td>
                            <td>
                                <form role="form" id="frmVerifyPendingApproval" action="" method="post" class="registration-form">
                                    <fieldset style="display: block;">
                                        <textarea type="text" id="input-remark" name="input-remark" class="form-control" placeholder="Enter your reason..."></textarea>
                                    </fieldset>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="btn-confirm" class="btn btn-primary" data-id="">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    @if($booking->catering)
    <!-- Pending Caterer Quotation Modal -->
    <div id="verifyPendingCatererQuotationModal" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modalTitle" class="modal-title">Verification</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                </div>
                <div id="modalBody" class="modal-body">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-quotation-tab" data-toggle="tab" href="#nav-quotation" role="tab" aria-controls="nav-quotation" aria-selected="true">Quotation</a>
                            <a class="nav-item nav-link" id="nav-food-details-tab" data-toggle="tab" href="#nav-food-details" role="tab" aria-controls="nav-food-details" aria-selected="false">Food Details</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-quotation" role="tabpanel" aria-labelledby="nav-quotation-tab">
                            <div class="row mt-2">
                                <div class="col">
                                    <form role="form" id="frmVerifyPendingCatererQuotation" action="" method="post" class="registration-form" enctype="multipart/form-data">
                                        <fieldset style="display: block;">

                                        <div class="form-group">
                                            <label class="label-info">Caterer Name</label>
                                            <input type="text" id="input-caterer-name" name="input-caterer-name" class="form-control" placeholder="Enter caterer's name"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info">Cost (RM)</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">RM</span>
                                                </div>
                                                <input type="number" id="input-caterer-cost" name="input-caterer-cost" class="form-control" placeholder="Enter cost"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-info">Quotation file</label>
                                            <input type="file" id="input-caterer-quotation" name="input-caterer-quotation" class="form-control-file"/>
                                        </div>
                                    </fieldset>
                                </form>
                                <button id="btn-add" class="btn btn-success mb-5"><i class="fa fa-plus-circle"></i> Add</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="overflow-x: auto;">
                                <table id="dtQuotation" class="datatable table display table-responsive-lg" width="100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Caterer Name</th>
                                        <th>Cost</th>
                                        <th>Quotation File</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Caterer Name</th>
                                        <th>Cost</th>
                                        <th>Quotation File</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-food-details" role="tabpanel" aria-labelledby="nav-food-details-tab">
                        <div class="mt-2">
                            <form role="form" id="frmEditFood" action="" method="post" class="registration-form">
                                <fieldset style="display: block;">
                                    <div class="form-bottom">
                                        <div class="row">
                                            <div class="col-sm table-responsive">
                                                <label class="label-info font-weight-bold">Food</label>
                                                <table class="table table-borderless border rounded">
                                                    @foreach($booking->catering->catering_item->where('item_type','food') as $item)
                                                        <tr>
                                                            <td><label class="label-info font-weight-bold">Slot</label></td>
                                                            <td>{{ $item->item_name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="border-bottom rounded"><label class="label-info font-weight-bold">Remark</label></td>
                                                            <td class="border-bottom rounded"><input type="text" name="input-food[{{ $item->id }}]" class="form-control" value="{{ $item->item_remark }}"/></td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                            <div class="col-sm">
                                                <label class="label-info font-weight-bold">Drink</label>
                                                <table class="table table-borderless border rounded">
                                                    @foreach($booking->catering->catering_item->where('item_type','drink') as $item)
                                                        <tr>
                                                            <td><label class="label-info font-weight-bold">Slot</label></td>
                                                            <td>{{ $item->item_name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="border-bottom rounded"><label class="label-info font-weight-bold">Remark</label></td>
                                                            <td class="border-bottom rounded"><input type="text" name="input-drink[{{ $item->id }}]" class="form-control" value="{{ $item->item_remark }}"/></td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label class="label-info font-weight-bold">Food Setup</label>
                                                    @foreach($foodSetup as $key => $value)
                                                        <label class="form-control radio-inline"><input type="radio" name="input-foodsetup" value="{{ $key }}" {{ ($booking->catering->food_setup->id == $key) ? 'checked' : '' }}>{{ $value }}</label>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label class="label-info font-weight-bold">Budget Line (Grant/Division)</label>
                                                    <input type="text" id="input-budgetline" name="input-budgetline" class="form-control" placeholder="Enter which grant/division" value="{{ $booking->catering->budget_line }}"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btn-confirm-pending-caterer" class="btn btn-primary" data-id="">Confirm</button>
                <button id="btn-reject-pending-caterer" class="btn btn-danger" data-id="">Reject</button>
            </div>
        </div>
    </div>
</div>
@endif
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let hasSelectedQuotation = false;

            @if ($booking->catering)
                let dt = $('#dtQuotation').DataTable({
                        processing : true,
                        serverSide : true,
                        lengthChange: false,
                        info: false,
                        pagingType : "full_numbers",
                        paging : true,
                        searching : false,
                        lengthMenu : [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                        ajax : {
                            url: '{{ route('room.admin.api.get_caterer_quotation') }}',
                            type: "POST",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                'catering_id': '{{ $booking->catering->id }}'
                            },
                            failure: function ( result ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Failed loading the table!',
                                })
                            }
                        },
                        columns : [
                            {
                                data: 'id',
                                width : '10%',
                                searchable : false,
                                orderable : false,
                                render: function (data, type, row, meta) {
                                    return meta.row + meta.settings._iDisplayStart + 1;
                                }
                            },
                            {data: 'caterer_name'},
                            {
                                data: 'cost',
                                width : '10%',
                            },
                            {
                                data: 'file_name',
                                searchable: false,
                                orderable: false,
                                render : function(data,type,full,meta) {
                                    let url = '{{ asset('storage/:id') }}';
                                    url = url.replace(':id', data);

                                    let link = "<a href='"+ url +"' class='badge badge-primary' target='_blank'>View</a> | <a href='#' class='badge badge-danger link-del'>Delete</a>";

                                    if (full.is_selected == '0') {
                                        link += " | <a href='#' class='badge badge-secondary link-use'>Use this quotation</a>";
                                    } else {
                                        link += " | <span class='badge badge-pill badge-light'>Selected</span>";
                                    }
                                    return link;
                                }
                            }
                        ],
                        error: function(e) {
                            console.log(e.responseText);
                        }
                    });

                dt.on('draw', function(){
                    dt.data().each( function (d) {
                        if (d.is_selected == '1') {
                            hasSelectedQuotation = true;
                            return false;
                        }
                    });
                });

                $('#btn-confirm-pending-caterer').on('click', function (){
                    if (dt.data().count() > 0) {
                        if (hasSelectedQuotation) {
                            Swal.fire({
                                title: 'Are you sure?',
                                text: "You won't be able to revert this!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, proceed!',
                            }).then((result) => {
                                if (result.value) {
                                    ajaxVerifyPendingCatererQuotation('confirm');
                                }
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Select one quotation from the table to proceed!',
                            });
                        }
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Add at least one (1) caterer quotation!',
                        });
                    }
                });

                $('#btn-reject-pending-caterer').on('click', function (){
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, reject it!',
                        input: 'textarea',
                        inputPlaceholder: 'Type your reason here...',
                        inputAttributes: {
                            'aria-label': 'Type your reason here'
                        },
                        inputValidator: (value) => {
                            if (!value) {
                                return 'You need to write something!'
                            }
                        }
                    }).then((result) => {
                        if (result.value) {
                            ajaxVerifyPendingCatererQuotation('reject', result.value);
                        }
                    });
                });

                function ajaxVerifyPendingCatererQuotation(action, remark='') {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('room.admin.api.verify_pending_quotation') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            booking_id: '{{ $booking->id }}',
                            action: action,
                            remark: remark,
                            form: $('#frmEditFood').serializeArray()
                        }
                    })
                        .done(function( msg ) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Booking has been saved',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function () {
                                window.location.replace("{{ route('room.admin.view',$booking->id) }}");
                            })
                        })
                        .fail(function( msg ) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        });
                }

                $('#btn-add').on('click', function () {
                    if ($('#frmVerifyPendingCatererQuotation').valid()) {
                        let formData = new FormData();
                        formData.append('_token', '{{ csrf_token() }}');
                        formData.append('catering_id', '{{ $booking->catering->id }}');
                        formData.append('input-caterer-name', $('#input-caterer-name').val());
                        formData.append('input-caterer-cost', $('#input-caterer-cost').val());
                        formData.append('file', $('input[type=file]')[0].files[0]);

                        $.ajax({
                            method: 'post',
                            url: '{{ route('room.admin.api.add_caterer_quotation') }}',
                            enctype: 'multipart/form-data',
                            processData: false,
                            contentType: false,
                            data: formData,
                        })
                            .done(function( msg ){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Caterer Quotation added successfully!',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(function () {
                                    dt.ajax.reload();
                                })
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                });

                $('#dtQuotation tbody').on('click', '.link-use', function (e) {
                    e.preventDefault();

                    let row = dt.row($(this).closest('tr')).data();

                    $.ajax({
                        method: "POST",
                        url: "{{ route('room.admin.api.select_caterer_quotation') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            quotation_id: row.id,
                        }
                    })
                        .done(function( msg ) {
                            dt.ajax.reload();
                        })
                        .fail(function( msg ) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        });

                });

                $('#dtQuotation tbody').on('click', '.link-del', function (e) {
                    e.preventDefault();
                    let row = dt.row($(this).closest('tr')).data();

                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                method: "POST",
                                url: "{{ route('room.admin.api.delete_caterer_quotation') }}",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    quotation_id: row.id,
                                }
                            })
                                .done(function( msg ) {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Quotation has been deleted.',
                                        showConfirmButton: false,
                                        timer: 1000
                                    }).then(function () {
                                        dt.ajax.reload();
                                    })
                                })
                                .fail(function( msg ) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Something went wrong!',
                                    })
                                });
                        }
                    })
                });

                $('#frmVerifyPendingCatererQuotation').validate({
                    rules: {
                        'input-caterer-name': {
                            required: true
                        },
                        'input-caterer-cost': {
                            required: true
                        },
                        'input-caterer-quotation': {
                            required: true
                        },
                    }
                });
            @endif

            $('#btn-cancel-room-booking').on('click', function () {
                let id = '{{ $booking->id }}';

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, cancel it!',
                    input: 'textarea',
                    inputPlaceholder: 'Type your reason here...',
                    inputAttributes: {
                        'aria-label': 'Type your reason here'
                    },
                    inputValidator: (value) => {
                        if (!value) {
                            return 'You need to write something!'
                        }
                    }
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.api.cancel_room_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                                remarks: result.value
                            }
                        })
                            .done(function( msg ) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your booking has been cancelled.',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(function () {
                                    window.location.replace("{{ route('room.admin.view',$booking->id) }}");
                                })
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-delete-room-booking').on('click', function () {
                let id = '{{ $booking->id }}';

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.api.delete_room_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                            }
                        })
                            .done(function( msg ) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your booking has been deleted.',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(function () {
                                    window.location.replace("{{ route('room.admin.api.get_all_booking') }}");
                                })
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-verify-room-booking').on('click', function () {
                let status = '{{ $booking->booking_status }}';

                if (status == '{{ config('constant.status.PENDING_APPROVAL') }}') {
                    $('#verifyPendingApprovalModal').modal('show');
                } else if (status == '{{ config('constant.status.PENDING_CATERER_QUOTATION') }}') {
                    $('#verifyPendingCatererQuotationModal').modal('show');
                }
            });

            $('#sel-action').on('change', function (data) {
                $('#remark-tr').toggle('show');
            });

            $('#btn-confirm').on('click', function () {
                let allowSubmission = true;

                if ($('#remark-tr').is(':visible')) {
                    if (!$('#frmVerifyPendingApproval').valid()) {
                        allowSubmission = false
                    }
                }

                if (allowSubmission) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('room.admin.api.verify_booking') }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            booking_id: '{{ $booking->id }}',
                            action: $('#sel-action').val(),
                            remark: $('#input-remark').val()
                        }
                    })
                        .done(function( msg ) {
                            $('.modal').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Room booking has been saved',
                                showConfirmButton: false,
                                timer: 1500
                            }).then(function () {
                                window.location.replace("{{ route('room.admin.view',$booking->id) }}");
                            })
                        })
                        .fail(function( msg ) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        });
                }

            });



            $('#frmVerifyPendingApproval').validate({
                rules: {
                    'input-remark': {
                        required: true
                    }
                }
            });




        });
    </script>
@endsection
