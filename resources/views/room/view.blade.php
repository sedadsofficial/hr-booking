@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Meeting Room</a></li>
    <li class="breadcrumb-item active" aria-current="page">View</li>
@endsection

@section('content')
    <div style="width: 100%">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">View Booking Details</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">
        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <table class="table table-borderless table-responsive">
                            <tr>
                                <td><label class="label-info font-weight-bold">Booking Status</label></td>
                                <td><h5><span class="badge badge-secondary">{{ $booking->booking_status }}</span></h5></td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">From</label></td>
                                <td>{{ Carbon\Carbon::parse($booking->meeting_date_from)->format('d/m/Y h:i A')}}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">To</label></td>
                                <td>{{ Carbon\Carbon::parse($booking->meeting_date_to)->format('d/m/Y h:i A')}}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Title</label></td>
                                <td>{{ $booking->meeting_desc }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Room</label></td>
                                <td>{{ $booking->room->name }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Type of Setup</label></td>
                                <td>{{ $booking->room_setup->name }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Chairperson</label></td>
                                <td>{{ $booking->chairperson }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">No of Pax</label></td>
                                <td>{{ $booking->pax_no }}</td>
                            </tr>
                            <tr>
                                <td><label class="label-info font-weight-bold">Remarks</label></td>
                                <td>{{ $booking->remarks }}</td>
                            </tr>
                            @if( $booking->catering || $booking->refreshment)
                                <tr>
                                    <td><label class="label-info font-weight-bold">Details of Food</label></td>
                                    <td>
                                        @if($booking->refreshment)
                                            <table class="table table-borderless table-responsive">
                                                <tr>
                                                    <td><label class="label-info font-weight-bold">Food</label></td>
                                                    <td>{{ $booking->refreshment->food->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td><label class="label-info font-weight-bold">Drink</label></td>
                                                    <td>{{ $booking->refreshment->drink->name }}</td>
                                                </tr>
                                            </table>
                                        @endif

                                        @if($booking->catering)
                                            <div class="row">
                                                <div class="col-sm table-responsive">
                                                    <label class="label-info font-weight-bold">Food</label>
                                                    <table class="table table-borderless border rounded">
                                                        @foreach($booking->catering->catering_item->where('item_type','food') as $item)
                                                            <tr>
                                                                <td><label class="label-info font-weight-bold">Slot</label></td>
                                                                <td>{{ $item->item_name }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border-bottom rounded"><label class="label-info font-weight-bold">Remark</label></td>
                                                                <td class="border-bottom rounded">{{ $item->item_remark }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                                <div class="col-sm">
                                                    <label class="label-info font-weight-bold">Drink</label>
                                                    <table class="table table-borderless border rounded">
                                                        @foreach($booking->catering->catering_item->where('item_type','drink') as $item)
                                                            <tr>
                                                                <td><label class="label-info font-weight-bold">Slot</label></td>
                                                                <td>{{ $item->item_name }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border-bottom rounded"><label class="label-info font-weight-bold">Remark</label></td>
                                                                <td class="border-bottom rounded">{{ $item->item_remark }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>
                                            </div>
                                            @if($booking->catering_quotation()->selected()->count() > 0)
                                                <div class="row">
                                                    <div class="col-sm">
                                                        <label class="label-info font-weight-bold">Caterer Info</label>
                                                        <div class="table border rounded">
                                                            <div class="border-bottom">
                                                                <div class="row p-3">
                                                                    <div class="col-sm-3"><label class="label-info font-weight-bold">Caterer Name</label></div>
                                                                    <div class="col-sm">{{ $booking->catering_quotation()->selected()->first()->caterer_name }}</div>
                                                                </div>
                                                                <div class="row p-3">
                                                                    <div class="col-sm-3"><label class="label-info font-weight-bold">Cost (RM)</label></div>
                                                                    <div class="col-sm">{{ $booking->catering_quotation()->selected()->first()->cost }}</div>
                                                                </div>
                                                                <div class="row p-3">
                                                                    <div class="col-sm-3"><label class="label-info font-weight-bold">Quotation File</label></div>
                                                                    <div class="col-sm"><a href="{{ asset('storage/'.$booking->catering_quotation()->selected()->first()->file_name) }}" target="_blank">View</a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="row">
                                                <table class="table table-borderless table-responsive">
                                                    <tr>
                                                        <td><label class="label-info font-weight-bold">Food Setup</label></td>
                                                        <td>{{ $booking->catering->food_setup->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><label class="label-info font-weight-bold">Budget Line</label></td>
                                                        <td>{{ $booking->catering->budget_line }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        </table>
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row p-3">
                                            <div class="col-sm text-nowrap font-weight-bold">Booked By :</div>
                                            <div class="col">
                                                <span class="font-italic">{{ $booking->user->name }}</span><br>
                                                <span>{{ $booking->user->division->name }}</span>
                                            </div>
                                        </div>
                                        <div class="row p-3">
                                            <div class="col-sm text-nowrap font-weight-bold">Booked On :</div>
                                            <div class="col">{{ Carbon\Carbon::parse($booking->created_at)->format('d/m/Y h:i A')}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mt-5">
                            @if ($booking->is_allowed_to_edit)
                                <a class="btn btn-warning" href="{{ route('room.edit',$booking->id) }}" role="button">Edit</a>
                            @endif
                            @if ($booking->is_allowed_to_cancel)
                                <input id="btn-cancel-room-booking" class="btn btn-warning cancel" type="button" value="Cancel"/>
                            @endif
                            @if ($booking->is_allowed_to_delete)
                                <input id="btn-delete-room-booking" class="btn btn-danger delete" type="button" value="Delete"/>
                            @endif
                            @if($booking->booking_status == config('constant.status.PENDING_CATERER_CONFIRMATION'))
                                <input id="btn-verify-room-booking" class="btn btn-success verify" type="button" value="Verify"/>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pending Confirmation Modal -->
    <div id="verifyPendingConfirmation" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modalTitle" class="modal-title">Verification</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                </div>
                <div id="modalBody" class="modal-body">
                    <table class="table table-borderless">
                        <tr>
                            <td>Action</td>
                            <td>
                                <span><em>* I hereby acknowledge, by my action below, that I have agreed to the given caterer quotation</em></span>
                                <select id="sel-action" class="custom-select">
                                    <option value="1" selected>Confirm</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="btn-confirm" class="btn btn-primary" data-id="">Confirm</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $('#btn-cancel-room-booking').on('click', function () {
                let id = '{{ $booking->id }}';

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, cancel it!',
                    input: 'textarea',
                    inputPlaceholder: 'Type your reason here...',
                    inputAttributes: {
                        'aria-label': 'Type your reason here'
                    },
                    inputValidator: (value) => {
                        if (!value) {
                            return 'You need to write something!'
                        }
                    }
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.api.cancel_room_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                                remarks: result.value
                            }
                        })
                            .done(function( msg ) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your booking has been cancelled.',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(function () {
                                    window.location.replace("{{ route('room.mybooking') }}");
                                })
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-delete-room-booking').on('click', function () {
                let id = '{{ $booking->id }}';

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            url: "{{ route('room.api.delete_room_booking') }}",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                booking_id: id,
                            }
                        })
                            .done(function( msg ) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Your booking has been deleted.',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then(function () {
                                    window.location.replace("{{ route('room.mybooking') }}");
                                })
                            })
                            .fail(function( msg ) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                })
                            });
                    }
                })
            });

            $('#btn-verify-room-booking').on('click', function () {
                $('#verifyPendingConfirmation').modal('show');
            });

            $('#btn-confirm').on('click', function () {
                $.ajax({
                    method: "POST",
                    url: "{{ route('room.api.verify_booking') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        booking_id: '{{ $booking->id }}',
                    }
                })
                    .done(function( msg ) {
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Room booking has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        }).then(function () {
                            window.location.replace("{{ route('room.view',$booking->id) }}");
                        })
                    })
                    .fail(function( msg ) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });

            });
        });
    </script>
@endsection
