@extends('layouts.app')

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Meeting Room</a></li>
    <li class="breadcrumb-item active" aria-current="page">Apply</li>
@endsection

@section('content')
    <style>
        .fc-event {
            cursor: pointer;
        }
    </style>

    <div style="width: 100%;">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Book a Room</h1><br/>
        </div>
        <hr class="sidebar-divider d-none d-md-block">

        <div class="card border-left-primary shadow">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        @if(!$rooms->isEmpty())
                        <ul class="list-group list-group-horizontal-md">
                            @foreach($rooms as $color => $room)
                            <li class="list-group-item flex-fill"><span class="badge" style="background-color: {{ $color }}">&nbsp;&nbsp;&nbsp;&nbsp;</span> {{ $room }}</li>
                            @endforeach
                        </ul>
                        @endif

                        <div class="mt-3" id='calendar'></div>

                        <!-- Modal: datetimeSelectionModal-->
                        <div class="modal fade .modal-lg" id="datetimeSelectionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Room Booking Request</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" id="frmSubmission" action="" method="post" class="registration-form">
                                            <fieldset style="display: block;">
                                                <div class="form-bottom">
                                                    <div class="form-group">
                                                        <label class="label-info">From</label>
                                                        <div class="input-group date" id="datetimepicker_from" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_from" readonly/>
                                                            <div class="input-group-append" data-target="#datetimepicker_from" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info">To</label>
                                                        <div class="input-group date" id="datetimepicker_to" data-target-input="nearest">
                                                            <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker_to" readonly/>
                                                            <div class="input-group-append" data-target="#datetimepicker_to" data-toggle="datetimepicker">
                                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info">Title</label>
                                                        <input type="text" id="input-title" name="input-title" class="form-control" placeholder="Enter meeting title"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info">Type of Setup</label>
                                                        {!! Form::select('input-setup', $setup, null, ['class' => 'form-control']) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info">Chairperson</label>
                                                        <input type="text" id="input-chairperson" name="input-chairperson" class="form-control" placeholder="Enter chairperson's name"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info">No. of Pax</label>
                                                        <input type="number" id="input-paxnum" name="input-paxnum" min="0" oninput="validity.valid||(value='');" class="form-control">
                                                    </div>
                                                    <div class="form-group form-check">
                                                        <input type="checkbox" class="form-check-input" id="check-require-food" name="check-require-food" value="1">
                                                        <label class="form-check-label" for="check-require-food">Require foods & beverages?</label>
                                                    </div>
                                                    <div id="food-group" class="animated--grow-in" style="display: none">
                                                        <div class="form-group">
                                                            <label class="label-info">Catering</label>
                                                            <div class="card border-left-warning shadow mb-2">
                                                                <div class="card-body">
                                                                    <div class="row align-items-center">
                                                                        <div class="col mr-2">
                                                                            <em>For catering, the order should be placed <strong>2 days</strong> before meeting / event date</em>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <label class="form-control radio-inline"><input type="radio" name="input-catering" id="input-catering-yes" value="1">Yes</label>
                                                            <label class="form-control radio-inline"><input type="radio" name="input-catering" id="input-catering-no" value="0">No</label>
                                                        </div>
                                                        <div id="food-details-catering" class="animated--grow-in" style="display: none">
                                                            <div class="form-group border rounded p-3">
                                                                <label class="label-info">Details of Food</label>
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <div class="form-group">
                                                                            <label class="label-info mr-3">Food</label>
                                                                            <div class="form-group form-check">
                                                                                <input type="checkbox" class="form-check-input" id="check-bfast-food" name="check-food[]" value="check-bfast-food">
                                                                                <label class="form-check-label" for="check-bfast-food">Breakfast (8:00 AM - 10:00 AM)</label>
                                                                                <input type="text" id="input-bfast-food" name="input-bfast-food" class="form-control" placeholder="Enter menu preference"/>
                                                                            </div>
                                                                            <div class="form-group form-check">
                                                                                <input type="checkbox" class="form-check-input" id="check-mbreak-food" name="check-food[]" value="check-mbreak-food">
                                                                                <label class="form-check-label" for="check-mbreak-food">Morning Break (10:30 AM - 11:30 AM)</label>
                                                                                <input type="text" id="input-mbreak-food" name="input-mbreak-food" class="form-control" placeholder="Enter menu preference"/>
                                                                            </div>
                                                                            <div class="form-group form-check">
                                                                                <input type="checkbox" class="form-check-input" id="check-lunch-food" name="check-food[]" value="check-lunch-food">
                                                                                <label class="form-check-label" for="check-lunch-food">Lunch (12:00 PM - 1:00 PM)</label>
                                                                                <input type="text" id="input-lunch-food" name="input-lunch-food" class="form-control" placeholder="Enter menu preference"/>
                                                                            </div>
                                                                            <div class="form-group form-check">
                                                                                <input type="checkbox" class="form-check-input" id="check-tbreak-food" name="check-food[]" value="check-tbreak-food">
                                                                                <label class="form-check-label" for="check-tbreak-food">Tea Break (2:00 PM - 4:00 PM)</label>
                                                                                <input type="text" id="input-tbreak-food" name="input-tbreak-food" class="form-control" placeholder="Enter menu preference"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col">
                                                                        <div class="form-group">
                                                                            <label class="label-info mr-3">Drink</label>
                                                                            <div class="form-group form-check">
                                                                                <input type="checkbox" class="form-check-input" id="check-bfast-drink" name="check-drink[]" value="check-bfast-drink">
                                                                                <label class="form-check-label" for="check-bfast-drink">Breakfast (8:00 AM - 10:00 AM)</label>
                                                                                <input type="text" id="input-bfast-drink" name="input-bfast-drink" class="form-control" placeholder="Enter menu preference"/>
                                                                            </div>
                                                                            <div class="form-group form-check">
                                                                                <input type="checkbox" class="form-check-input" id="check-mbreak-drink" name="check-drink[]" value="check-mbreak-drink">
                                                                                <label class="form-check-label" for="check-mbreak-drink">Morning Break (10:30 AM - 11:30 AM)</label>
                                                                                <input type="text" id="input-mbreak-drink" name="input-mbreak-drink" class="form-control" placeholder="Enter menu preference"/>
                                                                            </div>
                                                                            <div class="form-group form-check">
                                                                                <input type="checkbox" class="form-check-input" id="check-lunch-drink" name="check-drink[]" value="check-lunch-drink">
                                                                                <label class="form-check-label" for="check-lunch-drink">Lunch (12:00 PM - 1:00 PM)</label>
                                                                                <input type="text" id="input-lunch-drink" name="input-lunch-drink" class="form-control" placeholder="Enter menu preference"/>
                                                                            </div>
                                                                            <div class="form-group form-check">
                                                                                <input type="checkbox" class="form-check-input" id="check-tbreak-drink" name="check-drink[]" value="check-tbreak-drink">
                                                                                <label class="form-check-label" for="check-tbreak-drink">Tea Break (2:00 PM - 4:00 PM)</label>
                                                                                <input type="text" id="input-tbreak-drink" name="input-tbreak-drink" class="form-control" placeholder="Enter menu preference"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <div class="form-group">
                                                                            <label class="label-info">Food Setup</label>
                                                                            @foreach($foodSetup as $key => $value)
                                                                            <label class="form-control radio-inline"><input type="radio" name="input-foodsetup" value="{{ $key }}">{{ $value }}</label>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <div class="form-group">
                                                                            <label class="label-info">Budget Line (Grant/Division)</label>
                                                                            <input type="text" id="input-budgetline" name="input-budgetline" class="form-control" placeholder="Enter which grant/division"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="food-details-no-catering" class="animated--grow-in" style="display: none">
                                                            <div class="form-group border rounded p-3">
                                                                <label class="label-info">Details of Food</label>
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <div class="form-group">
                                                                            <label class="label-info">Food</label>
                                                                            {!! Form::select('input-food', $food, null, ['class' => 'form-control']) !!}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col">
                                                                        <div class="form-group">
                                                                            <label class="label-info">Drink</label>
                                                                            {!! Form::select('input-drink', $drink, null, ['class' => 'form-control']) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label-info">Remarks</label>
                                                        <textarea type="text" id="input-remark" name="input-remark" class="form-control" placeholder="Enter remarks (if any)"></textarea>
                                                    </div>
                                                </div>
                                                <input id="selectedDateFrom" type="hidden"/>
                                                <input id="selectedDateTo" type="hidden"/>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-next">Next</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal: roomSelectionModal -->
                        <div class="modal fade .modal-lg" id="roomSelectionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Pick a Room</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table id="dtRoom" class="datatable table display" cellspacing="0" style="table-layout: auto">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Room</th>
                                                <th>Capacity</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>Room</th>
                                                <th>Capacity</th>
                                                <th>Action</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-prev">Back</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Event Modal -->
                        <div id="fullCalModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="modalTitle" class="modal-title"></h4>
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span class="sr-only">close</span></button>
                                    </div>
                                    <div id="modalBody" class="modal-body"></div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
{{--                                        <button id="btn-view" class="btn btn-primary" data-id="">View</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let calendarEl = document.getElementById('calendar');

            let calendar = new FullCalendar.Calendar(calendarEl, {
                header: {
                    center: 'dayGridMonth,timeGridWeek' // buttons for switching between views
                },
                plugins: [ 'dayGrid','interaction','timeGrid','moment' ],
                nowIndicator: true,
                eventLimit: 5,
                views: {
                    dayGrid: {
                    },
                    week: {
                    }
                },
                dateClick: function (info) {
                    calendar.changeView('timeGridWeek', info.dateStr);
                    if (info.view.type == 'timeGridWeek') {
                        $('#datetimeSelectionModal').modal();
                        $('#selectedDateFrom').val(info.dateStr);
                    }
                },
                firstDay: 1,
                startParam: 'agenda.start',
                limitParam : 'agenda.limit',
                events: {
                    url: '{{ route('room.api.show_all_room_booking') }}',
                    method: 'GET',
                    failure: function() {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Unable to fetch calendar! Refresh the page and try again',
                        })
                    },
                    textColor: '#ffffff',
                },
                eventClick: function (event) {
                    $('#modalTitle').html(event.event.title);
                    $('#modalBody').html(event.event.extendedProps.description);
                    $('#btn-remove').attr('data-id',event.event.id);
                    $('#btn-edit').attr('data-id',event.event.id);
                    $('#btn-view').attr('data-id',event.event.id);
                    $('#fullCalModal').modal();
                },
            });

            calendar.render();

            $.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
                format: 'DD/MM/YYYY h:mm A',
                ignoreReadonly: true
            });

            $('#datetimeSelectionModal').on('shown.bs.modal', function (e) {
                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateFrom').val();

                $('#datetimepicker_from').datetimepicker('date', moment(dateFrom).format('DD/MM/YYYY h:mm A'));
                $('#datetimepicker_to').datetimepicker('date', moment(dateTo).format('DD/MM/YYYY h:mm A'));
            });

            $('#roomSelectionModal').on('shown.bs.modal', function (e) {
                console.log('roomSelectionModal is visible!');
                let url = '{{ route('room.api.get_available_room') }}';
                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateTo').val();
                // url = url.replace(':date', $('#selectedDate').text());
                $("#dtRoom").DataTable().destroy();
                $('#dtRoom').DataTable({
                    scrollX: true,
                    processing : true,
                    serverSide : true,
                    pagingType : "full_numbers",
                    paging : true,
                    searching : false,
                    lengthMenu : [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                    ajax : {
                        url: url,
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            selectedDateFrom : moment(dateFrom).subtract(10,'m').format('YYYY-MM-DD HH:mm:00'),
                            selectedDateTo : moment(dateTo).add(10,'m').format('YYYY-MM-DD HH:mm:00')
                        },
                        failure: function ( result ) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            })
                        }
                    },
                    columns : [
                        {
                            data: 'id',
                            width : '10%',
                            searchable : false,
                            orderable : false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: 'name'},
                        {
                            data: 'capacity',
                            width : '10%',
                        },
                        {
                            data: 'id',
                            width:'20%',
                            searchable: false,
                            orderable: false,
                            render : function(data,type,full,meta) {
                                return "<input type='button' class='btn btn-primary btn-sm pickRoom' value='Select'/>"
                            }
                        }
                    ],
                    error: function(e) {
                        console.log(e.responseText);
                    }
                });
            });

            $('#frmSubmission').validate({
                rules: {
                    'input-title':  {
                        required: true
                    },
                    'input-division': {
                        required: true
                    },
                    'input-setup': {
                        required: true
                    },
                    'input-chairperson': {
                        required: true
                    },
                    'input-paxnum': {
                        required: true
                    },
                    'input-catering': {
                        required: '#check-require-food:checked'
                    },
                    'input-bfast-food': {
                        required: '#check-bfast-food:checked'
                    },
                    'input-mbreak-food': {
                        required: '#check-mbreak-food:checked'
                    },
                    'input-lunch-food': {
                        required: '#check-lunch-food:checked'
                    },
                    'input-tbreak-food': {
                        required: '#check-tbreak-food:checked'
                    },
                    'input-bfast-drink': {
                        required: '#check-bfast-drink:checked'
                    },
                    'input-mbreak-drink': {
                        required: '#check-mbreak-drink:checked'
                    },
                    'input-lunch-drink': {
                        required: '#check-lunch-drink:checked'
                    },
                    'input-tbreak-drink': {
                        required: '#check-tbreak-drink:checked'
                    },
                    'check-food[]': {
                        required: '#input-catering-yes:checked',
                        minlength: 1,
                    },
                    'check-drink[]': {
                        required: '#input-catering-yes:checked',
                        minlength: 1,
                    },
                    'input-foodsetup': {
                        required: '#input-catering-yes:checked'
                    },
                    'input-budgetline': {
                        required: '#input-catering-yes:checked'
                    }
                },
                messages: {
                    'check-food[]': 'Choose at least one (1) food slot',
                    'check-drink[]': 'Choose at least one (1) drink slot',
                },
                errorPlacement: function(error, element)
                {
                    if ( element.is(":radio")) {
                        error.appendTo(element.parents('.form-group')[0]);
                    }
                    else if (element.is(":checkbox")) {
                        error.insertAfter(element.parents('.form-group').prev($('.label-info')));
                    }
                    else { // This is the default behavior
                        error.insertAfter( element );
                    }
                }
            });

            $("div[id*='SelectionModal']").each(function() {
                let currentModal = $(this);

                //click next
                currentModal.find('.btn-next').click(function(){
                    if ($('#frmSubmission').valid()) {
                        currentModal.modal('hide');
                        currentModal.closest("div[id*='SelectionModal']").nextAll("div[id*='SelectionModal']").first().modal('show');
                    }
                });

                //click prev
                currentModal.find('.btn-prev').click(function(){
                    currentModal.modal('hide');
                    currentModal.closest("div[id*='SelectionModal']").prevAll("div[id*='SelectionModal']").first().modal('show');
                });
            });

            $('body').on('hidden.bs.modal', function () {
                if($('.modal.show').length > 0) {
                    $('body').addClass('modal-open');
                }
            });

            $('#datetimepicker_from').on('change.datetimepicker', function (e) {
                $('#selectedDateFrom').val($("#datetimepicker_from").datetimepicker('date').format());
            });

            $('#datetimepicker_to').on('change.datetimepicker', function (e) {
                $('#selectedDateTo').val($("#datetimepicker_to").datetimepicker('date').format());
            });

            $('#datetimepicker_from').on('error.datetimepicker', function (e) {
                console.log(e);
            });

            $('#dtRoom tbody').on('click', '.pickRoom', function () {
                var row = $(this).closest('tr');
                var id = $('#dtRoom').DataTable().row( row ).data()["id"];

                let dateFrom = $('#selectedDateFrom').val();
                let dateTo = $('#selectedDateTo').val();

                $.blockUI({baseZ: 2000});
                $.ajax({
                    method: "POST",
                    url: "{{ route('room.api.save_room_booking') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        room_id: id,
                        dateFrom: moment(dateFrom).format('YYYY-MM-DD HH:mm:00'),
                        dateTo: moment(dateTo).format('YYYY-MM-DD HH:mm:00'),
                        form: $('#frmSubmission').serializeArray()
                    }
                })
                    .done(function( msg ) {
                        $.unblockUI();
                        $('.modal').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Room booking has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        calendar.refetchEvents();
                    })
                    .fail(function( msg ) {
                        $.unblockUI();
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',
                        })
                    });
            });

            $('#btn-view').on('click', function(event) {
                let url = '{{ url('room/mybooking/view/:id') }}';
                url = url.replace(':id', $(this).attr('data-id'));
                window.location.href = url;
            });

            $('#check-require-food').on('change', function () {
                $('#food-group').toggle();
                if (this.checked) {
                    console.log('sudah check!');
                } else {
                    console.log('uncheck!');
                    $('input:radio[name="input-catering"]').prop('checked', false);
                    $('#food-details-no-catering').hide();
                    $('#food-details-catering').hide();
                }
            });

            $('input:radio[name="input-catering"]').on('change', function () {
                if ($(this).val() == 1) {
                    $('#food-details-catering').show();
                    $('#food-details-no-catering').hide();
                } else {
                    $('#food-details-no-catering').show();
                    $('#food-details-catering').hide();
                }
            });

        });
    </script>
@endsection
