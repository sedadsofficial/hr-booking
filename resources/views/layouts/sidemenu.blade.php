
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('/home')}}">
        <div class="sidebar-brand-icon">
            <i class="fas fa-calendar-check"></i>
        </div>
        <div class="sidebar-brand-text mx-0">{{ config('constant.APP_NAME') }}<sup>1.0<sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ (\Request::route()->getName() == 'home') ? 'active' : null }}">
        <a class="nav-link" href="{{url('/home')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Booking Type
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ (\Request::is('room/*')) ? 'active' : null }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-chalkboard-teacher"></i>
            <span>Meeting Room</span>
        </a>
        <div id="collapseTwo" class="collapse {{ (\Request::is('room/*')) ? 'show' : null }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Action Menu:</h6>
                <a class="collapse-item {{ (\Request::route()->getName() == 'room.index') ? 'active' : null }}" href="{{route('room.index')}}">Apply</a>
                <a class="collapse-item {{ (Request::is('room/mybooking*')) ? 'active' : null }}" href="{{route('room.mybooking')}}">My Booking</a>
                @if(\Auth::user()->is_admin)
                <h6 class="collapse-header">Admin Menu:</h6>
                <a class="collapse-item {{ (in_array(Request::route()->getName(),['room.admin.all', 'room.admin.view'])) ? 'active' : null }}" href="{{route('room.admin.all')}}">All Booking</a>
                @endif
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item {{ (\Request::is('vehicle/*')) ? 'active' : null }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-car"></i>
            <span>Vehicle</span>
        </a>
        <div id="collapseUtilities" class="collapse {{ (\Request::is('vehicle/*')) ? 'show' : null }}" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Action Menu:</h6>
                <a class="collapse-item {{ (\Request::route()->getName() == 'vehicle.index') ? 'active' : null }}" href="{{route('vehicle.index')}}">Apply</a>
                <a class="collapse-item {{ (Request::is('vehicle/mybooking*')) ? 'active' : null }}" href="{{route('vehicle.mybooking')}}">My Booking</a>
                @if(\Auth::user()->is_driver)
                <h6 class="collapse-header">Driver Menu:</h6>
                <a class="collapse-item {{ (Request::is('vehicle/jobs*'))  ? 'active' : null }}" href="{{route('vehicle.jobs')}}">Jobs</a>
                <a class="collapse-item {{ (Request::is('vehicle/report*'))  ? 'active' : null }}" href="{{route('vehicle.report')}}">Reporting</a>
                @endif
                @if(\Auth::user()->is_admin)
                <h6 class="collapse-header">Admin Menu:</h6>
                <a class="collapse-item {{ (in_array(Request::route()->getName(),['vehicle.admin.all', 'vehicle.admin.view'])) ? 'active' : null }}" href="{{route('vehicle.admin.all')}}">All Booking</a>
                <a class="collapse-item {{ (Request::is('vehicle/admin/report*')) ? 'active' : null }}" href="{{route('vehicle.admin.report')}}">Reporting</a>
                @endif
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    @if(\Auth::user()->is_admin)
    <!-- Heading -->
    <div class="sidebar-heading">
        Administrator
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ (\Request::is('admin/*')) ? 'active' : null }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseAdmin" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cogs"></i>
            <span>System Setting</span>
        </a>
        <div id="collapseAdmin" class="collapse {{ (\Request::is('admin/*')) ? 'show' : null }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Global Setting:</h6>
                <a class="collapse-item {{ (Request::is('admin/setting/global/parameter_management*')) ? 'active' : null }}" href="{{route('admin.setting.global.parameter')}}">Parameter Management</a>
                <a class="collapse-item {{ (Request::is('admin/setting/global/user_management*')) ? 'active' : null }}" href="{{route('admin.setting.global.user_management')}}">User Management</a>

                <h6 class="collapse-header">Meeting Room Setting:</h6>
                <a class="collapse-item {{ (Request::is('admin/setting/room*')) ? 'active' : null }}" href="{{route('admin.setting.room.parameter')}}">Parameter Management</a>

                <h6 class="collapse-header">Vehicle Setting:</h6>
                <a class="collapse-item {{ (Request::is('admin/setting/vehicle/parameter_management*')) ? 'active' : null }}" href="{{route('admin.setting.vehicle.parameter')}}">Parameter Management</a>
                <a class="collapse-item {{ (Request::is('admin/setting/vehicle/driver_management*')) ? 'active' : null }}" href="{{route('admin.setting.vehicle.driver_management')}}">Driver Management</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    @endif

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
