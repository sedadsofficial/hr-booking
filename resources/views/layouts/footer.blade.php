    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="row copyright" style="justify-content: space-between;">
                <div class="col-xs-6">Copyright &copy; SEDA {{ date('Y') }}</div>
                <div class="col-xs-6">Developed with &#128149; by Digital Services Division</div>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->
