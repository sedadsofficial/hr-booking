<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('constant.APP_NAME') }}</title>

        <!-- Custom fonts for this template-->
        <link href="{{ asset('template/vendor/fontawesome-free/css/all.css') }}" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="{{ asset('template/css/sb-admin-2.css') }}" rel="stylesheet">

        <!-- FullCalendar CSS-->
        <link href="{{ asset('assets/fullcalendar-4/packages/core/main.css') }}" rel='stylesheet' />
        <link href="{{ asset('assets/fullcalendar-4/packages/daygrid/main.css') }}" rel='stylesheet' />
        <link href="{{ asset('assets/fullcalendar-4/packages/timegrid/main.css') }}" rel='stylesheet' />

        <!-- DataTables CSS-->
        <link href="{{ asset('template/vendor/datatables/dataTables.bootstrap4.css') }}" rel='stylesheet' />

        <!-- SweetAlert2 CSS -->
        <link href="{{ asset('assets/sweetalert2/dist/sweetalert2.css') }}" rel='stylesheet' />
        @yield('styles')

    <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('template/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('template/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

        <!-- Core plugin JavaScript-->
        <script src="{{ asset('template/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

        <!-- jQuery Validation Plugin-->
        <script src="{{ asset('assets/jquery-validation/dist/jquery.validate.js') }}"></script>
        <script src="{{ asset('assets/jquery-validation/dist/additional-methods.js') }}"></script>

        <!-- FullCalendar JavaScript-->
        <script src="{{ asset('assets/fullcalendar-4/packages/core/main.js') }}"></script>
        <script src="{{ asset('assets/fullcalendar-4/packages/daygrid/main.js') }}"></script>
        <script src="{{ asset('assets/fullcalendar-4/packages/interaction/main.js') }}"></script>
        <script src="{{ asset('assets/fullcalendar-4/packages/timegrid/main.js') }}"></script>
        <script src="{{ asset('assets/fullcalendar-4/packages/moment/main.js') }}"></script>

        <!-- DataTables Javascript-->
        <script src="{{ asset('template/vendor/datatables/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('template/vendor/datatables/dataTables.bootstrap4.js') }}"></script>

        <!-- Moment.js -->
        <script src="{{ asset('assets/moment/moment.min.js') }}"></script>

        <!-- Tempusdominus Datepicker-->
        <script type="text/javascript" src="{{ asset('assets/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.js')}}"></script>
        <link rel="stylesheet" href="{{ asset('assets/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.css')}}" />

        <!-- SweetAlert2 JS -->
        <script type="text/javascript" src="{{ asset('assets/sweetalert2/dist/sweetalert2.all.js')}}"></script>

        <!-- jQuery BlockUI JS -->
        <script type="text/javascript" src="{{ asset('assets/jquery.blockUI/jquery.blockUI.js')}}"></script>
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">

            @include('layouts.sidemenu')

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    @include('layouts.topbar')

                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <div class="row">
                            @yield('content')
                        </div>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                @include('layouts.footer')

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        @include('layouts.logoutmodal')



        <!-- Custom scripts for all pages-->
        <script src="{{ asset('template/js/sb-admin-2.min.js') }}"></script>
        @yield('scripts')

    </body>
</html>
