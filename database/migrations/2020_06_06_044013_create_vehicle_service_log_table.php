<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleServiceLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_service_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_id')->default(null);
            $table->string('panel_workshop_name')->default(null);
            $table->dateTime('service_date')->default(null);
            $table->string('service_desc')->default(null);
            $table->dateTime('next_service_date')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_service_logs');
    }
}
