<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSelectedFieldAndRemoveFileExtField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catering_quotations', function (Blueprint $table) {
            $table->boolean('is_selected')->after('file_name')->default(0);
            $table->dropColumn('file_ext');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catering_quotations', function (Blueprint $table) {
            $table->dropColumn('is_selected');
            $table->string('file_ext');
        });
    }
}
