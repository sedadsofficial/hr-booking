<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('car_booking_date_from')->default(null);
            $table->dateTime('car_booking_date_to')->default(null);
            $table->string('purpose_booking')->default(null);
            $table->string('purpose_detail')->default(null);
            $table->integer('transport_type')->default(null);
            $table->integer('driver_id')->default(null);
            $table->string('total_passenger')->default(null);
            $table->string('booking_status')->default(null);
            $table->string('destination')->default(null);
            $table->string('remarks')->default(null);
            $table->integer('vehicle_id')->default(null);
            $table->integer('user_id')->default(null);
            $table->integer('division_id')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('car_bookings');
    }
}
