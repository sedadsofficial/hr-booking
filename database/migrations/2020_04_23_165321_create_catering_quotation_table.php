<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCateringQuotationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catering_quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('catering_id');
            $table->string('caterer_name');
            $table->string('cost');
            $table->string('file_name');
            $table->string('file_ext');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('catering_quotations');
    }
}
