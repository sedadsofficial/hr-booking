<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplacementDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('replacement_drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id')->default(null);
            $table->integer('division_id')->default(null);
            $table->dateTime('license_expiry_date')->default(null);
            $table->string('license_file_name')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('replacement_drivers');
    }
}
