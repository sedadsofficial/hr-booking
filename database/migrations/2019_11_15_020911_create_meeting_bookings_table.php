<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->unsigned()->notnull();
            $table->integer('user_id')->unsigned()->notnull();
            $table->date('meeting_date')->index();
            $table->string('meeting_desc')->default(null);
            $table->integer('pax_no')->unsigned()->default(1);
            $table->integer('has_refreshment')->unsigned()->default(0);
            $table->string('budget_line')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('meeting_bookings');
    }
}
