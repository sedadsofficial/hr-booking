<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default(null);
            $table->integer('capacity')->default(null);
            $table->integer('plate_number')->default(null);
            $table->dateTime('insurance_date')->default(null);
            $table->string('insurance_file_name')->default(null);
            $table->dateTime('roadtax_expiry_date')->default(null);
            $table->string('roadtax_file_name')->default(null);
            $table->integer('num_of_service')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }
}
