<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleUsageLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_usage_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id')->default(null);
            $table->integer('booking_id')->default(null);
            $table->dateTime('actual_date_from')->default(null);
            $table->dateTime('actual_date_to')->default(null);
            $table->decimal('total_mileage_per_trip')->default(null);
            $table->decimal('total_toll')->default(null);
            $table->decimal('total_petrol')->default(null);
            $table->decimal('odometer')->default(null);
            $table->string('method_of_petrol_purchase')->default(null);
            $table->string('petrol_receipt_file_name')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_usage_logs');
    }
}
