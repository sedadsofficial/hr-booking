<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCancelAndRejectedRelatedFieldsInMeetingBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meeting_bookings', function (Blueprint $table) {
            $table->dropColumn('reject_remarks');
            $table->dropColumn('rejected_by');
            $table->dropColumn('cancel_remarks');
            $table->dropColumn('cancelled_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meeting_bookings', function (Blueprint $table) {
            //
        });
    }
}
