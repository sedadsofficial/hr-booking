<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSoftdeleteFromMeetingBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meeting_bookings', function (Blueprint $table) {
            $table->dropSoftDeletes();
            $table->renameColumn('deleted_by', 'cancelled_by');
            $table->renameColumn('delete_remarks', 'cancel_remarks');
            $table->integer('rejected_by')->after('deleted_by');
            $table->string('reject_remarks')->after('budget_line');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meeting_bookings', function (Blueprint $table) {
            $table->dropColumn('rejected_by');
            $table->dropColumn('reject_remarks');
            $table->renameColumn('cancelled_by', 'deleted_by');
            $table->renameColumn('cancel_remarks', 'delete_remarks');
        });
    }
}
