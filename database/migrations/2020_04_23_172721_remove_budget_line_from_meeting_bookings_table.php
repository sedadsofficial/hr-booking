<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveBudgetLineFromMeetingBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meeting_bookings', function (Blueprint $table) {
            $table->dropColumn('budget_line');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meeting_bookings', function (Blueprint $table) {
            $table->string('budget_line')->nullable();
        });
    }
}
