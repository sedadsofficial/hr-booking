<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefreshmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refreshments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meeting_booking_id')->unsigned()->notnull();
            $table->decimal('total_cost')->default(0);
            $table->integer('description')->unsigned()->default(0);
            $table->string('order_ref_no')->default(null);
            $table->string('caterer_name')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('refreshments');
    }
}
