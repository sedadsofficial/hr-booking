<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDestinationDetailsFieldToVehicleUsageLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_usage_logs', function (Blueprint $table) {
            $table->string('destination_details')->after('booking_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_usage_logs', function (Blueprint $table) {
            $table->dropColumn('destination_details');
        });
    }
}
