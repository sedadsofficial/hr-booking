<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsToMeetingBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meeting_bookings', function (Blueprint $table) {
            $table->string('booking_status')->after('pax_no');
            $table->string('remarks')->after('pax_no');
            $table->boolean('has_catering')->after('pax_no');
            $table->string('chairperson')->after('pax_no');
            $table->integer('room_setup_id')->after('pax_no');
            $table->integer('division_id')->after('pax_no');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meeting_bookings', function (Blueprint $table) {
            $table->dropColumn('division_id');
            $table->dropColumn('room_setup_id');
            $table->dropColumn('chairperson');
            $table->dropColumn('has_catering');
            $table->dropColumn('remarks');
            $table->dropColumn('booking_status');
        });
    }
}
