<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUnwantedFieldsAndAddAdditionalFieldToRefreshment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('refreshments', function (Blueprint $table) {
            $table->dropColumn('total_cost');
            $table->dropColumn('description');
            $table->dropColumn('order_ref_no');
            $table->dropColumn('caterer_name');
            $table->integer('drinks_id')->after('meeting_booking_id');
            $table->integer('foods_id')->after('meeting_booking_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('refreshments', function (Blueprint $table) {
            //
        });
    }
}
