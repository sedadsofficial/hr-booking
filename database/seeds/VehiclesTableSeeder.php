<?php

use App\Vehicle;
use Illuminate\Database\Seeder;

class VehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vehicle::firstOrCreate([
            'name' => 'Starex',
            'capacity' => '10'
        ]);

        Vehicle::firstOrCreate([
            'name' => 'Fortuner',
            'capacity' => '6'
        ]);

        Vehicle::firstOrCreate([
            'name' => 'Hilux',
            'capacity' => '4'
        ]);

        Vehicle::firstOrCreate([
            'name' => 'Insight',
            'capacity' => '3'
        ]);

        Vehicle::firstOrCreate([
            'name' => 'Prius',
            'capacity' => '3'
        ]);

        Vehicle::firstOrCreate([
            'name' => 'Pajero (Sabah)',
            'capacity' => '6'
        ]);


    }

}
