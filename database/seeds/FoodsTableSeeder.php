<?php

use App\Food;
use Illuminate\Database\Seeder;

class FoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Food::firstOrCreate([
            'name' => 'Biscuits',
        ]);
    }
}
