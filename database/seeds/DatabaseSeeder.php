<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RoomsTableSeeder::class);
         $this->call(RoomSetupsTableSeeder::class);
         $this->call(DivisionsTableSeeder::class);
         $this->call(FoodSetupsTableSeeder::class);
         $this->call(DrinksTableSeeder::class);
         $this->call(FoodsTableSeeder::class);
         $this->call(VehiclesTableSeeder::class);
    }
}
