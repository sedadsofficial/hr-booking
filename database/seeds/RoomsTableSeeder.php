<?php

use App\Room;
use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Room::firstOrCreate([
            'name' => 'Auditorium',
            'capacity' => '100'
        ]);

        Room::firstOrCreate([
            'name' => 'Perdana Meeting Room',
            'capacity' => '25'
        ]);

        Room::firstOrCreate([
            'name' => 'Tenaga Meeting Room',
            'capacity' => '15'
        ]);

        Room::firstOrCreate([
            'name' => 'Suria Meeting Room',
            'capacity' => '15'
        ]);

        Room::firstOrCreate([
            'name' => 'Lestari Meeting Room',
            'capacity' => '40'
        ]);

        Room::firstOrCreate([
            'name' => 'Discussion Room',
            'capacity' => '15'
        ]);

        Room::firstOrCreate([
            'name' => 'Dining Room',
            'capacity' => '50'
        ]);
    }

}
