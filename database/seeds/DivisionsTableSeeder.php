<?php

use App\Division;
use Illuminate\Database\Seeder;

class DivisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * -----------
         * CEO Office
         * Special Unit
         * Legal Unit
         * Audit Unit
         * CSO Office
         * Strategic Planning Division
         * Strategic Communications Division
         * Market Operations Division
         * Technical Development and Facilitation Division
         * Finance Division
         * Human Resources and Administration Division
         * Digital Services Division
         */


        Division::firstOrCreate([
            'name' => 'CEO Office',
        ]);
        Division::firstOrCreate([
            'name' => 'Special Unit',
        ]);
        Division::firstOrCreate([
            'name' => 'Legal Unit',
        ]);
        Division::firstOrCreate([
            'name' => 'Audit Unit',
        ]);
        Division::firstOrCreate([
            'name' => 'CSO Office',
        ]);
        Division::firstOrCreate([
            'name' => 'Strategic Planning Division',
        ]);
        Division::firstOrCreate([
            'name' => 'Strategic Communications Division',
        ]);
        Division::firstOrCreate([
            'name' => 'Market Operations Division',
        ]);
        Division::firstOrCreate([
            'name' => 'Technical Development and Facilitation Division',
        ]);
        Division::firstOrCreate([
            'name' => 'Finance Division',
        ]);
        Division::firstOrCreate([
            'name' => 'Human Resources and Administration Division',
        ]);
        Division::firstOrCreate([
            'name' => 'Digital Services Division',
        ]);
    }
}
