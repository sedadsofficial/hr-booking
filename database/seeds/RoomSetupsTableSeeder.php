<?php

use App\RoomSetup;
use Illuminate\Database\Seeder;

class RoomSetupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RoomSetup::firstOrCreate([
            'name' => 'Bilateral style meeting',
        ]);
        RoomSetup::firstOrCreate([
            'name' => 'Boardroom style meeting',
        ]);
        RoomSetup::firstOrCreate([
            'name' => 'U shape style meeting',
        ]);
        RoomSetup::firstOrCreate([
            'name' => 'Interview style',
        ]);
        RoomSetup::firstOrCreate([
            'name' => 'Training',
        ]);
    }
}
