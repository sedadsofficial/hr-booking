<?php

use App\Drink;
use Illuminate\Database\Seeder;

class DrinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Drink::firstOrCreate([
            'name' => 'Plain Water',
        ]);

        Drink::firstOrCreate([
            'name' => 'Teh Tarik',
        ]);

        Drink::firstOrCreate([
            'name' => 'Teh O',
        ]);

        Drink::firstOrCreate([
            'name' => 'Nescafe',
        ]);

        Drink::firstOrCreate([
            'name' => 'Nescafe O',
        ]);
    }
}
