<?php

use App\FoodSetup;
use Illuminate\Database\Seeder;

class FoodSetupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FoodSetup::firstOrCreate([
            'name' => 'Buffet',
        ]);

        FoodSetup::firstOrCreate([
            'name' => 'Dome',
        ]);

        FoodSetup::firstOrCreate([
            'name' => 'Meeting (Individually Packed)',
        ]);

        FoodSetup::firstOrCreate([
            'name' => 'Biscuits & Drink',
        ]);
    }
}
