<?php

namespace App\Console\Commands;

use App\User;
use App\Vehicle;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EmailRoadtaxExpiryReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:roadtax';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email Reminder for Expiring Roadtax';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $vehicle = Vehicle::all();

        foreach ($vehicle as $v) {
            $roadtax_date = Carbon::parse($v->roadtax_expiry_date);
            $insurance_date = Carbon::parse($v->insurance_date);

            //Trigger email when roadtax or insurance date is expiring in 2 months or less
            if ($roadtax_date->diffInMonths(Carbon::now()) <= 2  || $insurance_date->diffInMonths(Carbon::now()) <= 2 ) {
                $admin_emails = User::where('is_admin',1)->pluck('email')->toArray();
                Mail::queue('emails.admin.admin_roadtax_expiry_reminder', compact('v'), function ($message) use($admin_emails, $v) {
                    $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                    $message->subject('Reminder: Expired Insurance / Roadtax for Car '.$v->name.' ('.$v->plate_number.')');
                    $message->to($admin_emails);
                });
            }
        }
    }
}
