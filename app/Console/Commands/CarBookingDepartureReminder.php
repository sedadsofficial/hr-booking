<?php

namespace App\Console\Commands;

use App\CarBooking;
use App\User;
use App\Vehicle;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CarBookingDepartureReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:departure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email Reminder 15 minutes before departure';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $carBooking = CarBooking::whereBookingStatus('Approved')->get();

        foreach ($carBooking as $booking) {
            $booking_date = Carbon::parse($booking->car_booking_date_from);

            //Trigger email when it is 15 minutes before departure time
            if ($booking_date->diffInMinutes(Carbon::now(),false) == -14) {
                $user = $booking->user;
                Mail::queue('emails.vehicle.applicant_booking_approved', compact('user','booking'), function ($message) use($booking, $user) {
                    $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                    $message->subject('REMINDER : Please Standby at '.$booking->waiting_area.' before '.Carbon::parse($booking->car_booking_date_from)->format('h:i A'));
                    $message->to($user->email);
                    $message->cc([$booking->driver->user->email]);
                });
            }
        }
    }
}
