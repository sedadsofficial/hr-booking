<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringItem extends Model
{
    protected $fillable = ['catering_id', 'item_type', 'item_name', 'item_remark'];

    public function catering() {
        return $this->belongsTo(Catering::class);
    }
}
