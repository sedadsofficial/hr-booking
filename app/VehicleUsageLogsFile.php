<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleUsageLogsFile extends Model
{
    public function carBooking() {
        return $this->belongsTo(CarBooking::class);
    }
}
