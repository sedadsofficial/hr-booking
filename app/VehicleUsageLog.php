<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\VehicleUsageLog
 *
 * @property int $id
 * @property int $driver_id
 * @property int $booking_id
 * @property string $actual_date_from
 * @property string $actual_date_to
 * @property float $total_mileage_per_trip
 * @property float $total_toll
 * @property float $total_petrol
 * @property float $odometer
 * @property string $method_of_petrol_purchase
 * @property string $petrol_receipt_file_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereActualDateFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereActualDateTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereDriverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereMethodOfPetrolPurchase($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereOdometer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog wherePetrolReceiptFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereTotalMileagePerTrip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereTotalPetrol($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereTotalToll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleUsageLog whereUpdatedAt($value)
 */

class VehicleUsageLog extends Model
{
    public function carBooking() {
        return $this->belongsTo(CarBooking::class, 'booking_id');
    }
}
