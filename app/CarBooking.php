<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CarBooking
 *
 * @property int $id
 * @property string $car_booking_date_from
 * @property string $car_booking_date_to
 * @property string $purpose_booking
 * @property string $purpose_detail
 * @property int $transport_type
 * @property int $driver_id
 * @property string $total_passenger
 * @property string $booking_status
 * @property string $destination
 * @property string $remarks
 * @property int $vehicle_id
 * @property int $user_id
 * @property int $division_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereBookingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereCarBookingDateFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereCarBookingDateTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereDestination($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereDriverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking wherePurposeBooking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking wherePurposeDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereTotalPassenger($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereTransportType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CarBooking whereVehicleId($value)
 */

class CarBooking extends Model
{
    protected $appends = ['is_allowed_to_cancel', 'is_allowed_to_edit', 'is_allowed_to_delete'];

    protected $allowableStatusToCancel;
    protected $allowableStatusToEdit;
    protected $allowableStatusToDelete;
    protected $allowableStatusToVerify;

    public function __construct(array $attributes = [])
    {
        $this->allowableStatusToCancel = [
            config('constant.status.APPROVED')
        ];

        $this->allowableStatusToEdit = [
            config('constant.status.PENDING_APPROVAL'),
            config('constant.status.APPROVED')
        ];

        $this->allowableStatusToDelete = [
            config('constant.status.PENDING_APPROVAL'),
        ];

        $this->allowableStatusToVerify = [
            config('constant.status.PENDING_APPROVAL'),
        ];

        parent::__construct($attributes);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function passenger() {
        return $this->hasMany(Passenger::class, 'booking_id');
    }

    public function activity() {
        return $this->morphMany(Activity::class,'activitieable');
    }

    public function vehicle() {
        return $this->belongsTo(Vehicle::class);
    }

    public function driver() {
        return $this->belongsTo(Driver::class, 'driver_id');
    }

    public function vehicle_log() {
        return $this->hasOne(VehicleUsageLog::class, 'booking_id');
    }

    public function vehicle_log_files() {
        return $this->hasMany(VehicleUsageLogsFile::class, 'booking_id');
    }

    public function delete() {
        if (!$this->passenger->isEmpty()) {
            $this->passenger()->delete();
        }
        return parent::delete();
    }

    public function getIsAllowedToCancelAttribute() {
        return in_array($this->booking_status, $this->allowableStatusToCancel);
    }

    public function getIsAllowedToEditAttribute() {
        return in_array($this->booking_status, $this->allowableStatusToEdit);
    }

    public function getIsAllowedToDeleteAttribute() {
        return in_array($this->booking_status, $this->allowableStatusToDelete);
    }

    public function getIsAllowedToVerifyAttribute() {
        return in_array($this->booking_status, $this->allowableStatusToVerify);
    }

    public function getBookingNoAttribute() {
        return 'VB'.$this->id;
    }

    public function getBookingDateAttribute() {
        $from = strtotime($this->car_booking_date_from);
        $to = strtotime($this->car_booking_date_to);
        $format = 'd/m/Y';
        return date($format,$from) . ' - ' . date($format,$to);
    }

    public function getBookingTimeAttribute() {
        $from = strtotime($this->car_booking_date_from);
        $to = strtotime($this->car_booking_date_to);
        $format = 'h:i A';
        return date($format,$from) . ' - ' . date($format,$to);
    }
}
