<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Vehicle
 *
 * @property int $id
 * @property string $name
 * @property int $capacity
 * @property int $plate_number
 * @property string $insurance_date
 * @property string $insurance_file_name
 * @property string $roadtax_expiry_date
 * @property string $roadtax_file_name
 * @property int $num_of_service
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle whereCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle whereInsuranceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle whereInsuranceFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle whereNumOfService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle wherePlateNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle whereRoadtaxExpiryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle whereRoadtaxFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vehicle whereUpdatedAt($value)
 */

class Vehicle extends Model
{
    public function carBooking() {
        return $this->hasMany(CarBooking::class);
    }

    public function serviceLog() {
        return $this->hasMany(VehicleServiceLog::class);
    }

    public function getAvailableVehicle($from, $to, $excluded_id = false) {
        if ($excluded_id) {
            $allBooking = CarBooking::with('vehicle')
                ->whereNotIn('booking_status', [config('constant.status.CANCELLED'),config('constant.status.REJECTED')])
                ->whereNotIn('id',[$excluded_id])
                ->get();
        } else {
            $allBooking = CarBooking::with('vehicle')
                ->whereNotIn('booking_status', [config('constant.status.CANCELLED'),config('constant.status.REJECTED')])
                ->get();
        }

        $vehicle = array();
        foreach ($allBooking as $booking) {
            if ($to > $booking->car_booking_date_from && $from < $booking->car_booking_date_to) {
                $vehicle[] = $booking->vehicle->id;
            }
        }
        return self::whereNotIn('id',$vehicle);
    }
}
