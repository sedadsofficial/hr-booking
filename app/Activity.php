<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Activity
 *
 * @property int $id
 * @property int $booking_id
 * @property string $activity_name
 * @property string $activity_remark
 * @property int $activity_by
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\MeetingBooking $MeetingBooking
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereActivityBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereActivityName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Activity whereUpdatedAt($value)
 */

class Activity extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'activities';

    public function MeetingBooking() {
        return $this->belongsTo(MeetingBooking::class);
    }

    public function user() {
        return $this->belongsTo(User::class, 'activity_by');
    }

    public function activitieable() {
        return $this->morphTo();
    }
}
