<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ReplacementDriver
 *
 * @property int $id
 * @property int $driver_id
 * @property int $division_id
 * @property string $license_expiry_date
 * @property string $license_file_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Driver $driver
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReplacementDriver whereDriverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReplacementDriver whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReplacementDriver whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReplacementDriver whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReplacementDriver whereLicenseExpiryDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReplacementDriver whereLicenseFileName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ReplacementDriver whereUpdatedAt($value)
 */

class ReplacementDriver extends Model
{
    public function driver() {
        return $this->belongsTo(Driver::class);
    }
}
