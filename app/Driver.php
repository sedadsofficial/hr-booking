<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Driver
 *
 * @property int $id
 * @property int $user_id
 * @property int $is_active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\ReplacementDriver $replacementDriver
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Driver whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Driver whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Driver whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Driver whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Driver whereUserId($value)
 */

class Driver extends Model
{
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function replacementDriver() {

        return $this->belongsTo(ReplacementDriver::class, 'driver_id');
    }

    public function getAvailableDriver($from, $to, $excluded_id = false) {
        if ($excluded_id) {
            $allBooking = CarBooking::with('driver')
                ->whereNotIn('booking_status', [config('constant.status.CANCELLED'),config('constant.status.REJECTED')])
                ->whereNotIn('id',[$excluded_id])
                ->get();
        } else {
            $allBooking = CarBooking::with('driver')
                ->whereNotIn('booking_status', [config('constant.status.CANCELLED'),config('constant.status.REJECTED')])
                ->get();
        }

        $driver = array();
        foreach ($allBooking as $booking) {
            if ($to > $booking->car_booking_date_from && $from < $booking->car_booking_date_to) {
                $driver[] = $booking->driver->id;
            }
        }
        return self::whereNotIn('id',$driver);
    }
}
