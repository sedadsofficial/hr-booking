<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Division
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Division whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Division whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Division whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Division whereUpdatedAt($value)
 */

class Division extends Model
{
    protected $fillable = ['name'];

    public function user() {
        return $this->hasMany(User::class);
    }
}
