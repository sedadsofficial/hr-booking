<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Refreshment
 *
 * @property int $id
 * @property int $meeting_booking_id
 * @property float $total_cost
 * @property int $description
 * @property string $order_ref_no
 * @property string $caterer_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\MeetingBooking $meetingBooking
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Refreshment whereCatererName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Refreshment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Refreshment whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Refreshment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Refreshment whereMeetingBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Refreshment whereOrderRefNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Refreshment whereTotalCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Refreshment whereUpdatedAt($value)
 * @mixin \Eloquent
 */

class Refreshment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['total_cost', 'description', 'order_ref_no', 'caterer_name'];

    public function meetingBooking() {
        return $this->belongsTo(MeetingBooking::class, 'meeting_booking_id');
    }

    public function food() {
        return $this->belongsTo(Food::class, 'foods_id');
    }

    public function drink() {
        return $this->belongsTo(Drink::class, 'drinks_id');
    }


}
