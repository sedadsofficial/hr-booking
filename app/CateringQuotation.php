<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringQuotation extends Model
{
    public function catering() {
        return $this->belongsTo(Catering::class);
    }

    public function scopeSelected($query) {
        return $query->where('is_selected', 1);
    }
}
