<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catering extends Model
{
    public function booking() {
        return $this->belongsTo(MeetingBooking::class, 'meeting_bookings_id');
    }

    public function catering_item() {
        return $this->hasMany(CateringItem::class);
    }

    public function catering_quotation() {
        return $this->hasMany(CateringQuotation::class);
    }

    public function food_setup() {
        return $this->belongsTo(FoodSetup::class, 'food_setups_id');
    }
}
