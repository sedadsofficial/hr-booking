<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * App\MeetingBooking
 *
 * @property int $id
 * @property int $room_id
 * @property int $user_id
 * @property string $meeting_date
 * @property string $meeting_desc
 * @property int $pax_no
 * @property int $has_refreshment
 * @property string $budget_line
 * @property string $meeting_date_from
 * @property string $meeting_date_to
 * @property int $division_id
 * @property int $room_setup_id
 * @property string $chairperson
 * @property int $has_catering
 * @property string $remarks
 * @property string $booking_status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $reject_remarks
 * @property string $cancel_remarks
 * @property int $cancelled_by
 * @property int $rejected_by
 * @property-read mixed $booking_date
 * @property-read mixed $booking_no
 * @property-read mixed $booking_time
 * @property-read mixed $is_allowed_to_cancel
 * @property-read mixed $is_allowed_to_delete
 * @property-read mixed $is_allowed_to_edit
 * @property-read mixed $is_allowed_to_verify
 * @property-read \App\Room $room
 * @property-read \App\User $user
 * @property-read \App\Division $division
 * @property-read \App\RoomSetup $room_setup
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Activity[] $activity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereBudgetLine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereHasRefreshment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereMeetingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereMeetingDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking wherePaxNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereRoomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereMeetingDateFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereMeetingDateTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereBookingStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereChairperson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereHasCatering($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereRoomSetupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereCancelRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereCancelledBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereRejectRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MeetingBooking whereRejectedBy($value)
 * @mixin \Eloquent
 */

class MeetingBooking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['meeting_date_from', 'meeting_date_to', 'meeting_desc', 'pax_no', 'has_refreshment'];

    protected $appends = ['is_allowed_to_cancel', 'is_allowed_to_edit', 'is_allowed_to_delete'];

    protected $allowableStatusToCancel;
    protected $allowableStatusToEdit;
    protected $allowableStatusToDelete;
    protected $allowableStatusToVerify;

    public function __construct(array $attributes = [])
    {
        $this->allowableStatusToCancel = [
            config('constant.status.PENDING_CATERER_QUOTATION'),
            config('constant.status.PENDING_CATERER_CONFIRMATION'),
            config('constant.status.APPROVED')
        ];

        $this->allowableStatusToEdit = [
            config('constant.status.PENDING_APPROVAL'),
            config('constant.status.PENDING_CATERER_QUOTATION'),
            config('constant.status.PENDING_CATERER_CONFIRMATION'),
            config('constant.status.APPROVED')
        ];

        $this->allowableStatusToDelete = [
            config('constant.status.PENDING_APPROVAL'),
        ];

        $this->allowableStatusToVerify = [
            config('constant.status.PENDING_APPROVAL'),
            config('constant.status.PENDING_CATERER_QUOTATION'),
            config('constant.status.PENDING_CATERER_CONFIRMATION')
        ];

        parent::__construct($attributes);
    }

    public function room() {
        return $this->belongsTo(Room::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function room_setup() {
        return $this->belongsTo(RoomSetup::class);
    }

    public function activity() {
        return $this->morphMany(Activity::class,'activitieable');
    }

    public function catering() {
        return $this->hasOne(Catering::class,'meeting_bookings_id');
    }

    public function catering_quotation() {
        return $this->hasManyThrough(CateringQuotation::class, Catering::class,'meeting_bookings_id', 'catering_id', 'id');
    }

    public function refreshment() {
        return $this->hasOne(Refreshment::class);
    }

    public function getIsAllowedToCancelAttribute() {
        return in_array($this->booking_status, $this->allowableStatusToCancel);
    }

    public function getIsAllowedToEditAttribute() {
        return in_array($this->booking_status, $this->allowableStatusToEdit);
    }

    public function getIsAllowedToDeleteAttribute() {
        return in_array($this->booking_status, $this->allowableStatusToDelete);
    }

    public function getIsAllowedToVerifyAttribute() {
        return in_array($this->booking_status, $this->allowableStatusToVerify);
    }

    public function getBookingNoAttribute() {
        return 'RB'.$this->id;
    }

    public function getBookingDateAttribute() {
        $from = strtotime($this->meeting_date_from);
        $to = strtotime($this->meeting_date_to);
        $format = 'd/m/Y';
        return date($format,$from) . ' - ' . date($format,$to);
    }

    public function getBookingTimeAttribute() {
        $from = strtotime($this->meeting_date_from);
        $to = strtotime($this->meeting_date_to);
        $format = 'h:i A';
        return date($format,$from) . ' - ' . date($format,$to);
    }

}
