<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\VehicleServiceLog
 *
 * @property int $id
 * @property int $vehicle_id
 * @property string $panel_workshop_name
 * @property string $service_date
 * @property string $service_desc
 * @property string $next_service_date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleServiceLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleServiceLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleServiceLog whereVehicleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleServiceLog whereNextServiceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleServiceLog wherePanelWorkshopName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleServiceLog whereServiceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleServiceLog whereServiceDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleServiceLog whereUpdatedAt($value)
 */

class VehicleServiceLog extends Model
{
    public function vehicle() {
        return $this->belongsTo(Vehicle::class);
    }
}
