<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Passenger
 *
 * @property int $id
 * @property int $booking_id
 * @property string $name
 * @property int $division_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Passenger whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Passenger whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Passenger whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Passenger whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Passenger whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Passenger whereUpdatedAt($value)
 */

class Passenger extends Model
{
    public function carBooking() {
        return $this->belongsTo(CarBooking::class, 'booking_id');
    }

    public function division() {
        return $this->belongsTo(Division::class,'division_id');
    }
}
