<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Room
 *
 * @property int $id
 * @property string $name
 * @property string $capacity
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MeetingBooking[] $meetingBooking
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereUpdatedAt($value)
 * @property string $event_color
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereEventColor($value)
 * @mixin \Eloquent
 */

class Room extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'capacity'];

    public function meetingBooking() {
        return $this->hasMany(MeetingBooking::class);
    }

    public function getAvailableRoom($from, $to, $excluded_id = false) {
        if ($excluded_id) {
            $allBooking = MeetingBooking::with('room')
                ->whereNotIn('booking_status', [config('constant.status.CANCELLED'),config('constant.status.REJECTED')])
                ->whereNotIn('id',[$excluded_id])
                ->get();
        } else {
            $allBooking = MeetingBooking::with('room')
                ->whereNotIn('booking_status', [config('constant.status.CANCELLED'),config('constant.status.REJECTED')])
                ->get();
        }

        $room = array();
        foreach ($allBooking as $booking) {
            if ($to > $booking->meeting_date_from && $from < $booking->meeting_date_to) {
                $room[] = $booking->room->id;
            }
        }
        return self::whereNotIn('id',$room);
    }
}
