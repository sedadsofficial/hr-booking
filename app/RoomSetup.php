<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\RoomSetup
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RoomSetup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RoomSetup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RoomSetup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RoomSetup whereUpdatedAt($value)
 * @mixin \Eloquent
 */

class RoomSetup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

}
