<?php

namespace App\Http\Controllers\Room;

use App\Activity;
use App\Catering;
use App\CateringItem;
use App\Division;
use App\Drink;
use App\Food;
use App\FoodSetup;
use App\Http\Controllers\Controller;
use App\MeetingBooking;
use App\Refreshment;
use App\Room;

use App\RoomSetup;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class BookingController extends Controller
{
    public function show($date) {
        $getMeetingBookingForDate = MeetingBooking::where('meeting_date',$date)->get();
//        $getAvailableRoom = Room::
//        dd($date);
        return view('show_for_date');
    }

    public function index() {
        $division = Division::pluck('name', 'id');
        $setup = RoomSetup::pluck('name', 'id');
        $food = Food::pluck('name','id');
        $drink = Drink::pluck('name','id');
        $foodSetup = FoodSetup::pluck('name','id');
        $rooms =  Room::where('is_active',1)->pluck('name','event_color');
        return view('room.home', compact('division','setup','food','drink','foodSetup','rooms'));
    }

    public function mybooking() {
        return view('room.mybooking');
    }

    public function view($id) {
        $booking = MeetingBooking::find($id);
        return view('room.view',compact('booking'));
    }

    public function edit($id) {
        $booking = MeetingBooking::find($id);
        $division = Division::pluck('name', 'id');
        $setup = RoomSetup::pluck('name', 'id');
        return view('room.edit', compact('booking','division', 'setup'));
    }

    public function save_edit($id) {

    }

    public function api_get_my_booking() {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');
        $search = Input::get('search')['value'];
        $user = Auth::user()->id;

        $data = MeetingBooking::select('rooms.*','meeting_bookings.*')
            ->where('user_id' , $user)
            ->join('rooms','meeting_bookings.room_id','=','rooms.id');

        if (!empty($search)) {
            $data->where(function ($query) use($search){
                $query->where('meeting_bookings.id','like','%'.$search.'%')
                    ->orWhere('meeting_desc','like','%'.$search.'%')
                    ->orWhere('meeting_date_from','like','%'.$search.'%')
                    ->orWhere('meeting_date_to','like','%'.$search.'%')
                    ->orWhere('meeting_date_to','like','%'.$search.'%')
                    ->orWhere('rooms.name','like','%'.$search.'%')
                    ->orWhere('booking_status','like','%'.$search.'%');
            });
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);
    }

    public function api_get_available_room() {
        $selectedDateFrom = Input::get('selectedDateFrom');
        $selectedDateTo = Input::get('selectedDateTo');
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');

        $room = new Room();
        if (Input::has('excludedId')) {
            $data =  $room->getAvailableRoom($selectedDateFrom,$selectedDateTo,Input::get('excludedId'));
        } else {
            $data =  $room->getAvailableRoom($selectedDateFrom,$selectedDateTo);
        }

        $data = $data->where('is_active', 1);

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $data->count()
        ]);
    }

    public function api_store_room_booking(Request $request) {
        $selectedDateFrom = Input::get('dateFrom');
        $selectedDateTo = Input::get('dateTo');
        $room_id = Input::get('room_id');

        foreach (Input::get('form') as $form) {
            if ($form['name'] == 'input-title') {
                $title = $form['value'];
            } elseif ($form['name'] == 'input-setup') {
                $roomSetupID = $form['value'];
            } elseif ($form['name'] == 'input-chairperson') {
                $chairperson = $form['value'];
            } elseif ($form['name'] == 'input-paxnum') {
                $totalPax = $form['value'];
            } elseif ($form['name'] == 'input-catering') {
                $hasCatering = $form['value'];
            } elseif ($form['name'] == 'input-budgetline') {
                $budgetLine = $form['value'];
            } elseif ($form['name'] == 'input-remark') {
                $remarks = $form['value'];
            } elseif ($form['name'] == 'check-require-food') {
                $requireFood = $form['value'];
            } elseif ($form['name'] == 'check-food[]') {
                $checkFood[] = $form['value'];
            } elseif ($form['name'] == 'check-drink[]') {
                $checkDrink[] = $form['value'];
            } elseif ($form['name'] == 'input-bfast-food') {
                $bfastFood = $form['value'];
            } elseif ($form['name'] == 'input-mbreak-food') {
                $mbreakFood = $form['value'];
            } elseif ($form['name'] == 'input-lunch-food') {
                $lunchFood = $form['value'];
            } elseif ($form['name'] == 'input-tbreak-food') {
                $tbreakFood = $form['value'];
            } elseif ($form['name'] == 'input-bfast-drink') {
                $bfastDrink = $form['value'];
            } elseif ($form['name'] == 'input-mbreak-drink') {
                $mbreakDrink = $form['value'];
            } elseif ($form['name'] == 'input-lunch-drink') {
                $lunchDrink = $form['value'];
            } elseif ($form['name'] == 'input-tbreak-drink') {
                $tbreakDrink = $form['value'];
            } elseif ($form['name'] == 'input-foodsetup') {
                $foodSetup = $form['value'];
            } elseif ($form['name'] == 'input-food') {
                $foodId = $form['value'];
            } elseif ($form['name'] == 'input-drink') {
                $drinkId = $form['value'];
            }
        }

        if (isset($requireFood)) {
            if ($hasCatering) {
                $mappedCheckedFood = [];
                $mappedCheckedDrink = [];

                foreach ($checkFood as $cf) {
                    switch ($cf) {
                        case 'check-bfast-food':
                            $mappedCheckedFood[] = array(
                                'type' => 'food',
                                'name' => 'Breakfast (8:00 AM - 10:00 AM)',
                                'remark' => $bfastFood
                            );
                            break;
                        case 'check-mbreak-food':
                            $mappedCheckedFood[] = array(
                                'type' => 'food',
                                'name' => 'Morning Break (10:30 AM - 11:30 AM)',
                                'remark' => $mbreakFood
                            );
                            break;
                        case 'check-lunch-food':
                            $mappedCheckedFood[] = array(
                                'type' => 'food',
                                'name' => 'Lunch (12:00 PM - 1:00 PM)',
                                'remark' => $lunchFood
                            );
                            break;
                        case 'check-tbreak-food':
                            $mappedCheckedFood[] = array(
                                'type' => 'food',
                                'name' => 'Tea Break (2:00 PM - 4:00 PM)',
                                'remark' => $tbreakFood
                            );
                            break;
                    }
                }

                foreach ($checkDrink as $cd) {
                    switch ($cd) {
                        case 'check-bfast-drink':
                            $mappedCheckedDrink[] = array(
                                'type' => 'drink',
                                'name' => 'Breakfast (8:00 AM - 10:00 AM)',
                                'remark' => $bfastDrink
                            );
                            break;
                        case 'check-mbreak-drink':
                            $mappedCheckedDrink[] = array(
                                'type' => 'drink',
                                'name' => 'Morning Break (10:30 AM - 11:30 AM)',
                                'remark' => $mbreakDrink
                            );
                            break;
                        case 'check-lunch-drink':
                            $mappedCheckedDrink[] = array(
                                'type' => 'drink',
                                'name' => 'Lunch (12:00 PM - 1:00 PM)',
                                'remark' => $lunchDrink
                            );
                            break;
                        case 'check-tbreak-drink':
                            $mappedCheckedDrink[] = array(
                                'type' => 'drink',
                                'name' => 'Tea Break (2:00 PM - 4:00 PM)',
                                'remark' => $tbreakDrink
                            );
                            break;
                    }
                }
            }
        }

        $meetingBooking = new MeetingBooking();
        $meetingBooking->room_id = $room_id;
        $meetingBooking->meeting_date_from = $selectedDateFrom;
        $meetingBooking->meeting_date_to = $selectedDateTo;
        $meetingBooking->user_id = Auth::user()->id;
        $meetingBooking->meeting_desc = $title;
        $meetingBooking->room_setup_id = $roomSetupID;
        $meetingBooking->chairperson = $chairperson;
        $meetingBooking->pax_no = $totalPax;
//        $meetingBooking->has_catering = $hasCatering;
//        $meetingBooking->budget_line = $budgetLine;
        $meetingBooking->remarks = $remarks;
        $meetingBooking->booking_status = isset($hasCatering) ? (($hasCatering) ? config('constant.status.PENDING_CATERER_QUOTATION') : config('constant.status.PENDING_APPROVAL')) : config('constant.status.PENDING_APPROVAL');

        if ($meetingBooking->save()) {
            if (isset($requireFood)) {
                if ($hasCatering) {
                    $catering = new Catering();
                    $catering->food_setups_id = $foodSetup;
                    $catering->budget_line = $budgetLine;
                    $catering->booking()->associate($meetingBooking);
                    $catering->save();

                    foreach ($mappedCheckedFood as $m) {
                        $item = new CateringItem();
                        $item->item_type = $m['type'];
                        $item->item_name = $m['name'];
                        $item->item_remark = $m['remark'];
                        $item->catering()->associate($catering);
                        $item->save();
                    }

                    foreach ($mappedCheckedDrink as $m) {
                        $item = new CateringItem();
                        $item->item_type = $m['type'];
                        $item->item_name = $m['name'];
                        $item->item_remark = $m['remark'];
                        $item->catering()->associate($catering);
                        $item->save();
                    }
                } else {
                    $refreshment = new Refreshment();
                    $refreshment->foods_id = $foodId;
                    $refreshment->drinks_id = $drinkId;
                    $refreshment->meetingBooking()->associate($meetingBooking);
                    $refreshment->save();
                }
            }

            $response = [
                'status' => 'success',
                'data' => $meetingBooking
            ];

            $user = Auth::user();
            Mail::queue('emails.room.applicant_pending_approval', compact('user','meetingBooking'), function ($message) use($user, $meetingBooking) {
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Pending Approval for Room Booking ('.$meetingBooking->booking_no.')');
                $message->to($user->email);
            });

            $admin_emails = User::where('is_admin',1)->pluck('email')->toArray();
            Mail::queue('emails.room.admin_pending_approval', compact('user','meetingBooking'), function ($message) use($admin_emails, $meetingBooking){
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Pending Approval for Room Booking ('.$meetingBooking->booking_no.')');
                $message->to($admin_emails);
            });

        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to save record'
            ];
        }
        return response()->json($response);
    }

    public function api_show_all_room_booking (Request $request) {
        $start = substr($request->get('agenda_start'),0, 10);
        $end = substr($request->get('end'),0, 10);

        $data = array();
        $meetingBooking = MeetingBooking::with(['room','user'])
            ->whereNotIn('booking_status', [config('constant.status.CANCELLED'),config('constant.status.REJECTED')])
            ->where(function ($q) use($start, $end){
                $q->whereRaw("'$start'" .'<= meeting_date_to');
                $q->whereRaw("'$end'" .'>= meeting_date_from');
            })
            ->get();

        foreach ($meetingBooking as $booking) {
            $desc = '<table class="table table-borderless table-responsive-sm">
                        <tr>
                            <td>From</td>
                            <td>'. Carbon::parse($booking->meeting_date_from)->format('d-m-Y h:i A') . '</td>
                        </tr>
                        <tr>
                            <td>To</td>
                            <td>'. Carbon::parse($booking->meeting_date_to)->format('d-m-Y h:i A') . '</td>
                        </tr>
                        <tr>
                            <td>Room</td>
                            <td>'. $booking->room->name . '</td>
                        </tr>
                        <tr>
                            <td>Booking Status</td>
                            <td><h5><span class="badge badge-secondary">'. $booking->booking_status .'</span></h5></td>
                        </tr>
                        <tr>
                            <td>Booked By</td>
                            <td>'. $booking->user->name . '</td>
                        </tr>
                     </table>';

            $data[] = array(
                'id' => $booking->id,
                'title' => $booking->meeting_desc,
                'start' => $booking->meeting_date_from,
                'end' => $booking->meeting_date_to,
                'color' => $booking->room->event_color,
                'description' => $desc,
                'allDay' => false
            );
        }
        return response()->json($data);
    }

    public function api_delete_room_booking () {
        $booking_id = Input::get('booking_id');

        $booking = MeetingBooking::find($booking_id);

        if ($booking->delete()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully delete!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to delete record'
            ];
        }
        return response()->json($response);
    }

    public function api_cancel_room_booking () {
        $booking_id = Input::get('booking_id');
        $remarks = Input::get('remarks');

        $booking = MeetingBooking::find($booking_id);
        $booking->booking_status = config('constant.status.CANCELLED');

        $activity = new Activity();
        $activity->activity_name = $booking->booking_status;
        $activity->activity_remark = $remarks;
        $activity->activity_by = Auth::user()->id;
        $booking->activity()->save($activity);

        if ($booking->save()) {
            $user = $booking->user;
            Mail::queue('emails.room.applicant_booking_cancelled', compact('user','booking', 'activity'), function ($message) use($user, $booking) {
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Room Booking Cancelled ('.$booking->booking_no.')');
                $message->to($user->email);
            });

            $admin_emails = User::where('is_admin',1)->pluck('email')->toArray();
            Mail::queue('emails.room.admin_booking_cancelled', compact('user','booking', 'activity'), function ($message) use($admin_emails, $booking){
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Room Booking Cancelled ('.$booking->booking_no.')');
                $message->to($admin_emails);
            });

            $response = [
                'status' => 'success',
                'data' => $booking
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to cancel record'
            ];
        }
        return response()->json($response);
    }

    public function api_edit_room_booking(Request $request) {
        $selectedDateFrom = Input::get('dateFrom');
        $selectedDateTo = Input::get('dateTo');
        $room_id = Input::get('room_id');
        $booking_id = Input::get('booking_id');

        foreach (Input::get('form') as $form) {
            if ($form['name'] == 'input-paxnum') {
                $totalPax = $form['value'];
                break;
            }
        }

        $meetingBooking = MeetingBooking::find($booking_id);
        $meetingBooking->room_id = $room_id;
        $meetingBooking->meeting_date_from = $selectedDateFrom;
        $meetingBooking->meeting_date_to = $selectedDateTo;
        $meetingBooking->pax_no = $totalPax;

        if ($meetingBooking->save()) {
            $response = [
                'status' => 'success',
                'data' => $meetingBooking
            ];

            $user = Auth::user();
            $admin_emails = User::where('is_admin',1)->pluck('email')->toArray();
            Mail::queue('emails.room.admin_booking_edit', compact('user','meetingBooking'), function ($message) use($admin_emails, $meetingBooking){
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Room Booking Update ('.$meetingBooking->booking_no.')');
                $message->to($admin_emails);
            });
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to save record'
            ];
        }
        return response()->json($response);
    }

    public function api_verify_booking(Request $request) {
        $booking_id = $request->get('booking_id');

        $booking = MeetingBooking::find($booking_id);
        $booking->booking_status = config('constant.status.APPROVED');

        $activity = new Activity();
        $activity->activity_name = $booking->booking_status;
        $activity->activity_by = Auth::user()->id;

        if ($booking->save() && $booking->activity()->save($activity)) {
            $user = $booking->user;
            $admin_emails = User::where('is_admin',1)->pluck('email')->toArray();

            Mail::queue('emails.room.applicant_booking_approved', compact('user','booking'), function ($message) use($booking, $user, $admin_emails) {
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Your Room Booking Has Been Approved ('.$booking->booking_no.')');
                $message->to($user->email);
                $message->cc($admin_emails);
            });

            $response = [
                'status' => 'success',
                'data' => $booking
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to update record'
            ];
        }
        return response()->json($response);

    }

}
