<?php

namespace App\Http\Controllers\Room;

use App\Activity;
use App\CateringItem;
use App\CateringQuotation;
use App\Division;
use App\Drink;
use App\Food;
use App\FoodSetup;
use App\Http\Controllers\Controller;
use App\MeetingBooking;
use App\Room;
use App\RoomSetup;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function all() {
        return view('room.admin.all_booking');
    }

    public function view($id) {
        $booking = MeetingBooking::find($id);
        $foodSetup = FoodSetup::pluck('name','id');
        return view('room.admin.view',compact('booking','foodSetup'));
    }

    public function api_get_all_booking() {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');
        $search = Input::get('search')['value'];

        $data = MeetingBooking::select('rooms.*','meeting_bookings.*')
            ->join('rooms','meeting_bookings.room_id','=','rooms.id');

        if (!empty($search)) {
            $data->where('meeting_bookings.id','like','%'.$search.'%')
                ->orWhere('meeting_desc','like','%'.$search.'%')
                ->orWhere('meeting_date_from','like','%'.$search.'%')
                ->orWhere('meeting_date_to','like','%'.$search.'%')
                ->orWhere('meeting_date_to','like','%'.$search.'%')
                ->orWhere('rooms.name','like','%'.$search.'%')
                ->orWhere('booking_status','like','%'.$search.'%');
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);
    }

    public function api_verify_booking() {
        $booking_id = Input::get('booking_id');
        $action = Input::get('action');
        $remark = Input::get('remark');

        $booking = MeetingBooking::find($booking_id);

        if ($action) { //Approved
            $booking->booking_status = config('constant.status.APPROVED');
        } else { //Rejected
            $booking->booking_status = config('constant.status.REJECTED');
        }

        $activity = new Activity();
        $activity->activity_name = $booking->booking_status;
        $activity->activity_remark = $remark;
        $activity->activity_by = Auth::user()->id;

        if ($booking->save() && $booking->activity()->save($activity)) {
            $user = $booking->user;
            $admin_emails = User::where('is_admin',1)->pluck('email')->toArray();
            if ($action) { //Approved
                Mail::queue('emails.room.applicant_booking_approved', compact('user','booking'), function ($message) use($booking, $user, $admin_emails) {
                    $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                    $message->subject('Your Room Booking Has Been Approved ('.$booking->booking_no.')');
                    $message->to($user->email);
                    $message->cc($admin_emails);
                });
            } else { //Rejected
                Mail::queue('emails.room.applicant_booking_rejected', compact('user','booking','activity'), function ($message) use($booking, $user, $admin_emails) {
                    $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                    $message->subject('Your Room Booking Has Been Rejected ('.$booking->booking_no.')');
                    $message->to($user->email);
                    $message->cc($admin_emails);
                });
            }

            $response = [
                'status' => 'success',
                'data' => $booking
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to update record'
            ];
        }
        return response()->json($response);
    }

    public function api_verify_pending_quotation(Request $request) {
        $booking_id = $request->get('booking_id');
        $action = $request->get('action');
        $remark = $request->get('remark');
        $inputCateringItem = [];

        foreach ($request->get('form') as $form) {
            if ($form['name'] == 'input-foodsetup') {
                $foodSetupID = $form['value'];
            } elseif ($form['name'] == 'input-budgetline') {
                $budgetLine = $form['value'];
            } elseif (str_contains($form['name'], 'input-food[') || str_contains($form['name'], 'input-drink[')) {
                preg_match_all("/\\[(.*?)\\]/", $form['name'], $id);
                $inputCateringItem[] = array('id' => (int)$id[1][0], 'value' => $form['value']);
            }
        }

        $booking = MeetingBooking::find($booking_id);
        if ($action == 'confirm') {
            $booking->booking_status = config('constant.status.PENDING_CATERER_CONFIRMATION');
        } elseif ($action == 'reject') {
            $booking->booking_status = config('constant.status.REJECTED');
        }

        $booking->catering->food_setups_id = $foodSetupID;
        $booking->catering->budget_line = $budgetLine;
        foreach ($inputCateringItem as $cateringItem) {
            $item = CateringItem::find($cateringItem['id']);
            $item->item_remark = $cateringItem['value'];
            $item->save();
        }

        $activity = new Activity();
        $activity->activity_name = $booking->booking_status;
        $activity->activity_by = Auth::user()->id;
        $activity->activity_remark = $remark;

        if ($booking->save() && $booking->activity()->save($activity) && $booking->catering->save()) {
            $user = $booking->user;
            $admin_emails = User::where('is_admin',1)->pluck('email')->toArray();
            if ($action == 'confirm') {
                Mail::queue('emails.room.applicant_pending_caterer_confirmation', compact('user','booking'), function ($message) use($booking, $user, $admin_emails) {
                    $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                    $message->subject('Pending Caterer Confirmation for Room Booking ('.$booking->booking_no.')');
                    $message->to($user->email);
                    $message->cc($admin_emails);
                });
            } elseif ($action == 'reject') {
                Mail::queue('emails.room.applicant_booking_rejected', compact('user','booking', 'activity'), function ($message) use($booking, $user, $admin_emails) {
                    $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                    $message->subject('Your Room Booking Has Been Rejected ('.$booking->booking_no.')');
                    $message->to($user->email);
                    $message->cc($admin_emails);
                });
            }

            $response = [
                'status' => 'success',
                'data' => $booking
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to update record'
            ];
        }
        return response()->json($response);
    }

    public function api_add_caterer_quotation(Request $request) {
        $cateringId = $request->get('catering_id');
        $catererName = $request->get('input-caterer-name');
        $catererCost = $request->get('input-caterer-cost');
        $file = $request->file('file');

        $catererQuot = new CateringQuotation();
        $catererQuot->catering_id = $cateringId;
        $catererQuot->caterer_name = $catererName;
        $catererQuot->cost = $catererCost;
        $catererQuot->file_name = uniqid().time().'.'.$file->getClientOriginalExtension();

        if ($catererQuot->save()) {
            Storage::put(
                $catererQuot->file_name,
                file_get_contents($file->getRealPath())
            );
        }

        $response = [
            'status' => 'success',
            'data' => $catererQuot
        ];

        return response()->json($response);
    }

    public function api_delete_caterer_quotation(Request $request) {
        $quotationId = $request->get('quotation_id');
        $catererQuot = CateringQuotation::find($quotationId);
        $file = $catererQuot->file_name;

        if ($catererQuot->delete() && Storage::delete($file)) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully deleted!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to delete record'
            ];
        }
        return response()->json($response);
    }

    public function api_select_caterer_quotation(Request $request) {
        $quotationId = $request->get('quotation_id');

        $catererQuot = CateringQuotation::find($quotationId);

        DB::table('catering_quotations')
            ->where('catering_id', $catererQuot->catering_id)
            ->update(['is_selected' => 0]);

        $catererQuot->is_selected = 1;

        if ($catererQuot->save()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully updated!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to update record'
            ];
        }
        return response()->json($response);
    }

    public function api_get_caterer_quotation(Request $request) {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');

        $data = CateringQuotation::where('catering_id', $request->get('catering_id'));

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $data->count()
        ]);
    }

    public function api_get_rooms_parameter() {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');

        $data = new Room();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $data->count()
        ]);
    }

    public function api_get_room_setup_parameter() {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');

        $data = new RoomSetup();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $data->count()
        ]);
    }

    public function api_get_divisions_parameter() {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');

        $data = new Division();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $data->count()
        ]);
    }

    public function api_get_food_setup_parameter() {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');

        $data = new FoodSetup();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $data->count()
        ]);
    }

    public function api_get_foods_parameter() {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');

        $data = new Food();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $data->count()
        ]);
    }

    public function api_get_drinks_parameter() {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');

        $data = new Drink();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $data->count()
        ]);
    }

    public function api_save_room_parameter(Request $request) {
        $roomId = $request->has('room_id') ? $request->get('room_id') : null;

        foreach ($request->get('form') as $form) {
            if ($form['name'] == 'input-name') {
                $name = $form['value'];
            } elseif ($form['name'] == 'input-capacity') {
                $capacity = $form['value'];
            }
        }

        if ($roomId) {
            $room = Room::find($roomId);
            $room->name = $name;
            $room->capacity = $capacity;
            $room->save();
        } else {
            $room = new Room();
            $room->name = $name;
            $room->capacity = $capacity;
            $room->is_active = 1;
            $room->save();
        }
    }

    public function api_delete_room_parameter (Request $request) {
        $roomId = $request->has('room_id') ? $request->get('room_id') : null;

        $room = Room::find($roomId);
        $room->is_active = !$room->is_active; //Instead of delete, deactivate

        if ($room->save()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully updated!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to update record'
            ];
        }
        return response()->json($response);
    }

    public function api_save_room_setup_parameter(Request $request) {
        $dataId = $request->has('data_id') ? $request->get('data_id') : null;

        foreach ($request->get('form') as $form) {
            if ($form['name'] == 'input-name') {
                $name = $form['value'];
            }
        }

        if ($dataId) {
            $roomSetup = RoomSetup::find($dataId);
            $roomSetup->name = $name;
            $roomSetup->save();
        } else {
            $roomSetup = new RoomSetup();
            $roomSetup->name = $name;
            $roomSetup->save();
        }
    }

    public function api_delete_room_setup_parameter (Request $request) {
        $dataId = $request->has('data_id') ? $request->get('data_id') : null;

        $roomSetup = RoomSetup::find($dataId);

        if ($roomSetup->delete()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully delete!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to delete record'
            ];
        }
        return response()->json($response);
    }

    public function api_save_divisions_parameter(Request $request) {
        $dataId = $request->has('data_id') ? $request->get('data_id') : null;

        foreach ($request->get('form') as $form) {
            if ($form['name'] == 'input-name') {
                $name = $form['value'];
            }
        }

        if ($dataId) {
            $divisions = Division::find($dataId);
            $divisions->name = $name;
            $divisions->save();
        } else {
            $divisions = new Division();
            $divisions->name = $name;
            $divisions->save();
        }
    }

    public function api_delete_divisions_parameter (Request $request) {
        $dataId = $request->has('data_id') ? $request->get('data_id') : null;

        $divisions = Division::find($dataId);

        if ($divisions->delete()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully delete!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to delete record'
            ];
        }
        return response()->json($response);
    }

    public function api_save_food_setup_parameter(Request $request) {
        $dataId = $request->has('data_id') ? $request->get('data_id') : null;

        foreach ($request->get('form') as $form) {
            if ($form['name'] == 'input-name') {
                $name = $form['value'];
            }
        }

        if ($dataId) {
            $foodSetup = FoodSetup::find($dataId);
            $foodSetup->name = $name;
            $foodSetup->save();
        } else {
            $foodSetup = new FoodSetup();
            $foodSetup->name = $name;
            $foodSetup->save();
        }
    }

    public function api_delete_food_setup_parameter (Request $request) {
        $dataId = $request->has('data_id') ? $request->get('data_id') : null;

        $foodSetup = FoodSetup::find($dataId);

        if ($foodSetup->delete()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully delete!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to delete record'
            ];
        }
        return response()->json($response);
    }

    public function api_save_foods_parameter(Request $request) {
        $dataId = $request->has('data_id') ? $request->get('data_id') : null;

        foreach ($request->get('form') as $form) {
            if ($form['name'] == 'input-name') {
                $name = $form['value'];
            }
        }

        if ($dataId) {
            $food = Food::find($dataId);
            $food->name = $name;
            $food->save();
        } else {
            $food = new Food();
            $food->name = $name;
            $food->save();
        }
    }

    public function api_delete_foods_parameter (Request $request) {
        $dataId = $request->has('data_id') ? $request->get('data_id') : null;

        $food = Food::find($dataId);

        if ($food->delete()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully delete!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to delete record'
            ];
        }
        return response()->json($response);
    }

    public function api_save_drinks_parameter(Request $request) {
        $dataId = $request->has('data_id') ? $request->get('data_id') : null;

        foreach ($request->get('form') as $form) {
            if ($form['name'] == 'input-name') {
                $name = $form['value'];
            }
        }

        if ($dataId) {
            $drink = Drink::find($dataId);
            $drink->name = $name;
            $drink->save();
        } else {
            $drink = new Drink();
            $drink->name = $name;
            $drink->save();
        }
    }

    public function api_delete_drinks_parameter (Request $request) {
        $dataId = $request->has('data_id') ? $request->get('data_id') : null;

        $drink = Drink::find($dataId);

        if ($drink->delete()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully delete!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to delete record'
            ];
        }
        return response()->json($response);
    }
}
