<?php

namespace App\Http\Controllers\Vehicle;

use App\Activity;
use App\CarBooking;
use App\Division;
use App\Driver;
use App\Http\Controllers\Controller;
use App\Passenger;
use App\ReplacementDriver;
use App\User;
use App\Vehicle;
use App\VehicleUsageLog;
use App\VehicleUsageLogsFile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class BookingController extends Controller
{
    public function index()
    {
        $division = Division::pluck('name', 'id');
        $driver = Driver::where('is_official_office_driver', 1)->where('is_active', 1)->with('user')->get()->pluck('user.name', 'id');
        $replacementDriver =  Driver::where('is_official_office_driver', 0)->where('is_active', 1)->with('user')->get()->pluck('user.name', 'id');
        $vehicles =  Vehicle::where('is_active', 1)->pluck('name', 'event_color');
        return view('vehicle.home', compact('division', 'driver', 'replacementDriver', 'vehicles'));
    }

    public function mybooking()
    {
        return view('vehicle.mybooking');
    }

    public function view($id)
    {
        $booking = CarBooking::find($id);
        return view('vehicle.view', compact('booking'));
    }

    public function edit($id)
    {
        $booking = CarBooking::find($id);
        $division = Division::pluck('name', 'id');
        $driver = Driver::with('user')->get()->pluck('user.name', 'id');
        return view('vehicle.edit', compact('booking', 'division', 'driver'));
    }

    public function api_get_my_booking()
    {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');
        $search = Input::get('search')['value'];
        $user = Auth::user()->id;

        $data = CarBooking::select('vehicles.*', 'car_bookings.*')
            ->where('user_id', $user)
            ->join('vehicles', 'car_bookings.vehicle_id', '=', 'vehicles.id');

        if (!empty($search)) {
            $data->where(function ($query) use ($search) {
                $query->where('car_bookings.id', 'like', '%' . $search . '%')
                    ->orWhere('purpose_detail', 'like', '%' . $search . '%')
                    ->orWhere('car_booking_date_from', 'like', '%' . $search . '%')
                    ->orWhere('car_booking_date_to', 'like', '%' . $search . '%')
                    ->orWhere('vehicles.name', 'like', '%' . $search . '%')
                    ->orWhere('booking_status', 'like', '%' . $search . '%');
            });
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);
    }

    public function api_show_all_vehicle_booking(Request $request)
    {
        $start = substr($request->get('agenda_start'), 0, 10);
        $end = substr($request->get('end'), 0, 10);

        $data = array();
        $carBooking = CarBooking::with(['vehicle', 'user'])
            ->whereNotIn('booking_status', [config('constant.status.CANCELLED'), config('constant.status.REJECTED')])
            ->where(function ($q) use ($start, $end) {
                $q->whereRaw("'$start'" . '<= car_booking_date_to');
                $q->whereRaw("'$end'" . '>= car_booking_date_from');
            })
            ->get();

        foreach ($carBooking as $booking) {
            $desc = '<table class="table table-borderless table-responsive-sm">
                        <tr>
                            <td>From</td>
                            <td>' . Carbon::parse($booking->car_booking_date_from)->format('d-m-Y h:i') . '</td>
                        </tr>
                        <tr>
                            <td>To</td>
                            <td>' . Carbon::parse($booking->car_booking_date_to)->format('d-m-Y h:i') . '</td>
                        </tr>
                        <tr>
                            <td>Vehicle</td>
                            <td>' . $booking->vehicle->name . '</td>
                        </tr>
                        <tr>
                            <td>Booking Status</td>
                            <td><h5><span class="badge badge-secondary">' . $booking->booking_status . '</span></h5></td>
                        </tr>
                        <tr>
                            <td>Booked By</td>
                            <td>' . $booking->user->name . '</td>
                        </tr>
                     </table>';

            $data[] = array(
                'id' => $booking->id,
                'title' => $booking->purpose_detail,
                'start' => $booking->car_booking_date_from,
                'end' => $booking->car_booking_date_to,
                'color' => $booking->vehicle->event_color,
                'description' => $desc,
                'allDay' => false
            );
        }
        return response()->json($data);
    }

    public function api_get_available_vehicle()
    {
        $selectedDateFrom = Input::get('selectedDateFrom');
        $selectedDateTo = Input::get('selectedDateTo');
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');

        $room = new Vehicle();
        if (Input::has('excludedId')) {
            $data =  $room->getAvailableVehicle($selectedDateFrom, $selectedDateTo, Input::get('excludedId'));
        } else {
            $data =  $room->getAvailableVehicle($selectedDateFrom, $selectedDateTo);
        }

        $data = $data->where('is_active', 1)
            ->where('insurance_date', '>=', Carbon::parse($selectedDateFrom)->startOfDay())
            ->where('insurance_date', '>=', Carbon::parse($selectedDateTo)->startOfDay())
            ->where('roadtax_expiry_date', '>=', Carbon::parse($selectedDateFrom)->startOfDay())
            ->where('roadtax_expiry_date', '>=', Carbon::parse($selectedDateTo)->startOfDay());

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $data->count()
        ]);
    }

    public function api_store_vehicle_booking(Request $request)
    {
        $selectedDateFrom = $request->get('dateFrom');
        $selectedDateTo = $request->get('dateTo');
        $vehicle_id = $request->get('vehicle_id');

        $param = array();
        parse_str($request->get('form'), $param);

        if ($param['input-transport-type'] == 1) { //Transport With Driver
            $driverId = $param['input-driver'];
        } else { //else 0, Transport only
            $driverId = $param['input-driver-transport-only'];
        }

        $carBooking = new CarBooking();
        $carBooking->car_booking_date_from = $selectedDateFrom;
        $carBooking->car_booking_date_to = $selectedDateTo;
        $carBooking->vehicle_id = $vehicle_id;
        $carBooking->user_id = Auth::user()->id;
        $carBooking->destination = $param['input-destination'];
        $carBooking->purpose_booking = json_encode($param['check-purpose']);
        $carBooking->purpose_booking_others = $param['input-others'];
        $carBooking->purpose_detail = $param['input-detail-of-purpose'];
        $carBooking->transport_type = $param['input-transport-type'];
        $carBooking->driver_id = $driverId;
        $carBooking->booking_status = config('constant.status.PENDING_APPROVAL');
        $carBooking->remarks = $param['input-remark'];

        if ($carBooking->save()) {
            if (isset($param['input-name'])) { //If have passenger
                $passengerArr = array();
                foreach ($param['input-name'] as $key => $val) {
                    $val2 = $param['input-division'][$key];
                    $passengerArr[$key] = ['name' => $val, 'div' => $val2];
                }

                foreach ($passengerArr as $p) {
                    $passenger = new Passenger();
                    $passenger->name = $p['name'];
                    $passenger->division_id = $p['div'];
                    $passenger->carBooking()->associate($carBooking);
                    $passenger->save();
                }
            }

            $response = [
                'status' => 'success',
                'data' => $carBooking
            ];

            $user = Auth::user();
            Mail::queue('emails.vehicle.applicant_pending_approval', compact('user', 'carBooking'), function ($message) use ($user, $carBooking) {
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Pending Approval for Car Booking (' . $carBooking->booking_no . ')');
                $message->to($user->email);
            });

            $admin_emails = User::where('is_admin', 1)->pluck('email')->toArray();
            Mail::queue('emails.vehicle.admin_pending_approval', compact('user', 'carBooking'), function ($message) use ($admin_emails, $carBooking) {
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Pending Approval for Car Booking (' . $carBooking->booking_no . ')');
                $message->to($admin_emails);
            });
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to save record'
            ];
        }
        return response()->json($response);
    }

    public function api_edit_vehicle_booking(Request $request)
    {
        $selectedDateFrom = Input::get('dateFrom');
        $selectedDateTo = Input::get('dateTo');
        $vehicle_id = Input::get('vehicle_id');
        $booking_id = Input::get('booking_id');

        $param = array();
        parse_str($request->get('form'), $param);

        $carBooking = CarBooking::find($booking_id);
        $carBooking->vehicle_id = $vehicle_id;
        $carBooking->destination = $param['input-destination'];
        $carBooking->car_booking_date_from = $selectedDateFrom;
        $carBooking->car_booking_date_to = $selectedDateTo;

        if (isset($param['input-driver'])) {
            $carBooking->driver_id = $param['input-driver'];
        }

        //flush out passengers on every Edit
        if ($carBooking->passenger->count() > 0) {
            $carBooking->passenger()->delete();
        }

        if (isset($param['input-name'])) { //If have passenger
            $passengerArr = array();
            foreach ($param['input-name'] as $key => $val) {
                $val2 = $param['input-division'][$key];
                $passengerArr[$key] = ['name' => $val, 'div' => $val2];
            }

            foreach ($passengerArr as $p) {
                $passenger = new Passenger();
                $passenger->name = $p['name'];
                $passenger->division_id = $p['div'];
                $passenger->carBooking()->associate($carBooking);
                $passenger->save();
            }
        }

        if ($carBooking->save()) {
            $response = [
                'status' => 'success',
                'data' => $carBooking
            ];

            $user = Auth::user();
            $admin_emails = User::where('is_admin', 1)->pluck('email')->toArray();
            $cc_mailing_list = $admin_emails;

            if ($carBooking->booking_status == config('constant.status.APPROVED')) { //If application is approved and edit has been made, notify driver
                array_push($cc_mailing_list, $carBooking->driver->user->email);
            }

            Mail::queue('emails.vehicle.admin_booking_edit', compact('user', 'carBooking'), function ($message) use ($cc_mailing_list, $carBooking) {
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Car Booking Update (' . $carBooking->booking_no . ')');
                $message->to($cc_mailing_list);
            });
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to save record'
            ];
        }
        return response()->json($response);
    }

    public function api_delete_vehicle_booking()
    {
        $booking_id = Input::get('booking_id');

        $booking = CarBooking::find($booking_id);

        if ($booking->delete()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully deleted!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to delete record'
            ];
        }
        return response()->json($response);
    }

    public function api_cancel_vehicle_booking()
    {
        $booking_id = Input::get('booking_id');
        $remarks = Input::get('remarks');

        $booking = CarBooking::find($booking_id);
        $booking->booking_status = config('constant.status.CANCELLED');

        $activity = new Activity();
        $activity->activity_name = $booking->booking_status;
        $activity->activity_remark = $remarks;
        $activity->activity_by = Auth::user()->id;
        $booking->activity()->save($activity);

        if ($booking->save()) {
            $user = $booking->user;
            Mail::queue('emails.vehicle.applicant_booking_cancelled', compact('user', 'booking', 'activity'), function ($message) use ($user, $booking) {
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Car Booking Cancelled (' . $booking->booking_no . ')');
                $message->to($user->email);
                $message->cc($booking->driver->user->email);
            });

            $admin_emails = User::where('is_admin', 1)->pluck('email')->toArray();
            Mail::queue('emails.vehicle.admin_booking_cancelled', compact('user', 'booking', 'activity'), function ($message) use ($admin_emails, $booking) {
                $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                $message->subject('Car Booking Cancelled (' . $booking->booking_no . ')');
                $message->to($admin_emails);
            });

            $response = [
                'status' => 'success',
                'data' => $booking
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to cancel record'
            ];
        }
        return response()->json($response);
    }

    public function show_jobs()
    {
        return view('vehicle.jobs');
    }

    public function api_get_my_jobs()
    {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');
        $search = Input::get('search')['value'];
        $driverId = Auth::user()->driver->id;

        $data = CarBooking::select('vehicles.*', 'car_bookings.*')
            ->where('driver_id', $driverId)
            ->join('vehicles', 'car_bookings.vehicle_id', '=', 'vehicles.id');

        if (!empty($search)) {
            $data->where(function ($query) use ($search) {
                $query->where('car_bookings.id', 'like', '%' . $search . '%')
                    ->orWhere('purpose_detail', 'like', '%' . $search . '%')
                    ->orWhere('car_booking_date_from', 'like', '%' . $search . '%')
                    ->orWhere('car_booking_date_to', 'like', '%' . $search . '%')
                    ->orWhere('vehicles.name', 'like', '%' . $search . '%')
                    ->orWhere('booking_status', 'like', '%' . $search . '%');
            });
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);
    }

    public function view_jobs($id)
    {
        $booking = CarBooking::find($id);
        return view('vehicle.jobs_view', compact('booking'));
    }

    public function api_save_vehicle_logbook(Request $request)
    {
        $actualDateFrom = Input::get('dateFrom');
        $actualDateTo = Input::get('dateTo');
        $booking_id = Input::get('booking_id');

        $param = array();
        parse_str($request->get('form'), $param);

        $booking = CarBooking::find($booking_id);

        $log = $booking->vehicle_log ? VehicleUsageLog::where('booking_id', $booking_id)->first() : new VehicleUsageLog();
        $log->booking_id = $booking_id;
        $log->destination_details = $param['input-destination-details'];
        $log->actual_date_from = $actualDateFrom;
        $log->actual_date_to = $actualDateTo;
        $log->total_mileage_per_trip = $param['input-total-mileage'];
        $log->total_toll = $param['input-total-toll'];
        $log->total_petrol = $param['input-total-petrol'];
        $log->odometer = $param['input-odometer'];
        $log->method_of_petrol_purchase = isset($param['input-petrol-purchase-method']) ? $param['input-petrol-purchase-method'] : '';

        if ($log->save()) {
            $response = [
                'status' => 'success',
                'data' => $log
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to save record'
            ];
        }
        return response()->json($response);
    }

    public function api_upload_vehicle_log_files(Request $request)
    {
        $bookingId = $request->get('booking_id');
        $fileTitle = $request->get('input-file-name');
        $file = $request->file('file');

        $logFile = new VehicleUsageLogsFile();
        $logFile->booking_id = $bookingId;
        $logFile->file_title = $fileTitle;
        $logFile->file_name = uniqid() . time() . '.' . $file->getClientOriginalExtension();

        if ($logFile->save()) {
            Storage::put(
                $logFile->file_name,
                file_get_contents($file->getRealPath())
            );

            $response = [
                'status' => 'success',
                'data' => $logFile
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to upload file'
            ];
        }
        return response()->json($response);
    }

    public function api_get_vehicle_log_files(Request $request)
    {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');

        $data = VehicleUsageLogsFile::where('booking_id', $request->get('booking_id'));

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $data->count(),
            'recordsFiltered' => $data->count()
        ]);
    }

    public function api_delete_vehicle_log_file(Request $request)
    {
        $logFileId = $request->get('log_file_id');
        $logFile = VehicleUsageLogsFile::find($logFileId);
        $file = $logFile->file_name;

        if ($logFile->delete() && Storage::delete($file)) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully deleted!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to delete record'
            ];
        }
        return response()->json($response);
    }

    public function report()
    {
        $vehicle = new Vehicle();
        $vehicle = $vehicle->pluck('name', 'id');
        return view('vehicle.admin.reporting', compact('vehicle'));
    }

    public function api_generate_report(Request $request)
    {
        $startPagination = $request->get('start');
        $endPagination = $request->get('length');
        $draw = $request->get('draw');
        $columns = $request->get('columns');
        $order = $request->get('order');

        $vehicle_id = $request->get('vehicle_id');
        $date_from = $request->get('dateFrom');
        $date_to = $request->get('dateTo');

        $data = VehicleUsageLog::select(
            'vehicle_usage_logs.*',
            'vehicles.id as vehicle_id',
            'car_bookings.purpose_detail',
            'car_bookings.car_booking_date_from',
            'car_bookings.car_booking_date_to'
        )
            ->where('vehicle_id', $vehicle_id)
            ->leftJoin('car_bookings', 'vehicle_usage_logs.booking_id', '=', 'car_bookings.id')
            ->leftJoin('vehicles', 'car_bookings.vehicle_id', '=', 'vehicles.id');

        if ($date_from) {
            $data->where('car_booking_date_from', '>=', $date_from);
        }

        if ($date_to) {
            $data->where('car_booking_date_to', '<=', $date_to);
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);
    }

    public function api_job_done(Request $request)
    {
        $booking_id = $request->get('bookingId');
        $new_time = $request->get('newDateTo');

        $booking = CarBooking::find($booking_id);
        $booking->car_booking_date_to = $new_time;

        if ($booking->save()) {
            $response = [
                'status' => 'success',
                'data' => $booking
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to edit record'
            ];
        }
        return response()->json($response);
    }
}
