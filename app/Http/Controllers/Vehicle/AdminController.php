<?php

namespace App\Http\Controllers\Vehicle;

use App\Activity;
use App\CarBooking;
use App\Division;
use App\Driver;
use App\Http\Controllers\Controller;
use App\User;
use App\Vehicle;
use App\VehicleUsageLog;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function all() {
        return view('vehicle.admin.all_booking');
    }

    public function view($id) {
        $booking = CarBooking::find($id);
        $selectedDateFrom = $booking->car_booking_date_from;
        $selectedDateTo = $booking->car_booking_date_to;

        $driver = new Driver();
        $dataDriver =  $driver->getAvailableDriver($selectedDateFrom,$selectedDateTo,$id);
        $driver = $dataDriver->where('is_official_office_driver', 1)->with('user')->get()->pluck('user.name','id');

        $vehicle = new Vehicle();
        $dataVehicle = $vehicle->getAvailableVehicle($selectedDateFrom,$selectedDateTo,$id);
        $vehicle = $dataVehicle->get()->pluck('name','id');
        return view('vehicle.admin.view',compact('booking', 'driver', 'vehicle'));
    }

    public function edit($id) {
        $booking = CarBooking::find($id);
        $selectedDateFrom = $booking->car_booking_date_from;
        $selectedDateTo = $booking->car_booking_date_to;

        $division = Division::pluck('name', 'id');

        $driver = new Driver();
        $dataDriver =  $driver->getAvailableDriver($selectedDateFrom,$selectedDateTo,$id);
        $driver = $dataDriver->where('is_official_office_driver', 1)->with('user')->get()->pluck('user.name','id');

        $replacementDriver = new Driver();
        $replacementDataDriver =  $replacementDriver->getAvailableDriver($selectedDateFrom,$selectedDateTo,$id);
        $replacementDriver = $replacementDataDriver->where('is_official_office_driver', 0)->with('user')->get()->pluck('user.name','id');
        return view('vehicle.admin.edit', compact('booking','division','driver','replacementDriver'));
    }

    public function report() {
        $vehicle = new Vehicle();
        $vehicle = $vehicle->pluck('name','id');
        return view('vehicle.admin.reporting', compact('vehicle'));
    }

    public function api_generate_report(Request $request) {
        $startPagination = $request->get('start');
        $endPagination = $request->get('length');
        $draw = $request->get('draw');
        $columns = $request->get('columns');
        $order = $request->get('order');

        $vehicle_id = $request->get('vehicle_id');
        $date_from = $request->get('dateFrom');
        $date_to = $request->get('dateTo');

        $data = VehicleUsageLog::select('vehicle_usage_logs.*',
            'vehicles.id as vehicle_id',
            'car_bookings.purpose_detail',
            'car_bookings.car_booking_date_from',
            'car_bookings.car_booking_date_to')
            ->where('vehicle_id', $vehicle_id)
            ->leftJoin('car_bookings','vehicle_usage_logs.booking_id','=','car_bookings.id')
            ->leftJoin('vehicles','car_bookings.vehicle_id','=','vehicles.id');

        if ($date_from) {
            $data->where('car_booking_date_from', '>=', $date_from);
        }

        if ($date_to) {
            $data->where('car_booking_date_to', '<=', $date_to);
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);

    }

    public function api_get_all_booking() {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');
        $search = Input::get('search')['value'];

        $data = CarBooking::select('vehicles.*','car_bookings.*')
            ->join('vehicles','car_bookings.vehicle_id','=','vehicles.id');

        if (!empty($search)) {
            $data->where(function ($query) use($search){
                $query->where('car_bookings.id','like','%'.$search.'%')
                    ->orWhere('purpose_detail','like','%'.$search.'%')
                    ->orWhere('car_booking_date_from','like','%'.$search.'%')
                    ->orWhere('car_booking_date_to','like','%'.$search.'%')
                    ->orWhere('vehicles.name','like','%'.$search.'%')
                    ->orWhere('booking_status','like','%'.$search.'%');
            });
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);
    }

    public function api_verify_booking() {
        $booking_id = Input::get('booking_id');
        $action = Input::get('action');
        $remark = Input::get('remark');

        $booking = CarBooking::find($booking_id);

        if ($action) { //Approved
            $booking->booking_status = config('constant.status.APPROVED');
            $booking->waiting_area = 'B2'; //Default to B2
        } else { //Rejected
            $booking->booking_status = config('constant.status.REJECTED');
        }

        $activity = new Activity();
        $activity->activity_name = $booking->booking_status;
        $activity->activity_remark = $remark;
        $activity->activity_by = Auth::user()->id;

        if ($booking->save() && $booking->activity()->save($activity)) {
            $user = $booking->user;
            $admin_emails = User::where('is_admin',1)->pluck('email')->toArray();
            $cc_mailing_list = $admin_emails;
            array_push($cc_mailing_list, $booking->driver->user->email);
            if ($action) { //Approved
                Mail::queue('emails.vehicle.applicant_booking_approved', compact('user','booking'), function ($message) use($booking, $user, $admin_emails, $cc_mailing_list) {
                    $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                    $message->subject('Your Car Booking Has Been Approved ('.$booking->booking_no.')');
                    $message->to($user->email);
                    $message->cc($cc_mailing_list);
                });
            } else { //Rejected
                Mail::queue('emails.vehicle.applicant_booking_rejected', compact('user','booking','activity'), function ($message) use($booking, $user, $admin_emails) {
                    $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                    $message->subject('Your Car Booking Has Been Rejected ('.$booking->booking_no.')');
                    $message->to($user->email);
                    $message->cc($admin_emails);
                });
            }

            $response = [
                'status' => 'success',
                'data' => $booking
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to update record'
            ];
        }
        return response()->json($response);
    }

    public function api_verify_booking_with_driver(Request $request) {
        $booking_id = $request->get('booking_id');
        $action = $request->get('action');
        $remark = $request->get('remark');

        $param = array();
        parse_str($request->get('form'), $param);

        $booking = CarBooking::find($booking_id);
        if ($action) { //Approved
            $booking->booking_status = config('constant.status.APPROVED');
            $booking->driver_id = $param['input-driver'];
            $booking->vehicle_id = $param['input-transport'];
            $booking->waiting_area = $param['input-waiting-area'];
        } else { // Rejected
            $booking->booking_status = config('constant.status.REJECTED');
        }

        $activity = new Activity();
        $activity->activity_name = $booking->booking_status;
        $activity->activity_by = Auth::user()->id;
        $activity->activity_remark = $remark;

        if ($booking->save() && $booking->activity()->save($activity)) {
            $user = $booking->user;
            $admin_emails = User::where('is_admin',1)->pluck('email')->toArray();
            $cc_mailing_list = $admin_emails;
            array_push($cc_mailing_list, $booking->driver->user->email);
            if ($action) { //Approved
                Mail::queue('emails.vehicle.applicant_booking_approved', compact('user','booking'), function ($message) use($cc_mailing_list, $booking, $user, $admin_emails) {
                    $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                    $message->subject('Your Car Booking Has Been Approved ('.$booking->booking_no.')');
                    $message->to($user->email);
                    $message->cc($cc_mailing_list);
                });
            } else { //Rejected
                Mail::queue('emails.vehicle.applicant_booking_rejected', compact('user','booking','activity'), function ($message) use($booking, $user, $admin_emails) {
                    $message->from('hr@seda.gov.my', config('constant.APP_NAME'));
                    $message->subject('Your Car Booking Has Been Rejected ('.$booking->booking_no.')');
                    $message->to($user->email);
                    $message->cc($admin_emails);
                });
            }

            $response = [
                'status' => 'success',
                'data' => $booking
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to update record'
            ];
        }
        return response()->json($response);
    }
}
