<?php

namespace App\Http\Controllers\Administrator;

use App\Activity;
use App\CarBooking;
use App\CateringItem;
use App\CateringQuotation;
use App\Division;
use App\Drink;
use App\Driver;
use App\Food;
use App\FoodSetup;
use App\Http\Controllers\Controller;
use App\MeetingBooking;
use App\ReplacementDriver;
use App\Room;
use App\RoomSetup;
use App\User;
use App\Vehicle;
use App\VehicleServiceLog;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function global_parameter() {
        return view('admin.global_parameter');
    }

    public function user_management() {
        $division = Division::pluck('name', 'id');
        return view('admin.global_user_management', compact('division'));
    }

    public function room_parameter() {
        return view('admin.room_parameter');
    }

    public function vehicle_parameter() {
        return view('admin.vehicle_parameter');
    }

    public function driver_management() {
        return view('admin.vehicle_driver_management');
    }

    public function service_logbook($id) {
        $vehicle = Vehicle::find($id);
        return view('admin.vehicle_service_logbook', compact('vehicle'));
    }

    public function api_get_all_drivers(Request $request) {
        $startPagination = $request->get('start');
        $endPagination = $request->get('length');
        $draw = $request->get('draw');
        $columns = $request->get('columns');
        $order = $request->get('order');
        $search = $request->get('search')['value'];

        $data = Driver::select('drivers.*','users.name AS name','divisions.name AS division','replacement_drivers.id AS replacement_driver_id','license_expiry_date','license_file_name')
            ->join('users','drivers.user_id','=','users.id')
            ->leftJoin('replacement_drivers','drivers.id','=','replacement_drivers.driver_id')
            ->join('divisions','users.division_id','=','divisions.id');

        if (!empty($search)) {
            $data->where(function ($query) use($search){
                $query->where('users.name','like','%'.$search.'%')
                    ->orWhere('divisions.name','like','%'.$search.'%');
            });
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);
    }

    public function api_add_replacement_driver(Request $request) {
        $userId = $request->get('user_id');
        $expiryDate = $request->get('expiry_date');
        $file = $request->file('file');

        $driver = new Driver();
        $driver->user_id = $userId;
        $driver->is_active = 1;
        $driver->is_official_office_driver = 0;

        if ($driver->save()) {
            $replacementDriver = new ReplacementDriver();
            $replacementDriver->driver_id = $driver->id;
            $replacementDriver->license_expiry_date = $expiryDate;
            $replacementDriver->license_file_name = uniqid().time().'.'.$file->getClientOriginalExtension();

            if ($replacementDriver->save()) {
                Storage::put(
                    $replacementDriver->license_file_name,
                    file_get_contents($file->getRealPath())
                );

                $response = [
                    'status' => 'success',
                    'data' => $replacementDriver
                ];
            } else {
                $response = [
                    'status' => 'fail',
                    'message' => 'Unable to add driver'
                ];
            }
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to add driver'
            ];
        }
        return response()->json($response);
    }

    public function api_edit_replacement_driver(Request $request) {
        $driverId = $request->get('driver_id');
        $expiryDate = $request->get('expiry_date');
        $file = $request->file('file');

        $replacementDriver = ReplacementDriver::find($driverId);
        $replacementDriver->license_expiry_date = $expiryDate;

        if($request->hasFile('file')) {
            Storage::delete($replacementDriver->license_file_name);
            $replacementDriver->license_file_name = uniqid().time().'.'.$file->getClientOriginalExtension();
            Storage::put(
                $replacementDriver->license_file_name,
                file_get_contents($file->getRealPath())
            );
        }

        if ($replacementDriver->save()) {
            $response = [
                'status' => 'success',
                'data' => $replacementDriver
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to edit replacement driver'
            ];
        }
        return response()->json($response);
    }

    public function api_add_office_driver(Request $request) {
        $userId = $request->get('user_id');

        $driver = new Driver();
        $driver->user_id = $userId;
        $driver->is_active = 1;
        $driver->is_official_office_driver = 1;

        if ($driver->save()) {
            $response = [
                'status' => 'success',
                'data' => $driver
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to add driver'
            ];
        }
        return response()->json($response);
    }

    public function api_activate_or_deactivate_driver(Request $request) {
        $driverId = $request->get('driver_id');

        $driver = Driver::find($driverId);
        $driver->is_active = !$driver->is_active;

        if ($driver->save()) {
            $response = [
                'status' => 'success',
                'data' => $driver
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to switch status'
            ];
        }
        return response()->json($response);

    }

    public function api_get_all_users(Request $request) {
        $startPagination = $request->get('start');
        $endPagination = $request->get('length');
        $draw = $request->get('draw');
        $columns = $request->get('columns');
        $order = $request->get('order');
        $search = $request->get('search')['value'];

        $data = User::select('users.*','users.name AS name','divisions.name AS division','drivers.id AS driver_id')
            ->join('divisions','users.division_id','=','divisions.id')
            ->leftJoin  ('drivers','users.id','=','drivers.user_id');

        if (!empty($search)) {
            $data->where(function ($query) use($search){
                $query->where('users.name','like','%'.$search.'%')
                    ->orWhere('users.email','like','%'.$search.'%')
                    ->orWhere('divisions.name','like','%'.$search.'%');
            });
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);
    }

    public function api_get_vehicles_parameter() {
        $startPagination = Input::get('start');
        $endPagination = Input::get('length');
        $draw = Input::get('draw');
        $columns = Input::get('columns');
        $order = Input::get('order');
        $search = Input::get('search')['value'];

        $data = Vehicle::select();

        if (!empty($search)) {
            $data->where(function ($query) use($search){
                $query->where('name','like','%'.$search.'%')
                    ->orWhere('capacity','like','%'.$search.'%')
                    ->orWhere('plate_number','like','%'.$search.'%')
                    ->orWhere('insurance_date','like','%'.$search.'%')
                    ->orWhere('roadtax_expiry_date','like','%'.$search.'%');
            });
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);
    }

    public function api_save_vehicles_parameter(Request $request) {
        $vehicleId = $request->has('vehicle_id') ? $request->get('vehicle_id') : null;

        $name = $request->get('name');
        $capacity = $request->get('capacity');
        $plateNum = $request->get('plate_num');
        $insuranceDate = $request->get('insurance_date');
        $roadtaxDate = $request->get('roadtax_date');
        $insuranceFile = $request->file('insurance_file');
        $roadtaxFile = $request->file('roadtax_file');

        $vehicle = $vehicleId ? Vehicle::find($vehicleId) : new Vehicle();
        $vehicle->name = $name;
        $vehicle->capacity = $capacity;
        $vehicle->plate_number = $plateNum;
        $vehicle->insurance_date = $insuranceDate;
        $vehicle->roadtax_expiry_date = $roadtaxDate;

        if ($request->hasFile('insurance_file')) {
            if (!empty($vehicle->insurance_file_name)) {
                Storage::delete($vehicle->insurance_file_name);
            }
            $vehicle->insurance_file_name = uniqid().time().'.'.$insuranceFile->getClientOriginalExtension();
            Storage::put(
                $vehicle->insurance_file_name,
                file_get_contents($insuranceFile->getRealPath())
            );
        }

        if($request->hasFile('roadtax_file')) {
            if (!empty($vehicle->roadtax_file_name)) {
                Storage::delete($vehicle->roadtax_file_name);
            }
            $vehicle->roadtax_file_name = uniqid().time().'.'.$roadtaxFile->getClientOriginalExtension();
            Storage::put(
                $vehicle->roadtax_file_name,
                file_get_contents($roadtaxFile->getRealPath())
            );
        }

        if ($vehicle->save()) {
            $response = [
                'status' => 'success',
                'data' => $vehicle
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to save record'
            ];
        }
        return response()->json($response);
    }

    public function api_delete_vehicles_parameter (Request $request) {
        $vehicleId = $request->has('vehicle_id') ? $request->get('vehicle_id') : null;

        $vehicle = Vehicle::find($vehicleId);
        $vehicle->is_active = !$vehicle->is_active; //Instead of delete, deactivate

//        if (!empty($vehicle->insurance_file_name)) {
//            Storage::delete($vehicle->insurance_file_name);
//        }
//
//        if (!empty($vehicle->roadtax_file_name)) {
//            Storage::delete($vehicle->roadtax_file_name);
//        }

        if ($vehicle->save()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully updated!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to update record'
            ];
        }
        return response()->json($response);
    }

    public function api_get_service_log(Request $request) {
        $startPagination = $request->get('start');
        $endPagination = $request->get('length');
        $draw = $request->get('draw');
        $columns = $request->get('columns');
        $order = $request->get('order');
        $search = $request->get('search')['value'];
        $vehicle = $request->get('vehicle_id');

        $data = VehicleServiceLog::select('vehicle_service_logs.*')
            ->where('vehicle_id' , $vehicle);

        if (!empty($search)) {
            $data->where(function ($query) use($search){
                $query->where('panel_workshop_name','like','%'.$search.'%')
                    ->orWhere('service_date','like','%'.$search.'%')
                    ->orWhere('service_desc','like','%'.$search.'%')
                    ->orWhere('next_service_date','like','%'.$search.'%');
            });
        }

        $totalRecords = $data->get()->count();

        if ($endPagination == -1) {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->get();
        } else {
            $pagedData = $data->orderBy($columns[$order[0]['column']]['data'], $order[0]['dir'])->offset($startPagination)->limit($endPagination)->get();
        }

        return response()->json([
            'draw' => $draw,
            'data' => $pagedData,
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $totalRecords
        ]);
    }

    public function api_save_service_log (Request $request) {
        $serviceDate = $request->get('dateService');
        $nextServiceDate = $request->get('dateNextService');
        $vehicleId = $request->get('vehicleId');
        $serviceLogId = $request->has('serviceLogId') ? $request->get('serviceLogId') : null;
        $param = array();
        parse_str($request->get('form'), $param);

        $serviceLog = $serviceLogId ? VehicleServiceLog::find($serviceLogId) : new VehicleServiceLog();
        $serviceLog->vehicle_id = $vehicleId;
        $serviceLog->panel_workshop_name = $param['input-workshop-name'];
        $serviceLog->service_desc = $param['input-service-desc'];
        $serviceLog->service_date = $serviceDate;
        $serviceLog->next_service_date = $nextServiceDate;

        if ($serviceLog->save()) {
            $response = [
                'status' => 'success',
                'data' => $serviceLog
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to save record'
            ];
        }
        return response()->json($response);
    }

    public function api_delete_service_log (Request $request) {
        $serviceLogId = $request->has('serviceLogId') ? $request->get('serviceLogId') : null;

        $serviceLog = VehicleServiceLog::find($serviceLogId);

        if ($serviceLog->delete()) {
            $response = [
                'status' => 'success',
                'data' => 'Record successfully delete!'
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to delete record'
            ];
        }
        return response()->json($response);
    }

    public function api_grant_or_revoke_admin(Request $request) {
        $userId = $request->get('user_id');

        $user = User::find($userId);
        $user->is_admin = !$user->is_admin;

        if ($user->save()) {
            $response = [
                'status' => 'success',
                'data' => $user
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to switch status'
            ];
        }
        return response()->json($response);

    }

    public function api_save_user_info(Request $request) {
        $userId = $request->get('user_id');
        $user = User::find($userId);

        foreach ($request->get('form') as $form) {
            if ($form['name'] == 'input-name') {
                $name = $form['value'];
            } elseif ($form['name'] == 'input-email') {
                $email = $form['value'];
            } elseif ($form['name'] == 'division') {
                $division = $form['value'];
            }
        }

        $user->name = $name;
        $user->email = $email;
        $user->division_id = $division;

        if ($user->save()) {
            $response = [
                'status' => 'success',
                'data' => $user
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => 'Unable to switch status'
            ];
        }
        return response()->json($response);
    }
}
