<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();
Route::post('/logout', '\App\Http\Controllers\Auth\AuthController@logout');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Room', 'middleware' => 'auth', 'prefix' => 'room'], function() {
    Route::get('/index', 'BookingController@index')->name('room.index');
    Route::get('/date/{date}', 'BookingController@show')
        ->middleware(['auth','is_admin'])
        ->name('show_booking_for_date');
    Route::get('/mybooking', 'BookingController@mybooking')->name('room.mybooking');
    Route::get('/mybooking/view/{id}', 'BookingController@view')->name('room.view');
    Route::get('/mybooking/edit/{id}', 'BookingController@edit')->name('room.edit');

    /**
     * API Routes
     * */
    Route::group(['prefix' => 'api'], function() {
        Route::post('/get_available_room', 'BookingController@api_get_available_room')->name('room.api.get_available_room');
        Route::post('/save_room_booking', 'BookingController@api_store_room_booking')->name('room.api.save_room_booking');
        Route::get('/show_all_room_booking', 'BookingController@api_show_all_room_booking')->name('room.api.show_all_room_booking');
        Route::post('/delete_room_booking', 'BookingController@api_delete_room_booking')->name('room.api.delete_room_booking');
        Route::post('/cancel_room_booking', 'BookingController@api_cancel_room_booking')->name('room.api.cancel_room_booking');
        Route::post('/edit_room_booking', 'BookingController@api_edit_room_booking')->name('room.api.edit_room_booking');
        Route::post('/get_my_booking', 'BookingController@api_get_my_booking')->name('room.api.get_my_booking');
        Route::post('/verify_booking', 'BookingController@api_verify_booking')->name('room.api.verify_booking');
    });

    /**
     * Admin Routes
     */
    Route::group(['prefix' => 'admin', 'middleware'=>'is_admin'], function() {
        Route::get('/all','AdminController@all')->name('room.admin.all');
        Route::get('/view/{id}', 'AdminController@view')->name('room.admin.view');

        /**
         * API Routes
         * */
        Route::group(['prefix' => 'api'], function() {
            Route::post('/get_all_booking', 'AdminController@api_get_all_booking')->name('room.admin.api.get_all_booking');
            Route::post('/verify_booking', 'AdminController@api_verify_booking')->name('room.admin.api.verify_booking');
            Route::post('/verify_pending_quotation', 'AdminController@api_verify_pending_quotation')->name('room.admin.api.verify_pending_quotation');
            Route::post('/add_caterer_quotation', 'AdminController@api_add_caterer_quotation')->name('room.admin.api.add_caterer_quotation');
            Route::post('/get_caterer_quotation', 'AdminController@api_get_caterer_quotation')->name('room.admin.api.get_caterer_quotation');
            Route::post('/delete_caterer_quotation', 'AdminController@api_delete_caterer_quotation')->name('room.admin.api.delete_caterer_quotation');
            Route::post('/select_caterer_quotation', 'AdminController@api_select_caterer_quotation')->name('room.admin.api.select_caterer_quotation');
            Route::post('/get_rooms_parameter', 'AdminController@api_get_rooms_parameter')->name('room.admin.api.get_rooms_parameter');
            Route::post('/save_room_parameter', 'AdminController@api_save_room_parameter')->name('room.admin.api.save_room_parameter');
            Route::post('/delete_room_parameter', 'AdminController@api_delete_room_parameter')->name('room.admin.api.delete_room_parameter');
            Route::post('/get_room_setup_parameter', 'AdminController@api_get_room_setup_parameter')->name('room.admin.api.get_room_setup_parameter');
            Route::post('/save_room_setup_parameter', 'AdminController@api_save_room_setup_parameter')->name('room.admin.api.save_room_setup_parameter');
            Route::post('/delete_room_setup_parameter', 'AdminController@api_delete_room_setup_parameter')->name('room.admin.api.delete_room_setup_parameter');
            Route::post('/get_divisions_parameter', 'AdminController@api_get_divisions_parameter')->name('room.admin.api.get_divisions_parameter');
            Route::post('/save_divisions_parameter', 'AdminController@api_save_divisions_parameter')->name('room.admin.api.save_divisions_parameter');
            Route::post('/delete_divisions_parameter', 'AdminController@api_delete_divisions_parameter')->name('room.admin.api.delete_divisions_parameter');
            Route::post('/get_food_setup_parameter', 'AdminController@api_get_food_setup_parameter')->name('room.admin.api.get_food_setup_parameter');
            Route::post('/save_food_setup_parameter', 'AdminController@api_save_food_setup_parameter')->name('room.admin.api.save_food_setup_parameter');
            Route::post('/delete_food_setup_parameter', 'AdminController@api_delete_food_setup_parameter')->name('room.admin.api.delete_food_setup_parameter');
            Route::post('/get_foods_parameter', 'AdminController@api_get_foods_parameter')->name('room.admin.api.get_foods_parameter');
            Route::post('/save_foods_parameter', 'AdminController@api_save_foods_parameter')->name('room.admin.api.save_foods_parameter');
            Route::post('/delete_foods_parameter', 'AdminController@api_delete_foods_parameter')->name('room.admin.api.delete_foods_parameter');
            Route::post('/get_drinks_parameter', 'AdminController@api_get_drinks_parameter')->name('room.admin.api.get_drinks_parameter');
            Route::post('/save_drinks_parameter', 'AdminController@api_save_drinks_parameter')->name('room.admin.api.save_drinks_parameter');
            Route::post('/delete_drinks_parameter', 'AdminController@api_delete_drinks_parameter')->name('room.admin.api.delete_drinks_parameter');
        });
    });
});

Route::group(['namespace' => 'Vehicle', 'middleware' => 'auth', 'prefix' => 'vehicle'], function() {
    Route::get('/index', 'BookingController@index')->name('vehicle.index');
    Route::get('/date/{date}', 'BookingController@show')
        ->middleware(['auth','is_admin'])
        ->name('show_booking_for_date');
    Route::get('/mybooking', 'BookingController@mybooking')->name('vehicle.mybooking');
    Route::get('/mybooking/view/{id}', 'BookingController@view')->name('vehicle.view');
    Route::get('/mybooking/edit/{id}', 'BookingController@edit')->name('vehicle.edit');
    Route::get('/jobs', 'BookingController@show_jobs')->name('vehicle.jobs');
    Route::get('/jobs/view/{id}', 'BookingController@view_jobs')->name('vehicle.jobs.view');
    Route::get('/report', 'BookingController@report')->name('vehicle.report');

    /**
     * API Routes
     * */
    Route::group(['prefix' => 'api'], function() {
        Route::get('/show_all_vehicle_booking', 'BookingController@api_show_all_vehicle_booking')->name('vehicle.api.show_all_vehicle_booking');
        Route::post('/get_available_vehicle', 'BookingController@api_get_available_vehicle')->name('vehicle.api.get_available_vehicle');
        Route::post('/save_vehicle_booking', 'BookingController@api_store_vehicle_booking')->name('vehicle.api.save_vehicle_booking');
        Route::post('/edit_vehicle_booking', 'BookingController@api_edit_vehicle_booking')->name('vehicle.api.edit_vehicle_booking');
        Route::post('/delete_vehicle_booking', 'BookingController@api_delete_vehicle_booking')->name('vehicle.api.delete_vehicle_booking');
        Route::post('/cancel_vehicle_booking', 'BookingController@api_cancel_vehicle_booking')->name('vehicle.api.cancel_vehicle_booking');
        Route::post('/get_my_booking', 'BookingController@api_get_my_booking')->name('vehicle.api.get_my_booking');
        Route::post('/get_my_jobs', 'BookingController@api_get_my_jobs')->name('vehicle.api.get_my_jobs');
        Route::post('/save_vehicle_logbook', 'BookingController@api_save_vehicle_logbook')->name('vehicle.api.save_vehicle_logbook');
        Route::post('/upload_vehicle_log_files', 'BookingController@api_upload_vehicle_log_files')->name('vehicle.api.upload_vehicle_log_files');
        Route::post('/get_vehicle_log_files', 'BookingController@api_get_vehicle_log_files')->name('vehicle.api.get_vehicle_log_files');
        Route::post('/delete_vehicle_log_file', 'BookingController@api_delete_vehicle_log_file')->name('vehicle.api.delete_vehicle_log_file');
        Route::post('/generate_report', 'BookingController@api_generate_report')->name('vehicle.api.generate_report');
        Route::post('/job_done', 'BookingController@api_job_done')->name('vehicle.api.job_done');
    });

    /**
     * Admin Routes
     */
    Route::group(['prefix' => 'admin', 'middleware'=>'is_admin'], function() {
        Route::get('/all', 'AdminController@all')->name('vehicle.admin.all');
        Route::get('/view/{id}', 'AdminController@view')->name('vehicle.admin.view');
        Route::get('/edit/{id}', 'AdminController@edit')->name('vehicle.admin.edit');
        Route::get('/report', 'AdminController@report')->name('vehicle.admin.report');

        /**
         * API Routes
         * */
        Route::group(['prefix' => 'api'], function() {
            Route::post('/get_all_booking', 'AdminController@api_get_all_booking')->name('vehicle.admin.api.get_all_booking');
            Route::post('/verify_booking', 'AdminController@api_verify_booking')->name('vehicle.admin.api.verify_booking');
            Route::post('/verify_booking_with_driver', 'AdminController@api_verify_booking_with_driver')->name('vehicle.admin.api.verify_booking_with_driver');
        });
    });
});

Route::group(['namespace' => 'Administrator', 'middleware' => 'auth', 'prefix' => 'admin'], function() {
    Route::group(['prefix' => 'setting', 'middleware' => 'is_admin'], function() {
        Route::group(['prefix' => 'global'], function() {
            Route::get('/parameter_management', 'AdminController@global_parameter')->name('admin.setting.global.parameter');
            Route::get('/user_management', 'AdminController@user_management')->name('admin.setting.global.user_management');
        });
        Route::group(['prefix' => 'room'], function() {
            Route::get('/parameter_management', 'AdminController@room_parameter')->name('admin.setting.room.parameter');
        });
        Route::group(['prefix' => 'vehicle'], function() {
            Route::get('/parameter_management', 'AdminController@vehicle_parameter')->name('admin.setting.vehicle.parameter');
            Route::get('/driver_management', 'AdminController@driver_management')->name('admin.setting.vehicle.driver_management');
            Route::get('/service_logbook/{id}', 'AdminController@service_logbook')->name('admin.setting.vehicle.service_logbook');
        });

        /**
         * API Routes
         * */
        Route::group(['prefix' => 'api'], function() {
            Route::post('/get_all_drivers', 'AdminController@api_get_all_drivers')->name('admin.setting.api.get_all_drivers');
            Route::post('/add_replacement_driver', 'AdminController@api_add_replacement_driver')->name('admin.setting.api.add_replacement_driver');
            Route::post('/edit_replacement_driver', 'AdminController@api_edit_replacement_driver')->name('admin.setting.api.edit_replacement_driver');
            Route::post('/add_office_driver', 'AdminController@api_add_office_driver')->name('admin.setting.api.add_office_driver');
            Route::post('/activate_or_deactivate_driver', 'AdminController@api_activate_or_deactivate_driver')->name('admin.setting.api.activate_or_deactivate_driver');
            Route::post('/get_all_users', 'AdminController@api_get_all_users')->name('admin.setting.api.get_all_users');
            Route::post('/grant_or_revoke_admin', 'AdminController@api_grant_or_revoke_admin')->name('admin.setting.api.grant_or_revoke_admin');
            Route::post('/save_user_info', 'AdminController@api_save_user_info')->name('admin.setting.api.save_user_info');
            Route::post('/get_vehicles_parameter', 'AdminController@api_get_vehicles_parameter')->name('admin.setting.api.get_vehicles_parameter');
            Route::post('/save_vehicles_parameter', 'AdminController@api_save_vehicles_parameter')->name('admin.setting.api.save_vehicles_parameter');
            Route::post('/delete_vehicles_parameter', 'AdminController@api_delete_vehicles_parameter')->name('admin.setting.api.delete_vehicles_parameter');
            Route::post('/get_service_log', 'AdminController@api_get_service_log')->name('admin.setting.api.get_service_log');
            Route::post('/save_service_log', 'AdminController@api_save_service_log')->name('admin.setting.api.save_service_log');
            Route::post('/delete_service_log', 'AdminController@api_delete_service_log')->name('admin.setting.api.delete_service_log');
        });
    });
});
